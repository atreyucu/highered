<?php
/**
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 */
?>
<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>

<?php echo "<?php \$form = \$this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl(\$this->route),
	'method' => 'get',
)); ?>\n"; ?>

<?php foreach($this->tableSchema->columns as $column): ?>
    <?php
        $field = $this->generateInputField($this->modelClass, $column);
        if (strpos($field, 'password') !== false)
            continue;
    ?>
    <?php echo "<?php " . $this->generateSearchField($this->modelClass, $column)."; ?>\n"; ?>
<?php endforeach; ?>

<div class="form-actions">
    <?php echo "\t\t<?php \$this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('Buscar '.mod('ycm')->getPluralName(\$model))
        ));\n ?>"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>