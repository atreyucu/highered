<?php
/**
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 */
?>
<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>


<?php echo "<?php\n"; ?>
<?php
$submenu = null;
 foreach($this->tableSchema->columns as $column){

    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);

    if(isset($comment_json))
    {
        if(array_key_exists('breadcrumbs', $comment_json))
        {

            $breadcrumbs_temp = $comment_json['breadcrumbs'];
            if(array_key_exists('submenu', $breadcrumbs_temp))
            {

                $submenu = $breadcrumbs_temp["submenu"];
                break;
            }


        }
    }
 }


?>
$this->breadcrumbs = array(
        $model->adminNames[3] <?php if($submenu != null) echo ",'".$submenu."'" ?>  => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('YcmModule.ycm',
    'View <?php if($submenu != null) echo $submenu;  else echo "{name}";?>',
    array('{name}'=>$model->adminNames[3])
);

?>

<?php echo '<?php'; ?> $this->widget('application.extensions.bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(
<?php
foreach ($this->tableSchema->columns as $column)
    if($column->name!='id' && $column->name!='created' && $column->name!='updated' && $column->name!='owner')
		echo $this->generateDetailViewAttribute($this->modelClass, $column) . ",\n";
?>
	),
)); ?>

<div class="form-actions" style="text-align: right">
 <?php echo "   <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>";?>
<?php
    echo "<?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?>";
    echo "<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>\$model->id),array('class'=>'btn btn-primary'));?>";
    echo "<?php if(user()->isAdmin):?>\n";
    echo "<?php echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>\$model->id),array('class'=>'btn btn-danger delete'));?>";
    echo '<?php endif?>'
?>
</div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo "<?php echo Yii::t('admin','Data was saved with success')?>";?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
echo "<?php\n";
?>
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
  jQuery(document).on('click','a.delete',function() {
    if(!confirm($text)) return false;
        return true;

    });
JS;
cs()->registerScript('delete_confirm',$js,CClientScript::POS_READY);
<?php
echo "?>";
?>
