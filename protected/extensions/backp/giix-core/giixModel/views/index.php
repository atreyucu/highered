<?php
/** @var $this GiixModelGenerator */
/** @var $form CCodeForm */
/** @var $model GiixModelCode */
$class=get_class($model);
Yii::app()->clientScript->registerScript('gii.model',"
$('#{$class}_modelClass').change(function(){
	$(this).data('changed',$(this).val()!='');
});
$('#{$class}_tableName').bind('keyup change', function(){
	var model=$('#{$class}_modelClass');
	var tableName=$(this).val();
	if(tableName.substring(tableName.length-1)!='*') {
		$('.form .row.model-class').show();
	}
	else {
		$('#{$class}_modelClass').val('');
		$('.form .row.model-class').hide();
	}
	if(!model.data('changed')) {
		var i=tableName.lastIndexOf('.');
		if(i>=0)
			tableName=tableName.substring(i+1);
		var tablePrefix=$('#{$class}_tablePrefix').val();
		if(tablePrefix!='' && tableName.indexOf(tablePrefix)==0)
			tableName=tableName.substring(tablePrefix.length);
		var modelClass='';
		$.each(tableName.split('_'), function() {
			if(this.length>0)
				modelClass+=this.substring(0,1).toUpperCase()+this.substring(1);
		});
		model.val(modelClass);
	}
});
$('.form .row.model-class').toggle($('#{$class}_tableName').val().substring($('#{$class}_tableName').val().length-1)!='*');
");
?>
<h1>giix Model Generator</h1>

<p>This generator generates a model class for the specified database table.</p>

<?php
$form=$this->beginWidget('CCodeForm', array('model'=>$model));
echo $form->hiddenField($model,'tablePrefix');
?>

	<div class="row">
		<?php echo $form->labelEx($model,'tableName'); ?>

        <?php

        $options=array(
            'data-placeholder'=>Yii::t('YcmModule.ycm',
                'Choose {name}',
                array('{name}'=>$model->getAttributeLabel('tableName'))
            ),
            'empty' => Yii::t('YcmModule.ycm',
                'Choose {name}',
                array('{name}'=>$model->getAttributeLabel('tableName'))
            ),
            /*'multiple'=>'multiple',*/
            'class'=>'chosen-select',
        );
        $this->widget('ycm.extensions.chosen.EChosenWidget');
        echo $form->dropDownList($model,'tableName', $this->getTables(), $options);
        ?>
		<?php echo $form->error($model,'tableName'); ?>
	</div>
	<div class="row model-class">
		<?php echo $form->label($model,'modelClass',array('required'=>true)); ?>
		<?php echo $form->textField($model,'modelClass', array('size'=>65)); ?>
		<div class="hint">
		This is the name of the model class to be generated (e.g. <code>Post</code>, <code>Comment</code>).
		It is case-sensitive.
		</div>
		<?php echo $form->error($model,'modelClass'); ?>
	</div>
	<div class="row sticky">
		<?php echo $form->labelEx($model,'baseClass'); ?>
		<?php echo $form->textField($model,'baseClass',array('size'=>65)); ?>
		<div class="hint">
			This is the class that the new model class will extend from.
			Please make sure the class exists and can be autoloaded.
		</div>
		<?php echo $form->error($model,'baseClass'); ?>
	</div>
	<div class="row sticky">
        <?php echo $form->labelEx($model,'modelPath'); ?>
		<?php echo $form->dropDownList($model,'modelPath', $this->getModelPaths()); ?>
		<div class="hint">
			Application for <code>application.models</code> else select the name of the module
		</div>
		<?php echo $form->error($model,'modelPath'); ?>
	</div>

<?php $this->endWidget(); ?>