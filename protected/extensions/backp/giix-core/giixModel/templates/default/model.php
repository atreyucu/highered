<?php
/**
 * This is the template for generating the model class of a specified table.
 * In addition to the default model Code, this adds the CSaveRelationsBehavior
 * to the model class definition.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 * - $representingColumn: the name of the representing column for the table (string) or
 *   the names of the representing columns (array)
 * this code was improve by iReevo Team
 */
?>
<?php
$uid=0;
$seo=0;
$owner=0;
$one_many= array();
$many_many=array();
$timestamp_fields=0;
foreach(array_keys($relations) as $name){
    $relationData = $this->getRelationData($modelClass, $name);
    $relationType = $relationData[0];
    $relationModel = $relationData[1];
    if(GxActiveRecord::HAS_MANY){
        $one_many[]= $relationModel;
    }
    if(GxActiveRecord::MANY_MANY){
        $many_many[]= $relationModel;
    }
    if($relationModel=='SeoUrl'){
        $seo=1;
    }

} ?>

<?php echo "<?php\n"; ?>

Yii::import('<?php echo "{$this->baseModelPath}.{$this->baseModelClass}"; ?>');

class <?php echo $modelClass; ?> extends <?php echo $this->baseModelClass."\n"; ?>
{
    /**
    * @param string $className
    * @return <?php echo $modelClass; ?>
    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }
<?php $tablename = $modelClass;?>

<?php foreach($columns as $column){

    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);

    if(isset($comment_json))
    {
        if(array_key_exists('breadcrumbs', $comment_json))
        {
            $breadcrumbs_temp = $comment_json['breadcrumbs'];
            if(array_key_exists('tablename', $breadcrumbs_temp))
            {
                $tablename = $breadcrumbs_temp["tablename"];
            }


        }
    }

    if($column->name=='created' || $column->name=='updated'){
        $timestamp_fields = 1;
    }
    if($column->name=='id' && $column->type=='string'){
        $uid=1;
    }
    if($column->name=='owner'){
        $owner=1;
    }
} ?>

<?php if($uid==1 && $owner==0) :?>
    public function init(){
        $this->id = rand_uniqid();
    }
<?php endif?>
<?php if($uid==0 && $owner==1) :?>
    public function init(){
      $this->owner= UserModule::user()->username;
    }
<?php endif?>
<?php if($uid==1 && $owner==1) :?>
    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }
<?php endif?>

<?php if($timestamp_fields==1):?>
    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                $this->updated = date('Y-m-d G:i:s');
                return true;
            }
            else
            {
                $this->updated= date('Y-m-d G:i:s');
                return true;
            }
        }
        else{
            return false;
        }

    }
<?php endif?>


<?php if(count($many_many)>0):?>
    /* Debe crear el metodo que aparece debajo para cada relacion
    public function afterSave(){
        parent::afterSave();
        if (!$this->isNewRecord) {

            CupOfferCupCity::model()->deleteAll('cup_offer_id='.$this->id);
        }

        if(is_array($this->city)) {
            foreach($this->city as $city_id) {
                $offerCity = new CupOfferCupCity();
                $offerCity->cup_city_id = $city_id;
                $offerCity->cup_offer_id = $this->id;
                $offerCity->save(false);
            }
        }
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->city = array_map(function($cupcity){return $cupcity->id;}, $this->cities);
        }
    }
    */
<?php endif?>

    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('<?php echo $this->pluralize($modelClass); ?>','<?php echo strtolower($modelClass); ?>','<?php echo strtolower($this->pluralize($modelClass)); ?>','<?php echo $tablename?>'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV

<?php
    foreach ($this->getTableSchema($this->tableName)->columns as $column):
        /** @var $column TYPE_NAME */
        if ($column->isForeignKey):
            //tenemos $tableName
            //tenemos $column->name
            //Nesesitamos el nombre de la crase foranea
            $relatedClass = $this->findRelatedClass($modelClass, $column);
            if(isset($relatedClass)):
?>
    public function <?php echo $column->name; ?>Choices(){
        return <?php echo $relatedClass; ?>::choices();
    }

<?php
            endif;
        endif;
    endforeach;
?>

    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
<?php foreach ($this->getTableSchema($this->tableName)->columns as $column): ?>
    <?php if (!$column->autoIncrement): ?>
            <?php echo $this->generateWidget($column) . ",\n"; ?>
    <?php endif; ?>
<?php endforeach; ?>
        );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
<?php if($this->orderColumn):?>
    array(
        'class' => 'ext.yExt.YOrderColumn',
    ),
<?php endif; ?>
<?php
$count = 0;
/** @var $this GiixModelCode */
foreach ($this->getTableSchema($this->tableName)->columns as $column) {
    /** @var $column CDbColumnSchema */
    if (!$column->isPrimaryKey) {
        if (++$count == 7)
            echo "\t\t\t\t/*\n";
        $c = $this->generateGridViewColumn($this->modelClass, $column);
        echo $c?"\t\t\t\t$c,\n":'';
    }
}
if ($count >= 7)
    echo "\t\t\t*/\n";
?>
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                <?php
                $count = 0;
                /** @var $this GiixModelCode */
                foreach ($this->getTableSchema($this->tableName)->columns as $column) {
                    /** @var $column CDbColumnSchema */
                    if (!$column->isPrimaryKey) {
                        if (++$count == 7)
                            echo "\t\t\t\t/*\n";
                        $c = $this->generateDetailViewColumn($this->modelClass, $column);
                        echo $c?"\t\t\t\t$c,\n":'';
                    }
                }
                if ($count >= 7)
                    echo "\t\t\t*/\n";
                ?>
            ),
        );
}



}