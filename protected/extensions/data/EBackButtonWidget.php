<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 3/3/14
 * Time: 4:26 PM
 */
class EBackButtonWidget extends CWidget
{

    public $width = "150px";

    public function run()
    {

        echo CHtml::button(t('...Back'), array(
                'name' => 'btnBack',
                'class' => 'btn btn-info',
                'style' => 'width:' . $this->width . ';',
                'onclick' => "history.go(-1)",
            )
        );
    }

}