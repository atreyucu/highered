<?php

class YiiTwigExtension extends Twig_Extension
{
    public function getName()
    {
        return 'YiiTwig';
    }

    public function getGlobals()
    {
        return array(
            'app' => Yii::app(),
        );
    }

    public function getFilters()
    {
        return array(
            'formatDecimal' => new Twig_Filter_Method($this, 'formatDecimalFilter'),
            'formatCurrency' => new Twig_Filter_Method($this, 'formatCurrencyFilter'),
            'trans' => new Twig_Filter_Method($this, 'translateFilter'),
            'url' => new Twig_Filter_Method($this, 'urlFilter'),
            'count' => new Twig_Filter_Method($this, 'countFilter'),
            'attr' => new Twig_Filter_Method($this, 'attrFilter'),
            'str' => new Twig_Filter_Method($this, 'strFilter'),
            'float' => new Twig_Filter_Method($this, 'floatFilter'),
            'int' => new Twig_Filter_Method($this, 'intFilter'),
            'array' => new Twig_Filter_Method($this, 'arrayFilter'),
            'money' => new Twig_Filter_Method($this, 'formatMoney'),
            'number' => new Twig_Filter_Method($this, 'formatNumber'),

        );
    }

    public function translateFilter($string, $categ = "html")
    {
        return Yii::t($categ, $string);
    }

    public function urlFilter($string)
    {
        return CHtml::normalizeUrl(array($string));
    }

    public function countFilter($object)
    {
        return count($object);
    }

    public function attrFilter($object, $attr)
    {
        return $object->{$attr};
    }

    public function strFilter($object)
    {
        return (string)$object;
    }

    public function floatFilter($object)
    {
        return (float)$object;
    }

    public function intFilter($object)
    {
        return (int)$object;
    }

    public function arrayFilter($object)
    {
        return (array)$object;
    }

    public function formatDecimalFilter($number)
    {
        return Yii::app()->numberFormatter->formatDecimal((float)$number);
    }

    public function formatCurrencyFilter($number, $currency = 'USD')
    {
        return Yii::app()->numberFormatter->formatCurrency((float)$number, $currency);
    }

    public function formatMoney($value, $moneySymbol = '$', $decimals = 0, $dec_point = ',', $thousands_sep = '.')
    {
        return $moneySymbol . number_format($value, $decimals, $dec_point, $thousands_sep);
    }

    public function formatNumber($value, $decimals = 0, $dec_point = ',', $thousands_sep = '.')
    {
        return number_format($value, $decimals, $dec_point, $thousands_sep);
    }


}
