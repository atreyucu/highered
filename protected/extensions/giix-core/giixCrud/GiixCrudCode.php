<?php

/**
 * GiixCrudCode class file.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 * @link http://giix.org/
 * @copyright Copyright &copy; 2010-2011 Rodrigo Coelho
 * @license http://giix.org/license/ New BSD License
 */
Yii::import('system.gii.generators.crud.CrudCode');
Yii::import('ext.giix-core.helpers.*');

/**
 * GiixCrudCode is the model for giix crud generator.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 */
class GiixCrudCode extends CrudCode
{

    /**
     * @var string The type of authentication.
     */
    public $authtype = 'auth_none';
    /**
     * @var int Specifies if ajax validation is enabled. 0 represents false, 1 represents true.
     */
    public $enable_ajax_validation = 1;
    /**
     * @var string The controller base class name.
     */
    public $baseControllerClass = 'GxController';
    /*
     * @var string The module
     */
    public $modulePath;

    /*
     * @var pagetitle
     */
    //  public $pageTitle;

    /*
     * @var pagetitle
     */
    // public $breadcrumbsTitle;
    /**
     * Adds the new model attributes (class properties) to the rules.
     * #MethodTracker
     * This method overrides {@link CrudCode::rules}, from version 1.1.7 (r3135). Changes:
     * <ul>
     * <li>Adds the rules for the new attributes in the code generation form: authtype; enable_ajax_validation.</li>
     * </ul>
     */
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('authtype, enable_ajax_validation,modulePath', 'required'),
        ));
    }

    /**
     * Sets the labels for the new model attributes (class properties).
     * #MethodTracker
     * This method overrides {@link CrudCode::attributeLabels}, from version 1.1.7 (r3135). Changes:
     * <ul>
     * <li>Adds the labels for the new attributes in the code generation form: authtype; enable_ajax_validation.</li>
     * </ul>
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), array(
            'authtype' => 'Authentication type',
            'enable_ajax_validation' => 'Enable ajax validation',
        ));
    }


    /**
     * Generates and returns the view source code line
     * to create the appropriate gridview
     * the model attribute field type on the database.
     * #MethodTracker
     * This method is based on {@link CrudCode::generateActiveField}, from version 1.1.7 (r3135). Changes:
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the active field.
     */
    public function generateTbEditableColumn($modelClass, $column)
    {
        $comment = $column->comment;
        $comment_json = CJSON::decode($comment);
        $type = null;
        $i18n = false;
        $alt = true;
        $mask = null;
        $append = '';
        $prepend = '';
        $hint = '';
        $options = null;
        $label = (isset($comment_json['label'])) ? $comment_json['label'] : $column->name;

        $data = "array(0=>Yii::t('admin','No'),1=>Yii::t('admin','Yes'))";

        if (isset($comment_json)) {
            if (array_key_exists('type', $comment_json)) {
                $type = $comment_json['type'];
            }
            if (array_key_exists('alt', $comment_json)) {
                $alt = $comment_json['alt'];
            }
            if (array_key_exists('mask', $comment_json)) {
                $mask = $comment_json['mask'];
            }
            if (array_key_exists('options', $comment_json)) {
                $options = var_export($comment_json['options'], true);
            }
            if (array_key_exists('append', $comment_json)) {
                $append = $comment_json['append'];
            }
            if (array_key_exists('prepend', $comment_json)) {
                $prepend = $comment_json['prepend'];
            }
            if (array_key_exists('hint', $comment_json)) {
                $hint = $comment_json['hint'];
            }
            if (array_key_exists('data', $comment_json)) {
                $data ="array(";
                foreach($comment_json['data'] as $key=>$item){
                    $data.="$key=>Yii::t('admin','$item'),";
                }
                $data.=')';
            }
            if (array_key_exists('i18n', $comment_json)) {
                $i18n = true;
            }
        }
        if ($column->isForeignKey) {
            $relation = $this->findRelation($modelClass, $column);
            $relatedModelClass = $relation[3];

            if (stripos($column->comment, '@select2') !== false) {
                return "\$this->widget('bootstrap.widgets.TbSelect2',
                                    array(
                                        'form'=>\$form,
                                        'model'=>\$model,
                                        'attribute'=>'{$column->name}',
                                        'data' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                                        'htmlOptions' => array(
                                            //'multiple' => 'multiple',
                                            'placeholder'=>Yii::t('admin','Please, select {$column->name}'),
                                        ),
                                    )
                                )";

            } else {
                return "echo \$form->dropDownListGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                               // 'hint' => Yii::t('admin','Please, select {$column->name}'),
                            )
                        )";

            }
        }

        if (preg_match('/^.*array/i', $column->comment)) {
            return "echo \$form->dropDownListGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => {$data},
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                               //  'hint' => Yii::t('admin','Please, select {$column->name}'),
                            )
                        )";

        }

        if (strtoupper($column->dbType) == 'TINYINT(1)'
            || strtoupper($column->dbType) == 'BIT'
            || strtoupper($column->dbType) == 'BOOL'
            || strtoupper($column->dbType) == 'BOOLEAN'
        ) {
            return "echo \$form->checkBoxGroup(\$model, '{$column->name}')";
        } else if (strtoupper($column->dbType) == 'DATE') {
            return "echo \$form->datePickerGroup(
                \$model,
                '{$column->name}',
                array(
                    'widgetOptions' => array(
                        'options' => array(
                            'language' => 'en',
                        ),
                    ),
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'hint' => Yii::t('admin','Click, to select a date'),
                    'prepend' => '<i class=\"glyphicon glyphicon-calendar\"></i>'
                )
            )";
            // return "echo \$form->datepickerGroup(\$model, '{$column->name}')";
        } elseif (strtoupper($column->dbType) == 'TIME') {
            return "echo \$form->timePickerGroup(
			\$model,
			'{$column->name}',
			array(
				'widgetOptions' => array(
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-3'
					),
				),
				'hint' => Yii::t('admin','Select a time'),
				'prepend' => '<i class=\"glyphicon glyphicon-time\"></i>'
			)
		)";
        } else if (strtoupper($column->dbType) == 'SMALLINT'
            || strtoupper($column->dbType) == 'MEDIUMINT'
            || strtoupper($column->dbType) == 'INT'
            || strtoupper($column->dbType) == 'BIGINT'
        ) {
            $options = array();
            // $options=$this->getAttributeOptions($column->name,$options);
            return "echo \$form->numberFieldGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => Yii::t('admin','Please, insert {$column->name}'),
                                'append' => '#'
                            )
                        )";
        } else if (strtoupper($column->dbType) == 'DECIMAL'
            || strtoupper($column->dbType) == 'DOUBLE'
            || strtoupper($column->dbType) == 'FLOAT'
        ) {
            $options = array();
            // $options=$this->getAttributeOptions($column->name,$options);
            return "echo \$form->textFieldGroup(\$model,'{$column->name}',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				// 'hint' => Yii::t('admin','Please, insert {$column->name}'),
				'append' => '.00'
			)
		)";
        } elseif (stripos($column->comment, '@spinner') !== false) {
            $options = array();
            // $options=$this->getAttributeOptions($column->name,$options);
            return "echo \$form->numberFieldGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' => Yii::t('admin','Please, insert {$column->name}'),
                                'append' => '#'
                            )
                        )";
        } elseif (stripos($column->comment, '@currency') !== false) {
            $element = "echo \$form->textFieldRow(\$model, '{$column->name}',array('id'=>'id_{$column->name}',,array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5 currency',
                            ),
                           // 'hint' => Yii::t('admin','Please, insert {$column->name}'),
				            'append' => '$'
                        ));
                        \$this->widget('ext.currency.CurrencyWidget', array('selector' => '#id_{$column->name}'))";
            return $element;
        } else if (stripos($column->comment, '@image') !== false) {
            return "echo \$form->fileFieldGroup(\$model, 'recipeImg',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                        ))";
        } elseif (stripos($column->comment, 'recipeImg') !== false) {
            return "echo \$form->textFieldGroup(\$model, '{$column->name}',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'hint' => Yii::t('admin','Please, insert {$column->name}')
                            )
                        );
                        echo \$form->fileFieldGroup(\$model, 'recipeImg',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                        ))";
        } else if (stripos($column->comment, '@file') !== false) {
            return "echo \$form->fileFieldGroup(\$model, '{$column->name}',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                        ))";
        } elseif (stripos($column->dbType, 'text') !== false) { // Start of CrudCode::generateActiveField code.
            if (preg_match('/^.*_redactor$/i', $column->name) || stripos($column->comment, '@redactor') !== false) {
                $inputField = "echo \$form->redactorGroup(\$model,'{$column->name}',
                array(
                    'widgetOptions' => array(
                            'editorOptions' =>array(
                                'class' => 'span4',
                                'rows' => 10,
                                'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage())
                            )
                        )
                    )
                )";
                return $inputField;
            } elseif (preg_match('/^.*_html$/i', $column->name) || stripos($column->comment, '@html') !== false) {
                $inputField = "echo \$form->html5EditorGroup(\$model,'{$column->name}',
                                array(
                                    'widgetOptions' => array(
                                        'editorOptions' => array(
                                            'class' => 'span4',
                                            'rows' => 10,
                                            //'height' => '200',
                                            'options' => array('color' => true,'lang'=>app()->getLanguage())
                                        ),
                                    )
                                )
                        )";
                return $inputField;
            } elseif (preg_match('/^.*_ckeditor$/i', $column->name) || stripos($column->comment, '@ckeditor') !== false) {
                $inputField = "echo \$form->ckEditorGroup(\$model,'{$column->name}',
                                    array(
                                        'wrapperHtmlOptions' => array(
                                            'class' => 'col-sm-5',
                                        ),
                                        'widgetOptions' => array(
                                            'editorOptions' => array(
                                                'fullpage' => 'js:true',
                                                //'width' => '640',
                                                //'resize_maxWidth' => '640',
                                                '//resize_minWidth' => '320'
                                            )
                                        )
                                    )
                                )";
                return $inputField;
            } elseif (preg_match('/^.*_mk$/i', $column->name) || stripos($column->comment, '@markdown') !== false) {
                $inputField = "echo \$form->markdownEditorGroup(\$model,'{$column->name}'
                                array(
                                    'widgetOptions' => array(
                                        'htmlOptions' => array('style'=>'height: 200px;')
                                    )
                                )
                            )";
                return $inputField;
            } elseif (preg_match('/^.*_area$/i', $column->name) || stripos($column->comment, '@textarea') !== false) {
                $inputField = "echo \$form->textAreaGroup(\$model, '{$column->name}',array(
                                    'wrapperHtmlOptions' => array(
                                        'class' => 'col-sm-5',
                                    ),
                                    'widgetOptions' => array(
                                        'htmlOptions' => array('rows' => 5),
                                    )
                                ))";
                return $inputField;
            } else
                $inputField = 'textField';
            return "echo \$form->textFieldGroup(\$model, '{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'hint' => Yii::t('admin','Please, insert {$column->name}')
                            )
                        )";
        } else {
            $passwordI18n = Yii::t('admin', 'password');
            $passwordI18n = (isset($passwordI18n) && $passwordI18n !== '') ? '|' . $passwordI18n : '';
            $pattern = '/^(password|pass|passwd|passcode' . $passwordI18n . ')$/i';
            if (preg_match($pattern, $column->name) || stripos($column->comment, '@password') !== false)
                $inputField = 'passwordFieldGroup';
            elseif (preg_match('/^.*_color$/i', $column->name) || stripos($column->comment, '@colorpicker') !== false)
                $inputField = 'colorpickerGroup';
            else
                $inputField = 'textFieldGroup';

            if ($column->type !== 'string' || $column->size === null)
                return "echo \$form->{$inputField}(\$model, '{$column->name}',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'hint' => Yii::t('admin','Please, insert {$column->name}')
                            )
                        )";

            else
                return "echo \$form->{$inputField}(\$model, '{$column->name}', array('maxlength' => {$column->size}))";
        } // End of CrudCode::generateActiveField code.
    }

    /**
     * Generates and returns the view source code line
     * to create the appropriate active input field based on
     * the model attribute field type on the database.
     * #MethodTracker
     * This method is based on {@link CrudCode::generateActiveField}, from version 1.1.7 (r3135). Changes:
     * <ul>
     * <li>All styling is removed.</li>
     * </ul>
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the active field.
     */
    public function generateActiveField($modelClass, $column)
    {

        $comment = $column->comment;
        $comment_json = CJSON::decode($comment);
        $type = null;
        $i18n = false;
        $alt = true;
        $mask = null;
        $append = '';
        $prepend = '';
        $hint = '';
        $options = null;
        $label = (isset($comment_json['label'])) ? $comment_json['label'] : $column->name;
        $editable = true;

        $data = "array(0=>Yii::t('admin','No'),1=>Yii::t('admin','Yes'))";

        if (isset($comment_json)) {
            if (array_key_exists('type', $comment_json)) {
                $type = $comment_json['type'];
            }
            if (array_key_exists('alt', $comment_json)) {
                $alt = $comment_json['alt'];
            }
            if (array_key_exists('mask', $comment_json)) {
                $mask = $comment_json['mask'];
            }
            if (array_key_exists('options', $comment_json)) {
                $options = var_export($comment_json['options'], true);
            }
            if (array_key_exists('append', $comment_json)) {
                $append = $comment_json['append'];
            }
            if (array_key_exists('prepend', $comment_json)) {
                $prepend = $comment_json['prepend'];
            }
            if (array_key_exists('hint', $comment_json)) {
                $hint = $comment_json['hint'];
            }
            if (array_key_exists('data', $comment_json)) {
                $data ="array(";
                foreach($comment_json['data'] as $key=>$item){
                    $data.="$key=>Yii::t('admin','$item'),";
                }
                $data.=')';
            }
            if (array_key_exists('i18n', $comment_json)) {
                $i18n = true;
            }
        }

        if ($i18n) {
            $widget = "'widgetOptions' => array(
                    'htmlOptions' => array('value' => '{value}', 'name' => '{name}'),
                ),";
            $hmtloptions = "'htmlOptions' => array('value' => '{value}', 'name' => '{name}'),";

            if ($type == "@redactor" || $type == "redactor" )
                $widget = "'widgetOptions' => array(
                    'htmlOptions' => array('rows' => 4, 'value' => '{value}', 'name' => '{name}', 'class' => 'redactor'),
                ),";
        } else {
            $widget = "";
            $hmtloptions = "";
        }

        if ($column->isForeignKey) {
            $relation = $this->findRelation($modelClass, $column);
            $relatedModelClass = $relation[3];


            if (stripos($column->comment, '@select2') !== false or $type == '@select2') {
                return "\$this->widget('bootstrap.widgets.TbSelect2',
                                    array(
                                        'form'=>\$form,
                                        'model'=>\$model,
                                        'attribute'=>'{$column->name}',
                                        'data' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                                        'htmlOptions' => array(
                                            //'multiple' => 'multiple',
                                            'placeholder'=>Yii::t('admin','Select a '{$label}'),
                                        ),
                                    )
                                )";

            } else {
                $hintr = ($hint == '') ? "Yii::t('admin','Please, select').' $label'" : "Yii::t('admin','$hint')";
                $prependr = ($prepend == '') ? "Yii::t('admin','Select')" : "Yii::t('admin','$prepend')";
                return "echo \$form->dropDownListGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => {$hintr},
                                 'prepend' => {$prependr}
                            )
                        )";

            }
        }


        if (preg_match('/^.*array/i', $column->comment) || $type == strtolower('dropdown')) {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, select').' $label'" : "Yii::t('admin','$hint')";
            $prependr = ($prepend == '') ? "Yii::t('admin','Select')" : "Yii::t('admin','$prepend')";
            $result = "echo \$form->dropDownListGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => {$data},
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => {$hintr},
                                 'prepend' => {$prependr}
                            )
                        )";
            return $result;

        } elseif (strtoupper($column->dbType) == 'TINYINT(1)'
            || strtoupper($column->dbType) == 'BIT'
            || strtoupper($column->dbType) == 'BOOL'
            || strtoupper($column->dbType) == 'BOOLEAN'
        ) {
            if (isset($comment_json['data'])) {
                $hintr = ($hint == '') ? "Yii::t('admin','Please, select').' $label'" : "Yii::t('admin','$hint')";
                $prependr = ($prepend == '') ? "Yii::t('admin','Select')" : "Yii::t('admin','$prepend')";
                return "echo \$form->dropDownListGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => {$data},
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => {$hintr},
                                 'prepend' => {$prependr}
                            )
                        )";
            } else {
                return "echo \$form->checkBoxGroup(\$model, '{$column->name}')";
            }


        } else if (strtoupper($column->dbType) == 'DATE') {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, select').' $label'" : "Yii::t('admin','$hint')";
            $prependr = ($prepend == '') ? "'<i class=\"glyphicon glyphicon-calendar\"></i>'" : "Yii::t('admin','$prepend')";
            if ($options == '' || $options == null) {
                $options = "array(
                    'language' => app()->getLanguage(),
                    'dateFormat' => 'yy-mm-dd',
                    'numberOfMonths'=> 2,
                    'showButtonPanel'=> 'true',
                    'changeYear'=>'true',
                ),";
            }
            return "echo \$form->datePickerGroup(
                \$model,
                '{$column->name}',
                array(
                    'widgetOptions' => array(
                        'options' =>{$options}
                    ),
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    //'hint' => {$hintr},
                    'prepend' => {$prependr}
                )
            )";
            // return "echo \$form->datepickerGroup(\$model, '{$column->name}')";
        } elseif (strtoupper($column->dbType) == 'TIME') {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, select').' $label'" : "Yii::t('admin','$hint')";
            $prependr = ($prepend == '') ? "'<i class=\"glyphicon glyphicon-time\"></i>'" : "Yii::t('admin','$prepend')";
            if ($options == '' || $options == null) {
                $options = "array(
                    'language' => app()->getLanguage()
                ),";
            }
            return "echo \$form->timePickerGroup(
			\$model,
			'{$column->name}',
			array(
				'widgetOptions' => array(
				'options' => {$options}
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-3'
					),
				),
				//'hint' => {$hintr},
				'prepend' => {$prependr}
			)
		)";
        } else if (strtoupper($column->dbType) == 'SMALLINT'
            || strtoupper($column->dbType) == 'MEDIUMINT'
            || strtoupper($column->dbType) == 'INT'
            || strtoupper($column->dbType) == 'BIGINT'
        ) {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'#'" : "Yii::t('admin','$append')";
            return "echo \$form->numberFieldGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' => {$hintr},
                                'append' => {$appendr}
                            )
                        )";
        } else if (strtoupper($column->dbType) == 'DECIMAL'
            || strtoupper($column->dbType) == 'DOUBLE'
            || strtoupper($column->dbType) == 'FLOAT'
        ) {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'.00'" : "Yii::t('admin','$append')";
            if ($type == 'currency') {
                $hintr = ($hint == '') ? "Yii::t('admin','Please, insert').' $label'" : "Yii::t('admin','$hint')";
                $appendr = ($append == '') ? "'$'" : "Yii::t('admin','$append')";
                $id = $modelClass . '_' . $column->name;
                $default_options = array(
                    'symbol' => '$',
                    'showSymbol' => false,
                    'symbolStay' => false,
                    'thousands' => '',
                    'decimal' => '.',
                    'precision' => 2,
                    'defaultZero' => true,
                    'allowZero' => true,
                    'allowNegative' => false,
                );
                if ($options == null) {
                    $optionsr = var_export($default_options, true);
                } else {
                    $optionsr = $options;
                }
                $element = "echo \$form->textFieldGroup(\$model,'{$column->name}',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_{$column->name}',
                       ),
                       //'hint' => {$hintr},
				 'append' => {$appendr}
			)
		);
                        \$this->widget('ext.currency.CurrencyWidget', array('selector' => '#{$id}','options'=>{$optionsr}))";


                return $element;
            } else {
                return "echo \$form->textFieldGroup(\$model,'{$column->name}',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				 //'hint' => {$hintr},
				 'append' => {$appendr}
			)
		)";
            }
        } elseif (stripos($column->comment, '@email') !== false or $type == 'email') {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'@'" : "Yii::t('admin','$append')";
            return "echo \$form->emailFieldGroup(\$model,'{$column->name}',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => {$hintr},
                              'append' => {$appendr}
                        )
                    )";
        } elseif (stripos($column->comment, '@mask') !== false or $type == 'mask') {
            $lengh = strlen($mask);
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "Yii::t('admin','mask')" : "Yii::t('admin','$append')";
            return "echo \$form->maskedTextFieldGroup(\$model, '{$column->name}', array(
                'maxlength' => {$lengh},
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions'=>array(
                    'mask'=>'{$mask}'
                ),
                //'hint' => {$hintr},
                'append' => {$appendr}
            ))";

        } elseif (stripos($column->comment, '@url') !== false or $type == 'url') {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'http://'" : "Yii::t('admin','$append')";
            return "echo \$form->urlFieldGroup(\$model,'{$column->name}',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => {$hintr},
                              'append' => {$appendr}
                        )
                    )";
        } elseif (stripos($column->comment, '@spinner') !== false or $type == 'spinner') {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'#'" : "Yii::t('admin','$append')";
            return "echo \$form->numberFieldGroup(\$model,'{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               //'hint' => {$hintr},
                              'append' => {$appendr}
                            )
                        )";
        } elseif (stripos($column->comment, 'recipeImg') !== false or stripos($type, 'recipeImg') !== false) {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'text'" : "Yii::t('admin','$append')";
            $type_temp = 'recipeImg';
            if (stripos($type, 'recipeImg') !== false)
                $type_temp = $type;
            else
                $type_temp = $column->comment;
            $text_help = "//'hint' => Yii::t('admin','Please, insert {$column->name}')";
            if ($comment_json != null and array_key_exists('dimensions', $comment_json)) {

                $dimensions = str_replace(',', 'x', $comment_json['dimensions']);
                $text_help = "'hint' => Yii::t('admin','The image dimensions are {$dimensions}px')";
            }
            $return_text = "echo \$form->fileFieldGroup(\$model, '{$type_temp}',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image(\$model->_{$column->name}->getFileUrl('normal'), '', array('width' => '100px')),
                            {$text_help}
                                    ))";
            if ($alt || $alt=='true' || $alt=='TRUE') {
                $return_text = $return_text . "; \n
                 echo \$form->textFieldGroup(\$model, '{$column->name}',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),"
                         . $widget .
                                "//'hint' =>{$hintr},
                                            'append' => {$appendr},
                                        )
                                    )";
            }
            return $return_text;

        } else if (stripos($column->comment, '@image') !== false or stripos($type, '@image') !== false) {
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "'#'" : "Yii::t('admin','$append')";
            return "echo \$form->fileFieldGroup(\$model, 'recipeImg',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            // 'hint' => Yii::t('admin','Please, select {$column->name}'),
                            'append' => 'Text'
                        ));
                        echo \$form->textFieldGroup(\$model, '{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                " . $widget . "
                                'hint' => {$hintr},
                                'append' => {$appendr}
                            )
                        )";
        } else if (stripos($column->comment, '@file') !== false) {
            return "echo \$form->fileFieldGroup(\$model, '{$column->name}',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            // 'hint' => Yii::t('admin','Please, select {$column->name}'),
                            'append' => 'Text'
                        ))";
        } elseif (stripos($column->comment, "@textarea") !== false or stripos($type, "textarea") !== false) {

            return "echo \$form->textAreaGroup(\$model,'{$column->name}',
                          array(
                              'widgetOptions' => array(
                                      'editorOptions' =>array(
                                          'class' => 'span4',
                                          'rows' => 10,
                                          'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage()),"
            . $hmtloptions . "
                                      )
                                  )
                              )
                          )";
        } elseif (stripos($column->comment, '@redactor') !== false or stripos($type, 'redactor') !== false || $type == 'html' || $type == 'ckeditor' || $type == 'markdown' || $type == 'textarea') { // Start of CrudCode::generateActiveField code.
            if (preg_match('/^.*_redactor$/i', $column->name) || stripos($column->comment, 'redactor') !== false) {
                if($i18n){

                    $inputField = "echo \$form->textAreaGroup(\$model,'{$column->name}',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-12',
                        ),
                    " . $widget . "
                    )
                )";
                }
                else{
                    $inputField = "echo \$form->redactorGroup(\$model, '{$column->name}',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    )";
                }

                return $inputField;
            } elseif (preg_match('/^.*_html$/i', $column->name) || stripos($column->comment, '@html') !== false || $type == 'html') {
                $inputField = "echo \$form->html5EditorGroup(\$model,'{$column->name}',
                                array(
                                    'widgetOptions' => array(
                                        'editorOptions' => array(
                                            'class' => 'span4',
                                            'rows' => 10,
                                            //'height' => '200',
                                            'options' => array('color' => true,'lang'=>app()->getLanguage()),
                                            " . $hmtloptions . "
                                        ),
                                    )
                                )
                        )";
                return $inputField;
            } elseif (preg_match('/^.*_ckeditor$/i', $column->name) || stripos($column->comment, '@ckeditor') !== false || $type == 'ckeditor') {
                $inputField = "echo \$form->ckEditorGroup(\$model,'{$column->name}',
                                    array(
                                        'wrapperHtmlOptions' => array(
                                            'class' => 'col-sm-10',
                                        ),
                                        'widgetOptions' => array(
                                            'editorOptions' => array(
                                                'fullpage' => 'js:true',
                                                //'width' => '640',
                                                //'resize_maxWidth' => '640',
                                                '//resize_minWidth' => '320'
                                            )
                                        )
                                    )
                                )";
                return $inputField;
            } elseif (preg_match('/^.*_mk$/i', $column->name) || stripos($column->comment, '@markdown') !== false || $type == 'markdown') {
                $inputField = "echo \$form->markdownEditorGroup(\$model,'{$column->name}',
                                array(
                                    'widgetOptions' => array(
                                        'htmlOptions' => array('style'=>'height: 200px;')
                                    )
                                )
                            )";
                return $inputField;
            } elseif (preg_match('/^.*_area$/i', $column->name) || stripos($column->comment, '@textarea') !== false || $type == "textarea") {
                $inputField = "echo \$form->textAreaGroup(\$model, '{$column->name}',array(
                                    'wrapperHtmlOptions' => array(
                                        'class' => 'col-sm-5',
                                    ),
                                    'widgetOptions' => array(
                                        'htmlOptions' => array('rows' => 5),
                                        " . $hmtloptions . "
                                    )
                                ))";
                return $inputField;
            } elseif (strtoupper($column->dbType) == 'TEXT') {


                if (stripos($column->comment, "@redactor") !== false or stripos($type, "@redactor") !== false || $type == "redactor") {

                    $inputField = "echo \$form->redactorGroup(\$model,'{$column->name}',
                  array(
                      'widgetOptions' => array(
                              'editorOptions' =>array(
                                  'class' => 'span4',
                                  'rows' => 10,
                                  'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage()),
                                  " . $hmtloptions . "
                              )
                          )
                      )
                  )";
                    return $inputField;
                }

                if (stripos($column->comment, "@textarea") == false or stripos($type, "@textarea") == false || $type == 'textarea') {
                    $inputField = "echo \$form->textAreaGroup(\$model,'{$column->name}',
                  array(
                      'widgetOptions' => array(
                              'editorOptions' =>array(
                                  'class' => 'span4',
                                  'rows' => 10,
                                  'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage())
                                  " . $hmtloptions . "
                              )
                          )
                      )
                  )";
                    return $inputField;
                }
                return "echo \$form->textFieldGroup(\$model, '{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                " . $widget . "
                             //   'hint' => Yii::t('admin','Please, insert {$column->name}'),
                                'append' => 'Text'
                            )
                        )";
            } else
                $inputField = 'textField';
            $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
            $appendr = ($append == '') ? "Yii::t('admin','Text')" : "Yii::t('admin','$append')";
            return "echo \$form->textFieldGroup(\$model, '{$column->name}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' => {$hintr},
                                'append' => {$appendr}
                            )
                        )";
        } else {
            $passwordI18n = Yii::t('admin','password');
            $passwordI18n = (isset($passwordI18n) && $passwordI18n !== '') ? '|' . $passwordI18n : '';
            $pattern = '/^(password|pass|passwd|passcode' . $passwordI18n . ')$/i';
            if (preg_match($pattern, $column->name) || stripos($column->comment, '@password') !== false)
                $inputField = 'passwordFieldGroup';
            elseif (preg_match('/^.*_color$/i', $column->name) || stripos($column->comment, '@colorpicker') !== false)
                $inputField = 'colorpickerGroup';
            else
                $inputField = 'textFieldGroup';

            if (($column->type !== 'string' || $column->size === null) && $column->name != 'id') {
                $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
                $appendr = ($append == '') ? "Yii::t('admin','Text')" : "Yii::t('admin','$append')";
                return "echo \$form->{$inputField}(\$model, '{$column->name}',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>{$hintr},
                                'append' => {$appendr}
                            )
                        )";
            } else {
                $hintr = ($hint == '') ? "Yii::t('admin','Please, insert ').' $label'" : "Yii::t('admin','$hint')";
                $appendr = ($append == '') ? "Yii::t('admin','Text')" : "Yii::t('admin','$append')";
                return "echo \$form->{$inputField}(\$model, '{$column->name}', array(
                'maxlength' => {$column->size},
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 " . $widget . "
                                //'hint' => {$hintr},
                                'append' => {$appendr}
                                )
                      )";
            } // End of CrudCode::generateActiveField code.
        }
    }

    /**
     * Generates and returns the view source code line
     * to create the appropriate active input field based on
     * the model relation.
     * @param string $modelClass The model class name.
     * @param array $relation The relation details in the same format
     * used by {@link getRelations}.
     * @return string The source code line for the relation field.
     * @throws InvalidArgumentException If the relation type is not HAS_ONE, HAS_MANY or MANY_MANY.
     */
    public function generateActiveRelationField($modelClass, $relation)
    {
        $relationName = $relation[0];
        $relationType = $relation[1];
        $relationField = $relation[2]; // The FK.
        $relationModel = $relation[3];
        // The relation type must be HAS_ONE, HAS_MANY or MANY_MANY.
        // Other types (BELONGS_TO) should be generated by generateActiveField.
        if ($relationType != GxActiveRecord::HAS_ONE && $relationType != GxActiveRecord::HAS_MANY && $relationType != GxActiveRecord::MANY_MANY)
            throw new InvalidArgumentException(Yii::t('giix', 'The argument "relationName" must have a relation type of HAS_ONE, HAS_MANY or MANY_MANY.'));

        // Generate the field according to the relation type.
        switch ($relationType) {
            case GxActiveRecord::HAS_ONE:
                //return "echo \$form->dropDownListGroup(\$model, '{$relationName}', GxHtml::listDataEx({$relationModel}::model()->findAll()))";
                return "echo \$form->dropDownListGroup(\$model,'{$relationName}',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx({$relationModel}::model()->findAll()),

                                ),
                                 'hint' => Yii::t('admin','Please, select related {$relationName}'),
                            )
                        )";
                break;
            case GxActiveRecord::HAS_MANY:

                return "echo \$form->dropDownListGroup(\$model,'{$relationName}',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => GxHtml::listDataEx({$relationModel}::model()->findAll()),

                ),
                'hint' => Yii::t('admin','Please, select related {$relationName}'),
            )
                        )";
                break;
            case GxActiveRecord::MANY_MANY:
                return "echo \$form->dropDownListGroup(\$model,'{$relationName}',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => GxHtml::listDataEx({$relationModel}::model()->findAll()),
                        'htmlOptions' => array('multiple' => true),
                ),
                'hint' => Yii::t('admin','Please, select related {$relationName}'),
            )
                        )";
                //return "echo \$form->checkBoxListGroup(\$model, '{$relationName}', GxHtml::encodeEx(GxHtml::listDataEx({$relationModel}::model()->findAll()), false, true))";
                break;
        };
    }

    public function generateInputField($modelClass, $column)
    {
        return 'echo ' . parent::generateInputField($modelClass, $column);
    }

    /**
     * Generates and returns the view source code line
     * to create the appropriate attribute configuration for a CDetailView.
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the attribute.
     */
    public function generateDetailViewAttribute($modelClass, $column)
    {

        $comment = $column->comment;
        $comment_json = CJSON::decode($comment);
        $type = null;
        $mask = null;
        $options = null;
        $label = (isset($comment_json['label'])) ? $comment_json['label'] : $column->name;
        $editable= 'true';

        $data = "array(0=>Yii::t('admin','No'),1=>Yii::t('admin','Yes'))";
        if (isset($comment_json)) {
            if (array_key_exists('type', $comment_json)) {
                $type = $comment_json['type'];
            }
            if (array_key_exists('data', $comment_json)) {
//                $data = var_export($comment_json['data'], true);
                $data ="array(";
                foreach($comment_json['data'] as $key=>$item){
                    $data.="'$key'=>Yii::t('admin','$item'),";
                }
                $data.=')';
            }
            if (array_key_exists('editable', $comment_json)) {
                $editable = $comment_json['editable'];
            }
            if(array_key_exists('i18n', $comment_json) || array_key_exists('mask', $comment_json)){
                $editable='false';
            }
        }
        if ($type != null) {
            $label = "Image";
            if (array_key_exists('label', $comment_json)) {
                $label = $comment_json['label'];
            }

            if (stripos($type, 'recipeImg') !== false) {

                return "
                    array(
                            'label' => Yii::t('admin','" . $label . "'),
                            'type' => 'raw',
                            'value' => CHtml::image(\$model->_" . $column->name . "->getFileUrl('normal'), \$model->" . $column->name." , array('width'=> '100px')),
                        ),
                    array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => '{$column->name}',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
            }
        }

        if (!$column->isForeignKey) {
            if (preg_match('/^.*array/i', $column->comment) || (is_array($comment_json) and array_key_exists('data', $comment_json))) {
                return "array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => '{$column->name}',
                    'sortable' => false,
					'editable' => array(
                        'type'=>'select',
                        'apply'=>{$editable},
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('updateAttribute', 'model' => get_class(\$model)),
                        'source' => {$data},
                        'success' => \"" . $this->getAlertJS() . "\",
                            )
					)";
            }
            if (strtoupper($column->dbType) == 'TINYINT(1)'
                || strtoupper($column->dbType) == 'BIT'
                || strtoupper($column->dbType) == 'BOOL'
                || strtoupper($column->dbType) == 'BOOLEAN'
            ) {
                return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'select',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'source' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
                             'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
            } elseif (stripos($column->dbType, 'text') !== false) {
                if (preg_match('/^.*_redactor$/i', $column->name) || stripos($column->comment, '@redactor') !== false || $type == "redactor" ||
                    preg_match('/^.*_html$/i', $column->name) || stripos($column->comment, '@html') !== false || $type == "html" ||
                    preg_match('/^.*_ckeditor$/i', $column->name) || stripos($column->comment, '@ckeditor') !== false || $type == "ckeditor" ||
                    preg_match('/^.*_mk$/i', $column->name) || stripos($column->comment, '@markdown') !== false || $type == "markdown" ||
                    preg_match('/^.*_area$/i', $column->name) || stripos($column->comment, '@textarea') !== false || $type == "textarea"
                ) {
                    return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'textarea',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                             'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
                } else {
                    return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";

                }

            } elseif (stripos($column->dbType, 'varchar') !== false) {
                return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
            } elseif ((stripos($column->dbType, 'int') !== false || stripos($column->dbType, 'bigint') !== false ||
                    stripos($column->dbType, 'smallint') !== false ||
                    stripos($column->dbType, 'double') !== false ||
                    stripos($column->dbType, 'float') !== false ||
                    stripos($column->dbType, 'mediumint') !== false) && $column->name !== 'id'
            ) {
                return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
            } elseif (strtoupper($column->dbType) == 'DATE') {
                return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'date',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                             'success' => \"" . $this->getAlertJS() . "\",
                            'format' => 'yyyy-mm-dd',
                            'viewformat' => 'yyyy-mm-dd',
                            'options' => array(
                                'datepicker' => array(
                                    'language' => app()->getLanguage()
                                ),
                        )
					))";
            } else {

                return "'{$column->name}'";
            }
        } else {
            // Find the relation name for this column.
            $relation = $this->findRelation($modelClass, $column);
            $relationName = $relation[0];
            $relatedModelClass = $relation[3];
            $relatedControllerName = strtolower($relatedModelClass[0]) . substr($relatedModelClass, 1);

            return "array(
			'label' => Yii::t('admin','{$relationName}'),
			'type' => 'raw',
			'value' => \$model->{$relationName} !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx(\$model->{$relationName})), array('{$relatedControllerName}/view', 'id' => GxActiveRecord::extractPkValue(\$model->{$relationName}, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			)";
        }
    }

    public function getAlertJS()
    {
        return "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }";
    }

    /**
     * Generates and returns the view source code line
     * to create the CGridView column definition.
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the column definition.
     */
    public function generateGridViewColumn($modelClass, $column)
    {
        $comment = $column->comment;
        $comment_json = CJSON::decode($comment);
        $type = null;
        $mask = null;
        $options = null;
        $label = (isset($comment_json['label'])) ? $comment_json['label'] : $column->name;
        $editable = 'true';

        $data = "array(0=>Yii::t('admin','No'),1=>Yii::t('admin','Yes'))";
        if (isset($comment_json)) {
            if (array_key_exists('type', $comment_json)) {
                $type = $comment_json['type'];
            }
            if (array_key_exists('data', $comment_json)) {
//                $data = var_export($comment_json['data'], true);
                $data ="array(";
                foreach($comment_json['data'] as $key=>$item){
                    $data.="$key=>Yii::t('admin','$item'),";
                }
                $data.=')';
            }
            if (array_key_exists('editable', $comment_json)) {
                $editable = $comment_json['editable'];

            }
            if(array_key_exists('i18n', $comment_json) || array_key_exists('mask', $comment_json)){
                $editable='false';
            }

        }
        if (!$column->isForeignKey) {
                // Boolean or bit.
                if (preg_match('/^.*array/i', $column->comment) || (is_array($comment_json) and array_key_exists('data', $comment_json))) {
                    return "array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => '{$column->name}',
                    'filter' => {$data},
					'sortable' => true,
					'editable' => array(
                        'type'=>'select',
                        'apply'=>{$editable},
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('updateAttribute', 'model' => get_class(\$model)),
                        'source' => {$data},
                        'success' => \"" . $this->getAlertJS() . "\",
                            )
					)";
                }

                if ($type != null) {
                    $label = "Image";
                    if (array_key_exists('label', $comment_json)) {
                        $label = $comment_json['label'];
                    }

                    if (stripos($type, 'recipeImg') !== false) {

                        return "array(
                                'class' => 'bootstrap.widgets.TbImageColumn',
                                'header' => '" . $label . "',
                                'imagePathExpression' => '\$data->_" . $column->name . "->getFileUrl(\"normal\")',
                                'imageOptions'=>array('width'=>'100px')
                            )";
                    }
                }
                if (stripos($type, '@redactor') !== false) {
                    return "array(
                        'class' => 'bootstrap.widgets.TbEditableColumn',
                        'name' => '{$column->name}',
                        'sortable' => true,
                        'editable' => array(
                                'type'=>'textarea',
                                'apply'=>{$editable},
                                'url' => array('updateAttribute', 'model' => get_class(\$model)),
                                'placement' => 'right',
                                'inputclass' => 'span3',
                                 'success' => \"" . $this->getAlertJS() . "\",
                            )
                        )";
                } else

                    if (strtoupper($column->dbType) == 'TINYINT(1)'
                        || strtoupper($column->dbType) == 'BIT'
                        || strtoupper($column->dbType) == 'BOOL'
                        || strtoupper($column->dbType) == 'BOOLEAN'
                    ) {
                        return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'value' => '(\$data->{$column->name} == 0) ? Yii::t(\"admin\", \"No\") : Yii::t(\"admin\", \"Yes\")',
					'filter' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
					'sortable' => true,
					'editable' => array(
                            'type'=>'select',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'source' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
                             'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
                    } elseif (stripos($column->dbType, 'text') !== false) {
                        if (preg_match('/^.*_redactor$/i', $column->name) || stripos($column->comment, '@redactor') !== false || $type == "redactor" ||
                            preg_match('/^.*_html$/i', $column->name) || stripos($column->comment, '@html') !== false || $type == "html" ||
                            preg_match('/^.*_ckeditor$/i', $column->name) || stripos($column->comment, '@ckeditor') !== false || $type == "ckeditor" ||
                            preg_match('/^.*_mk$/i', $column->name) || stripos($column->comment, '@markdown') !== false || $type == "markdown" ||
                            preg_match('/^.*_area$/i', $column->name) || stripos($column->comment, '@textarea') !== false || $type == "textarea"
                        ) {
                            return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'textarea',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                             'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
                        } else {
                            return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";

                        }

                    } elseif (stripos($column->dbType, 'varchar') !== false) {

                        return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
                    } elseif ((stripos($column->dbType, 'int') !== false || stripos($column->dbType, 'bigint') !== false ||
                            stripos($column->dbType, 'smallint') !== false ||
                            stripos($column->dbType, 'double') !== false ||
                            stripos($column->dbType, 'float') !== false ||
                            stripos($column->dbType, 'mediumint') !== false) && $column->name !== 'id'
                    ) {
                        return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
                    } elseif (strtoupper($column->dbType) == 'DATE') {
                        return "array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => '{$column->name}',
					'sortable' => true,
					'editable' => array(
                            'type'=>'date',
                            'apply'=>{$editable},
                            'url' => array('updateAttribute', 'model' => get_class(\$model)),
                            'placement' => 'right',
                             'success' => \"" . $this->getAlertJS() . "\",
                            'format' => 'yyyy-mm-dd',
                            'viewformat' => 'yyyy/mm/dd',
                            'options' => array(
                                'datepicker' => array(
                                    'language' => app()->getLanguage()
                                ),
                        )
					))";

                    } else {
                        return "array('name'=> '{$column->name}',
                  'sortable' => false)
                  ";
                    } // Common column.

            } else { // FK.
                // Find the related model for this column.
                $relation = $this->findRelation($modelClass, $column);
                $relationName = $relation[0];
                $relatedModelClass = $relation[3];
                /*return "array(
				'name'=>'{$column->name}',
				'value'=>'GxHtml::valueEx(\$data->{$relationName})',
				'filter'=>GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
				)";*/
            return "array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => '{$column->name}',
                'filter' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                'sortable' => true,
                'editable' => array(
                    'type'=>'select',
                    'apply'=>{$editable},
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'url' => array('updateAttribute', 'model' => get_class(\$model)),
                    'source' => GxHtml::listDataEx({$relatedModelClass}::model()->findAll()),
                         'success' => \"" . $this->getAlertJS() . "\",
                        )
					)";
            }
        }

        /**
         * Generates and returns the view source code line
         * to create the advanced search.
         * @param string $modelClass The model class name.
         * @param CDbColumnSchema $column The column.
         * @return string The source code line for the column definition.
         */
        public function generateSearchField($modelClass, $column)
        {
            if (!$column->isForeignKey) {
                // Boolean or bit.
                if (strtoupper($column->dbType) == 'TINYINT(1)'
                    || strtoupper($column->dbType) == 'BIT'
                    || strtoupper($column->dbType) == 'BOOL'
                    || strtoupper($column->dbType) == 'BOOLEAN'
                )
                    return "echo \$form->dropDownListGroup(\$model, '{$column->name}', array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')), array('prompt' => Yii::t('admin','All')))";
                else // Common column. generateActiveField method will add 'echo' when necessary.
                    return $this->generateActiveField($this->modelClass, $column);
            } else { // FK.
                // Find the related model for this column.
                $relation = $this->findRelation($modelClass, $column);
                $relatedModelClass = $relation[3];
                return "echo \$form->dropDownListGroup(\$model, '{$column->name}', GxHtml::listDataEx({$relatedModelClass}::model()->findAll()), array('prompt' => Yii::t('admin','All')))";
            }
        }



        /**
         * Generates and returns the array (as a PHP source code string)
         * to collect the MANY_MANY related data from the POST.
         * @param string $modelClass The model class name.
         * @return string The source code to collect the MANY_MANY related
         * data from the POST.
         */
        public
        function generateGetPostRelatedData($modelClass, $indent = 1)
        {
            $result = array();
            $relations = $this->getRelations($modelClass);
            foreach ($relations as $relationData) {
                $relationName = $relationData[0];
                $relationType = $relationData[1];
                if ($relationType == GxActiveRecord::MANY_MANY)
                    $result[$relationData[0]] = "php:\$_POST['{$modelClass}']['{$relationName}'] === '' ? null : \$_POST['{$modelClass}']['{$relationName}']";
            }
            return GxCoreHelper::ArrayToPhpSource($result, $indent);
        }

        /**
         * Checks whether this AR has a MANY_MANY relation.
         * @param string $modelClass The model class name.
         * @return boolean Whether this AR has a MANY_MANY relation.
         */
        public
        function hasManyManyRelation($modelClass)
        {
            $relations = $this->getRelations($modelClass);
            foreach ($relations as $relationData) {
                if ($relationData[1] == GxActiveRecord::MANY_MANY) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Finds the relation of the specified column.
         * Note: There's a similar method in the class GxActiveRecord.
         * @param string $modelClass The model class name.
         * @param CDbColumnSchema $column The column.
         * @return array The relation. The array will have 3 values:
         * 0: the relation name,
         * 1: the relation type (will always be GxActiveRecord::BELONGS_TO),
         * 2: the foreign key (will always be the specified column),
         * 3: the related active record class name.
         * Or null if no matching relation was found.
         */
        public
        function findRelation($modelClass, $column)
        {
            if (!$column->isForeignKey)
                return null;
            $relations = GxActiveRecord::model($modelClass)->relations();
            // Find the relation for this attribute.
            foreach ($relations as $relationName => $relation) {
                // For attributes on this model, relation must be BELONGS_TO.
                if ($relation[0] == GxActiveRecord::BELONGS_TO && $relation[2] == $column->name) {
                    return array(
                        $relationName, // the relation name
                        $relation[0], // the relation type
                        $relation[2], // the foreign key
                        $relation[1] // the related active record class name
                    );
                }
            }
            // None found.
            return null;
        }

        /**
         * Returns all the relations of the specified model.
         * @param string $modelClass The model class name.
         * @return array The relations. Each array item is
         * a relation as an array, having 3 items:
         * 0: the relation name,
         * 1: the relation type,
         * 2: the foreign key,
         * 3: the related active record class name.
         * Or an empty array if no relations were found.
         */
        public
        function getRelations($modelClass)
        {
            $relations = GxActiveRecord::model($modelClass)->relations();
            $result = array();
            foreach ($relations as $relationName => $relation) {
                $result[] = array(
                    $relationName, // the relation name
                    $relation[0], // the relation type
                    $relation[2], // the foreign key
                    $relation[1] // the related active record class name
                );
            }
            return $result;
        }

        public
        function prepare()
        {
            $this->files = array();
            $templatePath = $this->templatePath;
            $controllerTemplateFile = $templatePath . DIRECTORY_SEPARATOR . 'controller.php';
            $files = scandir($templatePath);


            if (Yii::getPathOfAlias($this->modulePath) != '') {
                $path_controller = Yii::getPathOfAlias($this->modulePath) . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . ucwords($this->controller) . 'Controller.php';
            } else {
                $path_controller = $this->controllerFile;
            }

            $this->files[] = new CCodeFile(
                $path_controller,
                $this->render($controllerTemplateFile)
            );


            foreach ($files as $file) {
                if (is_file($templatePath . '/' . $file) && CFileHelper::getExtension($file) === 'php' && $file !== 'controller.php') {
                    if (Yii::getPathOfAlias($this->modulePath) != '') {
                        $path = Yii::getPathOfAlias($this->modulePath) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $this->controller . DIRECTORY_SEPARATOR . $file;
                    } else {
                        $path = $this->viewPath . DIRECTORY_SEPARATOR . $file;
                    }
                    $this->files[] = new CCodeFile($path,
                        $this->render($templatePath . '/' . $file)
                    );
                }

            }
            if (is_file($templatePath . '/' . $file) && CFileHelper::getExtension($file) === 'php' && $file == 'controller.php') {

            }
        }


        /**
         * Returns the message to be displayed when the newly generated code is saved successfully.
         * #MethodTracker
         * This method overrides {@link CrudCode::successMessage}, from version 1.1.7 (r3135). Changes:
         * <ul>
         * <li>Custom giix success message.</li>
         * </ul>
         * @return string The message to be displayed when the newly generated code is saved successfully.
         */
        public
        function successMessage()
        {
            /*<ul style="list-style-type: none; padding-left: 0;">
        <li><img src="http://giix.org/icons/love.png"> Show how you love giix on <a href="http://www.yiiframework.com/forum/index.php?/topic/13154-giix-%E2%80%94-gii-extended/">the forum</a> and on its <a href="http://www.yiiframework.com/extension/giix">extension page</a></li>
        <li><img src="http://giix.org/icons/vote.png"> Upvote <a href="http://www.yiiframework.com/extension/giix">giix</a></li>
        <li><img src="http://giix.org/icons/powered.png"> Show everybody that you are using giix in <a href="http://www.yiiframework.com/forum/index.php?/topic/19226-powered-by-giix/">Powered by giix</a></li>
        <li><img src="http://giix.org/icons/donate.png"> <a href="http://giix.org/">Donate</a></li>
    </ul>*/
            return <<<EOM
<p><strong>Sweet!</strong></p>

<p><b>Power by iReevo</b></p>
<p style="margin: 2px 0; position: relative; text-align: right; top: -15px; color: #668866;">icons by <a href="http://www.famfamfam.com/lab/icons/silk/" style="color: #668866;">famfamfam.com</a></p>
EOM;
        }

    }