<?php

/**
 * GiixCrudGenerator class file.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 * @link http://giix.org/
 * @copyright Copyright &copy; 2010-2011 Rodrigo Coelho
 * @license http://giix.org/license/ New BSD License
 */

/**
 * GiixCrudGenerator is the controller for giix crud generator.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 */
class GiixCrudGenerator extends CCodeGenerator {

	public $codeModel = 'ext.giix-core.giixCrud.GiixCrudCode';

    /**
     * The code generation action.
     * This is the action that displays the code generation interface.
     * Child classes mainly need to provide the 'index' view for collecting user parameters
     * for code generation.
     */
    public function actionIndex()
    {

        $model=$this->prepare();
        if($model->files!=array() && isset($_POST['generate'], $_POST['answers']))
        {

            $model->answers=$_POST['answers'];
            $model->status=$model->save() ? CCodeModel::STATUS_SUCCESS : CCodeModel::STATUS_ERROR;
        }

        $this->render('index',array(
            'model'=>$model,
        ));
    }
    public function getModules() {

        $system_modules = app()->getModules();
        $modules=array();
        foreach($system_modules as $name=>$class){
            if($name!='ycm' && $name!='auth' && $name!='user' && $name!='gii' ){
                $modules[]=$name;
            }
        }
        return $modules;
    }


    /**
	 * Returns the model names in an array.
	 * Only non abstract and subclasses of GxActiveRecord models are returned.
	 * The array is used to build the autocomplete field.
	 * @return array The names of the models
	 */
	protected function getModels() {
        $modules = app()->getModules();
        //ddump($modules);
        $models = array();
		foreach($modules as $name=>$class){
            if($name!='ycm' && $name!='auth' && $name!='user' && $name!='gii' && $name != 'contentViewsCounter'){
                $files = scandir(Yii::getPathOfAlias('application.modules.'.$name.'.models'));
                foreach ($files as $file) {
                    if ($file[0] !== '.' && CFileHelper::getExtension($file) === 'php') {
                        $fileClassName = substr($file, 0, strpos($file, '.'));
                        if (class_exists($fileClassName) && is_subclass_of($fileClassName, 'GxActiveRecord')) {
                            $fileClass = new ReflectionClass($fileClassName);
                            if (!$fileClass->isAbstract())
                                $models[] = $fileClassName;
                        }
                    }
                }
            }

        }

		return $models;
	}

}