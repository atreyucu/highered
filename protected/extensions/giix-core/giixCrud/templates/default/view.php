<?php
/**
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 */
?>
<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>


<?php echo "<?php\n"; ?>
<?php
$submenu = null;
$has_seo = false;
 foreach($this->tableSchema->columns as $column){

    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);

    if(isset($comment_json))
    {
        if (array_key_exists('seo', $comment_json)) {
            $has_seo = true;
        }
        if(array_key_exists('breadcrumbs', $comment_json))
        {

            $breadcrumbs_temp = $comment_json['breadcrumbs'];
            if(array_key_exists('verbose_name', $breadcrumbs_temp))
            {
                $submenu = $breadcrumbs_temp["verbose_name"];
            }
            elseif(array_key_exists('submenu', $breadcrumbs_temp))
            {

                $submenu = $breadcrumbs_temp["submenu"];
                break;
            }


        }
    }
 }


?>
$this->breadcrumbs = array(
        $model->adminNames[3] <?php if($submenu != null) echo ",Yii::t('sideMenu', '".$submenu."') " ?>  => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"<?php echo $submenu?>"))
);

?>

<?php echo '<?php'; ?> $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(
<?php

//--------------ESTE CODIG ME SIRVE PARA VER SI PUEJDO GENERAR LA INTERFAZ PARA LA RELATIONS------------------
//require_once dirname(__FILE__).('/../../../giixModel/GiixModelCode.php');
//$kaka = new GiixModelCode();
////$kaka->prepare();
//$b = $kaka->generateRelations();
//ddump($b[$this->modelClass]);
////--------------FINNNNN  ESTE CODIG ME SIRVE PARA VER SI PUEJDO GENERAR LA INTERFAZ PARA LA RELATIONS------------------

foreach ($this->tableSchema->columns as $column)
    if($column->name!='id' && $column->name!='created' && $column->name!='updated' && $column->name!='owner')
		echo $this->generateDetailViewAttribute($this->modelClass, $column) . ",\n";
?>
	),
)); ?>
<?php if($has_seo):?>
<?php echo "<?php \$model->seoViewWidget(); ?>"?>
<?php endif?>

<div class="form-actions" style="text-align: right">
 <?php echo "   <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>";?>
<?php
    echo "<?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?>";
    echo "<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>\$model->id),array('class'=>'btn btn-primary'));?>";
    echo "<?php if(user()->isAdmin):?>\n";
    echo "<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>\$model->id),array('class'=>'btn btn-danger delete'));?>";
    echo '<?php endif?>'
?>
</div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo "<?php echo Yii::t('admin','Data was saved with success')?>";?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
echo "<?php\n";
?>
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
<?php
echo "?>";
?>
