<?php
/**
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>


<?php echo "<?php\n"; ?>


<?php
$submenu = null;
 foreach($this->tableSchema->columns as $column){

    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);

    if(isset($comment_json))
    {
        if(array_key_exists('breadcrumbs', $comment_json))
        {

            $breadcrumbs_temp = $comment_json['breadcrumbs'];
            if(array_key_exists('verbose_name', $breadcrumbs_temp))
            {
                $submenu = $breadcrumbs_temp["verbose_name"];
            }
            elseif(array_key_exists('submenu', $breadcrumbs_temp))
            {

                $submenu = $breadcrumbs_temp["submenu"];
                break;
            }


        }
    }
 }


?>



$this->breadcrumbs = array(
    $model->adminNames[3] <?php if($submenu != null) echo ",Yii::t('sideMenu', '".$submenu."') " ?> => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"<?php echo $submenu?>"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

