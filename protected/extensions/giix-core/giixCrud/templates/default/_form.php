<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>

<?php $ajax = ($this->enable_ajax_validation) ? 'true' : 'false'; ?>

<?php echo '<?php '; ?>
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => '<?php echo $this->class2id($this->modelClass); ?>-form',
<?php if ($this->enable_ajax_validation): ?>
    'enableClientValidation'=>true,
<?php endif; ?>

));
<?php echo '?>'; ?>
<?php $model = CActiveRecord::model($this->modelClass);?>
<?php $has_seo = false;?>
<?php $relation = array(); ?>

<?php foreach ($this->tableSchema->columns as $column): ?>
    <?php
    if ($column->isForeignKey) {
        $relations[] = $this->findRelation($this->modelClass, $column);
    }

    ?>
    <?php $comment = $column->comment;

    $comment_json = CJSON::decode($comment);
    try {
        if (is_array($comment_json) and array_key_exists('seo', $comment_json)) {

            $has_seo = true;
        }
    }
    catch(Exception $e)
    {
        ddump($comment);
    }
    ?>

    <?php if (!$column->autoIncrement && ($column->name!='id' && $column->name!='created' && $column->name!='updated' && $column->name!='owner')): ?>

        <?php if (($model instanceof I18NAdapterAbstract) and (in_array($column->name, $model->i18nAttributes()))):?>
                <?php echo "<?php \$this->beginWidget('i18n.extensions.widgets.LanguageWidget', array(
                'model' => \$model,
                'attribute' => '".$column->name."' )); ?>\n"
                ?>
                <?php echo "<?php " . $this->generateActiveField($this->modelClass, $column, true) . "; ?>\n";?>

                <?php echo "<?php \$this->endWidget(); ?>\n"
                ?>
            <?php else :?>
                <?php echo "<?php " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n";?>
            <?php endif?>
<?php endif; ?>
<?php endforeach; ?>
<?php if($has_seo):?>
    <?php echo "<?php \$model->seoFormWidget(\$form); ?>";
endif?>


<?php
echo "<div class='form-actions'>";
echo "   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>";
echo "   <?php \$this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        \$this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php \$this->endWidget(); ?>

    <?php if(isset(\$model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>\$model->id),array('class'=>'btn btn-danger'));
    }?>";?>
<?php if (count($relations) > 0): ?>

    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo "<?php echo Yii::t('admin', 'Add'); ?>" ?>
            <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
            <?php foreach ($relations as $relation): ?>
                <li>
                    <?php echo "<?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . '" . $relation[3] . "'), array('/backend/" . $relation[0] . "/create', 'id' => \$model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>"; ?>
                </li>
            <?php endforeach ?>
        </ul>
    </div>

<?php endif ?>

</div>





