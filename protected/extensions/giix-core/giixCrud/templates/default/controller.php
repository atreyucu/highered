<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 * Improve iReevo
 */
?>

<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>


<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass; ?> {

<?php
$authpath = 'ext.giix-core.giixCrud.templates.default.auth.';
Yii::app()->controller->renderPartial($authpath . $this->authtype);
?>

public function actionView($id) {
    $model = $this->loadModel($id, '<?php echo $this->modelClass; ?>');
    $this->render('view', array(
    'model' => $model,
    ));
}

public function actionCreate() {
    $model = new <?php echo $this->modelClass; ?>;


<?php if ($this->enable_ajax_validation): ?>
    $this->performAjaxValidation($model, '<?php echo $this->class2id($this->modelClass) ?>-form');
<?php endif; ?>

<?php foreach ($this->getTableSchema()->columns as $column)
            {

    $comment = $column->comment;
    $json = json_decode($comment, 6);
    if (isset($json)) {
        if (array_key_exists('rows', $json)) {
            echo " if(\$model->count() >= ";
            echo $json['rows'] . ")\n";
            echo "\$this->redirect(array('admin'));";
            break;
        }
    }
}

?>
    if (isset($_POST['<?php echo $this->modelClass; ?>'])) {
        $model->setAttributes($_POST['<?php echo $this->modelClass; ?>']);
        if ($model->save()) {
            if (Yii::app()->getRequest()->getIsAjaxRequest())
                Yii::app()->end();
            else{
                Yii::app()->user->setFlash('success',Yii::t('admin','Success, item was saved.'));
                if (Yii::app()->request->getParam('id'))
                {
                    if(Yii::app()->request->getParam('action')=='create')
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                else
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                }
                else
                    $this->redirect(array('admin'));
            }
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving item.'));
        }
    }

    $this->render('create', array( 'model' => $model));
}

public function actionUpdate($id) {
    $model = $this->loadModel($id, '<?php echo $this->modelClass; ?>');

<?php if ($this->enable_ajax_validation): ?>
    $this->performAjaxValidation($model, '<?php echo $this->class2id($this->modelClass) ?>-form');
<?php endif; ?>

    if (isset($_POST['<?php echo $this->modelClass; ?>'])) {
        $model->setAttributes($_POST['<?php echo $this->modelClass; ?>']);
        if ($model->save()) {
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the changes were saved.'));
            $this->redirect(array('admin'));
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving the item.'));
        }
    }
    $this->render('update', array(
        'model' => $model,
    ));
}

public function actionDelete($id)
{
    if(isset($id)){
        if($this->loadModel($id,"<?php echo $this->modelClass; ?>")->delete()){
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the item was deleted.'));
            $this->redirect(array('admin'));
        }
        else{
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, exist a native error to delete the item: '.$id.', to resolve this problem, please contact with the database administrator.'));
        }
    }
    else {
        Yii::app()->user->setFlash('error',Yii::t('admin','Error, the item '.$id.' is not defined.'));
    }
}

public function actionAdmin() {
    $model = new <?php echo $this->modelClass; ?>('search');
    $model->unsetAttributes();

    if (isset($_GET['<?php echo $this->modelClass; ?>']))
        $model->setAttributes($_GET['<?php echo $this->modelClass; ?>']);

    $this->render('admin', array(
        'model' => $model,
    ));
}

public function actionUpdateAttribute($model)
{
    if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
        Yii::import("bootstrap.widgets.TbEditableSaver");
        $editableSaver = new TbEditableSaver($model);
        $editableSaver->update();
        app()->end();
    }
}

}