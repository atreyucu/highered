<?php
/**
 * Author : Yusnel Rojas Garcia <yrojass@gmail.com>
 * User   : one
 * Date   : 8/21/12
 * Time   : 1:59 AM
 */
class EmailLoader implements Twig_LoaderInterface
{

    /**
     * Gets the source code of a template, given its name.
     *
     * @param  string $name The name of the template to load
     *
     * @return string The template source code
     */
    public function getSource($name)
    {
        $email = Email::model()->findByPurpose($name);
        if (null === $email) {
            throw new Twig_Error_Loader(sprintf('Email "%s" is not defined.', $name));
        }
        return $email->content;
    }

    /**
     * Gets the cache key to use for the cache for a given template name.
     *
     * @param  string $name The name of the template to load
     *
     * @return string The cache key
     */
    public function getCacheKey($name)
    {
        return $name;
    }

    /**
     * Returns true if the template is still fresh.
     *
     * @param string    $name The template name
     * @param timestamp $time The last modification time of the cached template
     */
    public function isFresh($name, $time)
    {
        return false;
    }
}
