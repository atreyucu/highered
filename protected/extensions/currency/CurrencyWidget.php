<?php

class CurrencyWidget extends CWidget
{
    /**
     * Assets package ID.
     */
    const PACKAGE_ID = 'currency-widget';

    /**
     * @var string path to assets
     */
    protected $assetsPath;

    /**
     * @var string URL to assets
     */
    protected $assetsUrl;

    /**
     * @var array currency options
     */
    public $options = array(
        'symbol' => '$',
        'showSymbol' => false,
        'symbolStay' => false,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2,
        'defaultZero' => true,
        'allowZero' => true,
        'allowNegative' => false,
    );

    /**
     * @var array currency options
     */
    public $paramOptions = 'currency.defaults';

    /**
     * @var string select selector for jQuery
     */
    public $selector = '.currency';

    /**
     * Init widget
     */
    public function init()
    {
        parent::init();
        if ($this->assetsPath === null) {
            $this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        }
        if ($this->assetsUrl === null) {
            $this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
        }


        $param = param($this->paramOptions);
        if ($param) {
            $this->options = CMap::mergeArray($this->options, $param);
        }

        $this->registerClientScript();
    }

    /**
     * Register CSS and scripts.
     */
    protected function registerClientScript()
    {
        $cs = Yii::app()->clientScript;
        if (!isset($cs->packages[self::PACKAGE_ID])) {
            $cs->packages[self::PACKAGE_ID] = array(
                'basePath' => $this->assetsPath,
                'baseUrl' => $this->assetsUrl,
                'js' => array(
                    'js/jquery.maskMoney.js',
                ),
                'depends' => array(
                    'jquery',
                ),
            );
        }
        $cs->registerPackage(self::PACKAGE_ID);
        $cs->registerScript(
            __CLASS__ . '#' . $this->id,
            'jQuery(' . CJavaScript::encode($this->selector) . ').maskMoney(' . CJavaScript::encode($this->options) . ');',
            CClientScript::POS_READY
        );
    }
}