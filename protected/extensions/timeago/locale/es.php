<?php
// Español
return array(
    // environ ~= about, it's optional
    'prefixAgo' => "hace",
    'prefixFromNow' => "dentro de",
    'suffixAgo' => NULL,
    'suffixFromNow' => NULL,
    'seconds' => "unos segundos",
    'minute' => "un minuto",
    'minutes' => "%d minutos",
    'hour' => "una hora",
    'hours' => "%d horas",
    'day' => "un día",
    'days' => "%d días",
    'month' => "un mes",
    'months' => "%d meses",
    'year' => "un año",
    'years' => "%d años",
    'wordSeparator' => ' ',
    'rules' => array(),
    'numbers' => array(),
);