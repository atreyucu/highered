<?php
class IRContentButtons extends CWidget
{
    /**
     * @var array
     * <a class="btn btn-large tip-bottom" title="Manage Comments">
     * <i class="icon-comment"></i>
     * <span class="label label-important">5</span>
     * </a>
     */


    public $items = array();

    public function renderItem($item)
    {

        if (!isset($item['visible']) || $item['visible']) {
            $title = $item['title'];
            if ($title == null) {
                $title = '';
            }

            $icon = $item['icon'];
            if ($icon == null) {
                $icon = 'hand-right';
            }

            $url = $item['url'];
            if ($url == null) {
                $url = '#';
            }

            $content = "<i class=\"icon-{$icon}\"></i>";
            $label = $item['label'];
            if ($label != null) {
                $labelType = $item['labelType'];
                if ($labelType == null) {
                    $labelType = 'important';
                }
                $content .= "<span class=\"label label-{$labelType}\">{$label}</span>";
            }
            echo CHtml::link($content, $url, array(
                'class' => 'btn btn-large tip-bottom',
                'title' => $title,
            ));
        }
    }

    public function init()
    {
        echo CHtml::openTag('div', array('class' => 'btn-group'));
    }

    public function run()
    {
        foreach ($this->items as $item)
            $this->renderItem($item);
        echo CHtml::closeTag('div');
    }
}