<?php
Yii::import('application.extensions.bootstrap.widgets.TbMenu');

class IRUserMenu extends TbMenu
{
    public $htmlOptions = array(
        'class' => 'btn-group'
    );
    public $itemCssClass = 'btn btn-inverse';


    protected function renderMenuItem($item)
    {
        if (isset($item['icon'])) {
            if (strpos($item['icon'], 'icon') === false) {
                $pieces = explode(' ', $item['icon']);
                $item['icon'] = 'icon-' . implode(' icon-', $pieces);
            }

            $item['label'] = '<i class="icon icon ' . $item['icon'] . '"></i> <span class="text">' . $item['label'] . '</span>';
        } else {
            $item['label'] = '<span class="text">' . $item['label'] . '</span>';
        }

        if (!isset($item['linkOptions'])) {
            $item['linkOptions'] = array();
        }

        if (isset($item['items']) && !empty($item['items'])) {
            if (empty($item['url'])) {
                $item['url'] = '#';
            }

            if (isset($item['linkOptions']['class'])) {
                $item['linkOptions']['class'] .= ' dropdown-toggle';
            } else {
                $item['linkOptions']['class'] = 'dropdown-toggle';
            }

            $item['linkOptions']['data-toggle'] = 'dropdown';
            $item['label'] .= ' <span class="caret"></span>';
        }

        if (isset($item['url'])) {
            return CHtml::link($item['label'], $item['url'], $item['linkOptions']);
        } else {
            return $item['label'];
        }
    }
}