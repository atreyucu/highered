<?php
Yii::import('zii.widgets.CMenu');

class IRSideMenu extends CMenu
{
    public function init()
    {
        $this->encodeLabel = false;
        $this->activateParents = true;
        $this->submenuHtmlOptions = array('class' => 'submenu');

        foreach ($this->items as $k => $v) {
            if (!isset($v['icon']))
                $this->items[$k]['icon'] = 'hand-right';
        }

        parent::init();
    }

    protected function renderMenuItem($item, $position)
    {
        if ($position == 1) {
            if (isset($item['icon'])) {
                $label = CHtml::tag('i', array('class' => 'glyphicon glyphicon-' . $item['icon'])) . '</i> <span>' . $item['label'] . '</span><i class="arrow fa fa-chevron-right"></i>';
            } else {
                $label = '<span>' . $item['label'] . '</span></span><i class="arrow fa fa-chevron-right"></i>';
            }
        } else {
            if (isset($item['icon'])) {
                $label = CHtml::tag('i', array('class' => 'glyphicon glyphicon-' . $item['icon'])) . '</i> <span>' . $item['label'] . '</span>';
            } else {
                $label = '<span>' . $item['label'] . '</span></span>';
            }
        }


        if (isset($item['url'])) {
            $label = $this->linkLabelWrapper === null ? $label : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $label);
            return CHtml::link($label, $item['url'], isset($item['linkOptions']) ? $item['linkOptions'] : array());
        } else
            return CHtml::tag('span', isset($item['linkOptions']) ? $item['linkOptions'] : array(), $label);


        return parent::renderMenuItem($item);
    }

    protected function renderMenuRecursive($items)
    {
        $count = 0;
        $n = count($items);


        foreach ($items as $item) {
            $count++;
            $options = isset($item['itemOptions']) ? $item['itemOptions'] : array();
            $class = array();
            if ($item['active'] && $this->activeCssClass != '') {
                $class[] = $this->activeCssClass;
            }
            if (isset($item['items']) && count($item['items'])) {
                $class[] = 'submenu';
                if ($item['active']) {
                    $class[] = 'open';
                }
            }


            if ($count === 1 && $this->firstItemCssClass !== null)
                $class[] = $this->firstItemCssClass;
            if ($count === $n && $this->lastItemCssClass !== null)
                $class[] = $this->lastItemCssClass;
            if ($this->itemCssClass !== null)
                $class[] = $this->itemCssClass;
            if ($class !== array()) {
                if (empty($options['class']))
                    $options['class'] = implode(' ', $class);
                else
                    $options['class'] .= ' ' . implode(' ', $class);
            }

            echo CHtml::openTag('li', $options);
            (isset($item['items']) && count($item['items'])) ? $position = 1 : $position = 2;
            $menu = $this->renderMenuItem($item, $position);

            if (isset($this->itemTemplate) || isset($item['template'])) {
                $template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template, array('{menu}' => $menu));
            } else
                echo $menu;

            if (isset($item['items']) && count($item['items'])) {

                echo "\n" . CHtml::openTag('ul', isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions) . "\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag('ul') . "\n";
            }

            echo CHtml::closeTag('li') . "\n";
        }
    }
}