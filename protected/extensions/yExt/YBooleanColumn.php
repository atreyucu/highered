<?php

Yii::import('application.extensions.bootstrap.widgets.TbDataColumn');
Yii::import('application.extensions.bootstrap.helpers.TbHtml');
/**
 * Class YBooleanColumn
 * Para las columnas booleanas
 */
class YBooleanColumn extends AdminDataColumn
{
    public $filter = true;

    public function init()
    {
        $gridId = $this->grid->getId();
        /** @var $model YPermisionsBehavior|GxActiveRecord */
        $model = $this->grid->dataProvider->model;

        $this->filter = $this->filter ? lookupYesNo() : false;
        $this->type = 'raw';
        $this->htmlOptions = CMap::mergeArray(array(
            'class' => 'button-column',
        ), $this->htmlOptions);
        grid_link(".toogle-link", $gridId, 'toogle_link');
    }

    protected function renderDataCellContent($row, $data)
    {
        $value = $data->{$this->name};
        $new_value = !$value;
        if ($data->can_set_value($this->name, $new_value))
            echo CHtml::link(TbHtml::icon($data->{$this->name} ? "ok" : "remove"), ycm()->url_toogle($data, $this->name), array('class' => 'toogle-link'));
        else
            echo TbHtml::icon($data->{$this->name} ? "ok" : "remove");
    }
}