<?php

Yii::import('application.extensions.bootstrap.widgets.TbDataColumn');
Yii::import('application.extensions.bootstrap.helpers.TbHtml');
/**
 * Class YRelatedColumn
 * Para las columnas de llave foranea
 */
class YRelatedColumn extends AdminDataColumn
{
    public $relation;

    public function init()
    {
        /** @var $model CActiveRecord */
        $model = $this->grid->dataProvider->model;
        $relations = $model->relations();
        $this->name = $relations[$this->relation][2];
        if (user()->checkAccess($relations[$this->relation][1] . '.update')) {
            $this->value = "CHtml::link(GxHtml::valueEx(\$data->{$this->relation}), ycm()->url_update(\$data->{$this->relation}))";
        } else {
            $this->value = "GxHtml::valueEx(\$data->{$this->relation})";
        }
        $this->filter = $model->elementos($this->name, $this->relation);
        $this->type = 'raw';
    }
}