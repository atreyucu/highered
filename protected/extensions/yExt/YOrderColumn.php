<?php

Yii::import('zii.widgets.grid.CGridColumn');
class YOrderColumn extends CGridColumn
{
    public $cssClass = 'order_link';

    public function __construct($grid)
    {
        $this->visible = user()->checkAccess($this->grid->dataProvider->modelClass . '.order');
        parent::__construct($grid);
    }

    public function init()
    {
        /** @var $model CActiveRecord */
        $model = $this->grid->dataProvider->model;
        $gridId = $this->grid->getId();
        $this->htmlOptions = array(
            'class' => 'button-column',
        );
        $this->header = $model->getAttributeLabel($model->order->attr);


        Yii::app()->clientScript->registerCoreScript('jquery');
        grid_link(".{$this->cssClass}", $gridId, $this->cssClass);
        return parent::init();
    }

    public function renderDataCellContent($row, $data)
    {
        $ajaxUrl = ycm()->url_order('up', $data);
        $up = $data->hasUp() ? CHtml::link(TbHtml::icon('arrow-up'), $ajaxUrl, array('class' => $this->cssClass)) : TbHtml::icon('arrow-up');

        $ajaxUrl = ycm()->url_order('down', $data);
        $down = $data->hasDown() ? CHtml::link(TbHtml::icon('arrow-down'), $ajaxUrl, array('class' => $this->cssClass)) : TbHtml::icon('arrow-down');
        echo "$up $down";
    }

}

?>