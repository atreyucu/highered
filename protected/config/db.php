<?php
return array(
    'connectionString' => 'mysql:host=localhost;dbname=highered2',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'tablePrefix' => '',
    'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
    'enableParamLogging' => YII_DEBUG,
);
