<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mariela y Yorty
 * Date: 15/08/13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */

class App {
    /**
     * @param $category
     * @return TextModel By a category
     */
    static function TextModel($category){
        $cat= TextCategory::model()->findByCategory($category);
        if(count($cat)>0){
            $text = Text::model()->findByAttributes(array('text_category_id'=>$cat->id));
            if(count($text)==0){
                $text = Yii::t('Texts','Sorry, not exist texts defined for this category on the database');
            }
        }
        else{
            $text = Yii::t('Texts','Sorry, this category is not defined on the database');
        }

        return $text;
    }
    /**
     * @param $category
     * @return Text title
     */
    static function TextTitle($category){
        $cat= TextCategory::model()->findByCategory($category);
        if(count($cat)>0){
            $text = Text::model()->findByAttributes(array('text_category_id'=>$cat->id));
            if(count($text)==0){
                $text = Yii::t('Texts','Sorry, not exist texts defined for this category on the database');
            }
        }
        else{
            $text = Yii::t('Texts','Sorry, this category is not defined on the database');
        }

        return $text->title;
    }

    /**
     * @param $category
     * @return Text description
     */
    static function TextDescription($category){
        $cat= TextCategory::model()->findByCategory($category);
        if(count($cat)>0){
            $text = Text::model()->findByAttributes(array('text_category_id'=>$cat->id));
            if(count($text)==0){
                $text = Yii::t('Texts','Sorry, not exist texts defined for this category on the database');
            }
        }
        else{
            $text = Yii::t('Texts','Sorry, this category is not defined on the database');
        }

        return $text->description;
    }

    static function GetUsefulInformation($type) {
        switch ($type){
            case 'car':
                return UsefulInformation::model()->car()->findAll();
            case 'culturalIdeas':
                return UsefulInformation::model()->culturalIdeas()->findAll();
            case 'flight':
                return UsefulInformation::model()->flight()->findAll();
            case 'hotel':
                return UsefulInformation::model()->hotel()->findAll();
            case 'restaurant':
                return UsefulInformation::model()->restaurant()->findAll();
            case 'tour':
                return UsefulInformation::model()->tour()->findAll();
        }
        return UsefulInformation::model()->findAll();
    }

//    get my request class
    static function GetMyRequest() {
        $class = 'my-request';
        if((isset($_SESSION['user_hotel_request']) && count($_SESSION['user_hotel_request'])>0) ||
            (isset($_SESSION['user_cars_request'])&&count($_SESSION['user_cars_request'])>0) ||
            (isset($_SESSION['user_flight_request'])&& count($_SESSION['user_flight_request'])>0) ||
            (isset($_SESSION['user_tours_request'])&& count($_SESSION['user_tours_request'])>0 ) ||
            (isset($_SESSION['user_restaurant_request']) && count($_SESSION['user_restaurant_request'])>0) ||
            (isset($_SESSION['user_culturalsideas_request']) && count($_SESSION['user_culturalsideas_request'])>0 )){
            $class='my-request-invert';
        }
        return $class;
    }


}