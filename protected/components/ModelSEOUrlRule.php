<?php

class ModelSEOUrlRule extends CBaseUrlRule
{

    public $routeMap = array(

    );

    public $primaryKeyParam = 'id';

    /**
     * Creates a URL based on this rule.
     * @param CUrlManager $manager the manager
     * @param string $route the route
     * @param array $params list of parameters (name=>value) associated with the route
     * @param string $ampersand the token separating name-value pairs in the URL.
     * @return mixed the constructed URL. False if this rule does not apply.
     */
    public function createUrl($manager, $route, $params, $ampersand)
    {
        if ( $modelClass = array_search($route, $this->routeMap) ){
            $model = CActiveRecord::model($modelClass);
            $modelInstance = $model->findByPk($params[$this->primaryKeyParam]);

            return $modelInstance->seo->url;
        }

        return false;
    }

    /**
     * Parses a URL based on this rule.
     * @param CUrlManager $manager the URL manager
     * @param CHttpRequest $request the request object
     * @param string $pathInfo path info part of the URL (URL suffix is already removed based on {@link CUrlManager::urlSuffix})
     * @param string $rawPathInfo path info that contains the potential URL suffix
     * @return mixed the route that consists of the controller ID and action ID. False if this rule does not apply.
     */
    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $seo = SeoUrl::model()->findByAttributes(array('url'=>$pathInfo));
        if ($seo && $seo->modelClassName){
            $modelClass = CActiveRecord::model($seo->modelClassName);
            /** @var $seoBh IRSeoModelBehavior */
            $seoBh = $modelClass->asa('seo_bh');
            $instance = $modelClass->findByAttributes(array(
                $seoBh->seo_url_id_attr=>$seo->primaryKey
            ));

            if ($instance){
                $_GET['id'] = $instance->primaryKey;
                return $this->routeMap[$seo->modelClassName];
            }
        }
        return false;
    }
}
