<?php
/**
 * Class Menus
 * Esta clase se encarga de implementar un método por cada menú que tenga la administración
 */
class Menus
{

    public static function userMenu()
    {
        return array(
            array(
                'icon' => 'user',
                'label' => user()->isGuest ? '' : UserModule::user()->getFullName(),
                'url' => array('/user/profile/profile'),
                'visible' => !user()->isGuest,
            ),
            array(
                'icon' => 'signout',
                'label' => Yii::t('admin','Salir'),
                'url' => array('/user/logout'),
                'visible' => !user()->isGuest,
            ),
            array(
                'icon' => 'signin',
                'label' => Yii::t('admin','Login'),
                'url' => array('/user/login/login'),
                'visible' => user()->isGuest,
            ),
        );
    }

    public static function contentButtons()
    {
        return array(
            array(
                'url' => array('/user/admin/admin'),
                'icon' => 'group',
                'title' => Yii::t('admin','Administrar Usuarios'),
                'visible' => user()->checkAccess('User.list'),
            ),
            /*array(
                'url' => array('/irtext/default/admin'),
                'icon' => 'globe',
                'title' => Yii::t('admin','Administrar Textos'),
                'visible' => user()->checkAccess('IRSourceMessage.list'),
                /*'label' => '5',
                'labelType' => 'important',
            ),*/
            array(
                'url' => array('/irseo/default/admin'),
                'icon' => 'leaf',
                'title' => Yii::t('admin','Administrar SEO'),
                'visible' => user()->checkAccess('SeoUrl.list'),
                /*'label' => '5',
                'labelType' => 'important',*/
            ),
            array(
                'url' => array('/frontend/default/index'),
                'icon' => 'home',
                'title' => Yii::t('admin','FrontEnd'),
                'visible' => user()->checkAccess('User.list'),
                /*'label' => '5',
                'labelType' => 'important',*/
            ),
        );
    }

    public static function sideMenu()
    {

        $menu = CMap::mergeArray(
            array(
                array('label' => Yii::t('admin','Home'), 'icon' => 'home',
                    'url' => array('/site/index'),
                ),
            ),
            adminMenus()
        );


        return CMap::mergeArray($menu, array(
            array(
                'url' => '#',
                'icon' => 'briefcase',
                'label' => Yii::t('admin','Administration','Administración'),
                'visible' => UserModule::user()->id,
                'items' => array(
                    /*array(
                        'icon' => 'flag',
                        'label' => Yii::t('Administration','Organización'),
                        'url' => array('/entity/systemCompany/admin'),
                        'active' => Yii::app()->controller->module->id === 'entity' && Yii::app()->controller->id === 'systemCompany',
                    ),*/
                    array(
                        'icon' => 'flag',
                        'label' => t('General tracking code'),
                        'url' => array('/backend/generalTrackingCodes/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'generalTrackingCodes',
                    ),
                    array(
                        'icon' => 'user',
                        'label' => Yii::t('Administration','Usuarios'),
                        'url' => array('/user/admin/admin'),
                        'active' => Yii::app()->controller->module->id === 'user' && Yii::app()->controller->id === 'admin',
                    ),

                  /*  array(
                        'icon' => 'list',
                        'label' => Yii::t('Administration','Texts Category'),
                        'url' => array('/backend/textCategory/admin'),
                        'active' => Yii::app()->controller->module->id === 'backend' && Yii::app()->controller->id === 'textCategory',
                    ),
                    array(
                        'icon' => 'list',
                        'label' => Yii::t('Administration','Texts'),
                        'url' => array('/backend/text/admin'),
                        'active' => Yii::app()->controller->module->id === 'backend' && Yii::app()->controller->id === 'text',
                    ),
                    array(
                        'icon' => 'envelope',
                        'label' => Yii::t('Administration','Emails Templates'),
                        'url' => array('/backend/email/admin'),
                        'active' => Yii::app()->controller->module->id === 'backend' && Yii::app()->controller->id === 'email',
                    ),*/

                ),
            ),
            array(
                'url' => '#',
                'icon' => 'user',
                'label' => user()->isGuest ? Yii::t('admin','Usuario') : UserModule::user()->getFullName(),
                'items' => array(
                    array(
                        'icon' => 'user',
                        'label' => Yii::t('admin','Perfil'),
                        'url' => array('/user/profile/profile'),
                        'visible' => !user()->isGuest,
                    ),
                    array(
                        'icon' => 'pencil',
                        'label' => Yii::t('admin','Editar'),
                        'url' => array('/user/profile/edit'),
                        'visible' => !user()->isGuest,
                    ),
                    array(
                        'icon' => 'ok',
                        'label' => Yii::t('admin','Cambiar Contraseña'),
                        'url' => array('/user/profile/changePassword'),
                        'visible' => !user()->isGuest,
                    ),
                    array(
                        'icon' => 'signout',
                        'label' => Yii::t('admin','Salir'),
                        'url' => array('/user/logout'),
                        'visible' => !user()->isGuest,
                    ),
                    array(
                        'icon' => 'signin',
                        'label' => Yii::t('admin','Login'),
                        'url' => array('/user/login/login'),
                        'visible' => user()->isGuest,
                    ),
                    array(
                        'icon' => 'ok',
                        'label' => Yii::t('admin','Registration'),
                        'url' => array('/user/registration/registration'),
                        'visible' => user()->isGuest,
                    ),
                    array(
                        'icon' => 'ok',
                        'label' => Yii::t('admin','Lost Password?'),
                        'url' => array('/user/recovery/recovery'),
                        'visible' => user()->isGuest,
                    ),
                ),
            ),
        ));
    }
}