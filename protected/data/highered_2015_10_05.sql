-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2015 at 10:25 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `highered`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus_banner_section`
--

CREATE TABLE IF NOT EXISTS `aboutus_banner_section` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"About us","submenu":"Banner section"}}',
  `banner_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Banner Image","dimensions":"1600,484"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description","type":"@redactor"}',
  `biography_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Biography Image","dimensions":"540,410"}',
  `biography_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Biography title"}',
  `biography_subtitle` varchar(255) DEFAULT NULL COMMENT '{"label":"Biography subtitle"}',
  `biography_description` text COMMENT '{"label":"Biography","type":"@redactor"}',
  `contact_me_btn_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Contact me button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aboutus_banner_section`
--

INSERT INTO `aboutus_banner_section` (`id`, `banner_image`, `title`, `description`, `biography_image`, `biography_title`, `biography_subtitle`, `biography_description`, `contact_me_btn_text`, `created`, `updated`, `owner`) VALUES
('a425b317-1957-bf84-f5d7-6508245aa5a0', '', 'About us', '<p>\r\n            Higher Education Consultants of America, LLC is an education planning consultancy agency dedicated to\r\n            college admissions consulting, graduate and professional school admissions consultancy, international\r\n            college student applications.\r\n        </p><p>\r\n            Academic, social, financial, occupational, and other dimensions dominate the questions and concerns\r\n            families and students have with the college search process. HECA, LLC understands the many questions\r\n            you have about college. We provide our clients and scholars with important information regarding colleges\r\n            and universities, both their stated and unstated admissions requirements, campus culture, and  student\r\n            interests, etc.\r\n        </p><p>\r\n            Trying to find a best fit does not necessarily mean the most expensive or highest ranking institution.\r\n            For some people, finding the best fit may mean focusing on college majors and available financial aid\r\n            provided and/or private scholarships. After all, what good is it to be admitted to an Ivy League institution,\r\n            if the degree you want to earn is not offered? Not much if you are interested in a specific career track.\r\n        </p><p>\r\n            At Higher Education Consultants of America, we pride ourselves in ensuring that you understand the fit\r\n            of the institution is the most important part of the decision to accept admissions to an institution.\r\n            We put everything into perspective and collectively weigh your values and interests in the decision-making\r\n            process.\r\n        </p>', 'Conact photo', 'Pedro (Pete) Villarreal III, Ph.D.', 'CEO and Chief Education Consultant', '<p>\r\n                    Pete has served in various roles at a variety of institutions including public and private,\r\n                    religious and non-denomination, regional and major land grant universities, state-owned and\r\n                    state-related, nationally ranked and unranked institutions such as George Washington University,\r\n                    University of Florida, University of Miami, Pennsylvania State University, Baylor University, and\r\n                    the University of Texas-Pan American.\r\n                </p><p>\r\n                    His experiences as a professor teaching future administrators of colleges and universities gives\r\n                    him the insight and practical knowledge of the ins and outs on how higher education institutions\r\n                    organize themselves. This information can be quite useful to students and parents in the educational\r\n                    planning and consulting process.\r\n                </p><p>\r\n                    Pete has served in various roles at a variety of institutions including public and private,\r\n                    religious and non-denomination, regional and major land grant universities, state-owned and\r\n                    state-related, nationally ranked and unranked institutions such as George Washington University,\r\n                    University of Florida, University of Miami, Pennsylvania State University, Baylor University, and\r\n                    the University of Texas-Pan American.\r\n                </p>', 'Contact me', '2015-10-01 19:59:11', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `aboutus_page_section2`
--

CREATE TABLE IF NOT EXISTS `aboutus_page_section2` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"About us","submenu":"Section 2"}}',
  `left_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Left image","dimensions":"800,960"}',
  `mission_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Mission Title"}',
  `mission_text` text COMMENT '{"label":"Mission text"}',
  `services_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Services Title"}',
  `services_text` text COMMENT '{"label":"Services text"}',
  `committed_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Committed Title"}',
  `committed_text` text COMMENT '{"label":"Committed text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aboutus_page_section2`
--

INSERT INTO `aboutus_page_section2` (`id`, `left_image`, `mission_title`, `mission_text`, `services_title`, `services_text`, `committed_title`, `committed_text`, `created`, `updated`, `owner`) VALUES
('70618502-70b5-24c4-552d-fe417ca60b33', 'About image section 2', 'Mission', 'We serve students and their families through the college search process including college matching, college admissions and financial aid information dissemination, career exploration and major selection, and academic preparation and curricular advice. ', 'Services', 'We provide individual and group educational services to prospective college students. Specific services include assistance with the college search process, college admissions information, college matching; financial aid information; college applications; resume editing, and college essay/personal statements editing. We also provide our clients with access to college and university resources and guides. ', 'Committed to Equity', 'We commit to serve a very limited number of academically talented students who come from extreme poverty on a pro-bono basis. ', '2015-10-01 20:03:16', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `aboutus_page_section3`
--

CREATE TABLE IF NOT EXISTS `aboutus_page_section3` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"About us","submenu":"Section 3"}}',
  `right_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Right image","dimensions":"799,960"}',
  `vision_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Vision Title"}',
  `vision_text` text COMMENT '{"label":"Vision text"}',
  `clientele_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Clientele Title"}',
  `Clientele_text` text COMMENT '{"label":"Services text"}',
  `committed_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Committed Title"}',
  `committed_text` text COMMENT '{"label":"Committed text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aboutus_page_section3`
--

INSERT INTO `aboutus_page_section3` (`id`, `right_image`, `vision_title`, `vision_text`, `clientele_title`, `Clientele_text`, `committed_title`, `committed_text`, `created`, `updated`, `owner`) VALUES
('f8ebd85c-8d70-67a4-0116-9d12128366e2', 'About image section3', 'Vision', 'We will be a leader in the area of independent educational consulting dedicated to prospective college students and their families. ', 'Clientele', ' We serve three types of clients: 1) traditional students seeking assistance with undergraduate admissions; 2) students seeking assistance with graduate and professional school admissions; and 3) international students (largely from Latin America) seeking assistance with the international student admissions process at American higher education institutions. ', NULL, NULL, '2015-10-01 20:09:01', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `id` char(128) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `value` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE IF NOT EXISTS `company_info` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Company info"}}',
  `main_logo` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Main logo","dimensions":"260,70"}',
  `secondary_logo` varchar(255) NOT NULL COMMENT '{"type":"recipeImg2","label":"Secondary logo","dimensions":"219,59"}',
  `name` varchar(255) NOT NULL COMMENT '{"label":"name"}',
  `email` varchar(255) NOT NULL COMMENT '{"label":"email","type":"@email"}',
  `phone1` varchar(255) NOT NULL COMMENT '{"label":"phone1","type":"@phone"}',
  `phone2` varchar(255) NOT NULL COMMENT '{"label":"phone2","type":"@phone"}',
  `address` varchar(255) DEFAULT NULL COMMENT '{"label":"Address line1"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `main_logo`, `secondary_logo`, `name`, `email`, `phone1`, `phone2`, `address`, `created`, `updated`, `owner`) VALUES
('719ce4b2-c9f3-e2e4-e510-9be459cecdbf', 'Main site logo', 'Secundary site logo', 'Higher Education Consultants of America, LLC', 'contact@highered.us', '+1 305-888-9009 ', '', 'Miami, FL 33133/Coral Gables, FL 33145 ', '2015-10-01 19:38:29', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_banner_section`
--

CREATE TABLE IF NOT EXISTS `contactus_banner_section` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Contact us","submenu":"Banner section"}}',
  `banner_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Banner Image","dimensions":"1600,484"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description","type":"@redactor"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactus_banner_section`
--

INSERT INTO `contactus_banner_section` (`id`, `banner_image`, `title`, `description`, `created`, `updated`, `owner`) VALUES
('308faa66-abf6-5324-f59d-b9f9c3984554', '', 'Contact us', '<p>Higher Education Consultants of America, LLC is an education\r\n planning consultancy agency dedicated to college admissions consulting,\r\n graduate and professional school admissions consultancy, international \r\ncollege student applications. </p><p> Academic, social, financial, occupational, and other dimensions \r\ndominate the questions and concerns families and students have with the \r\ncollege search process. HECA, LLC understands the many questions you \r\nhave about college. We provide our clients and scholars with important \r\ninformation regarding colleges and universities, both their stated and \r\nunstated admissions requirements, campus culture, and student interests,\r\n etc. </p>', '2015-10-02 21:59:40', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_page_section2`
--

CREATE TABLE IF NOT EXISTS `contactus_page_section2` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Contact us","submenu":"Section 2"}}',
  `left_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Left image","dimensions":"800,900"}',
  `form_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Form Title"}',
  `from_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"From button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactus_page_section2`
--

INSERT INTO `contactus_page_section2` (`id`, `left_image`, `form_title`, `from_button_text`, `created`, `updated`, `owner`) VALUES
('a04ff50d-c44c-9244-6916-25be7698937b', '', 'Send us and email', 'Send it', '2015-10-02 22:01:15', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_page_section3`
--

CREATE TABLE IF NOT EXISTS `contactus_page_section3` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Contact us","submenu":"Section 3"}}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `use_map` tinyint(1) DEFAULT NULL COMMENT '{"label":"Use map view"}',
  `longitude` varchar(255) DEFAULT NULL COMMENT '{"label":"Longitude"}',
  `latitude` varchar(255) DEFAULT NULL COMMENT '{"label":"Latitude"}',
  `map_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg3","label":"Map image","dimensions":"1600,440"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactus_page_section3`
--

INSERT INTO `contactus_page_section3` (`id`, `title`, `use_map`, `longitude`, `latitude`, `map_image`, `created`, `updated`, `owner`) VALUES
('ed44caeb-3f06-2f64-8d70-42c3cce5aa6c', 'Check our services', 0, NULL, NULL, '', '2015-10-02 22:02:32', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purpose` varchar(255) NOT NULL,
  `from` varchar(255) NOT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `internal` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purpose` (`purpose`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `error_page`
--

CREATE TABLE IF NOT EXISTS `error_page` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Error Page"}}',
  `background_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Background image","dimensions":"1600,810"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `message` varchar(255) DEFAULT NULL COMMENT '{"label":"Message text"}',
  `button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `error_page`
--

INSERT INTO `error_page` (`id`, `background_image`, `title`, `message`, `button_text`, `created`, `updated`, `owner`) VALUES
('8edee2a6-80db-35e4-c96a-d12e366ecc4c', '', 'Error page', 'We can’t seem to find the page you are looking for', 'GO HOMEPAGE', '2015-10-01 19:43:07', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE IF NOT EXISTS `footer` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Footer"}}',
  `copy_right` varchar(255) DEFAULT NULL COMMENT '{"label":"Copy right information"}',
  `developer_by_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Developer by text"}',
  `developer_by_link_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Developer by link text"}',
  `developer_site_url` varchar(255) DEFAULT NULL COMMENT '{"type" : "@url", "label" : "Developer site url"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `copy_right`, `developer_by_text`, `developer_by_link_text`, `developer_site_url`, `created`, `updated`, `owner`) VALUES
('ad0a2c93-9c60-9074-bdae-940e8f315069', 'Pedro Villarreal III, Ph.D. All Rights Reserved.', 'WEBSITE DESIGN & DEVELOPMENT BY', 'THRUADS.COM CORP', 'http://thruads.com', '2015-10-01 19:34:53', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `general_tracking_codes`
--

CREATE TABLE IF NOT EXISTS `general_tracking_codes` (
  `id` varchar(50) NOT NULL COMMENT '{"type": "@uid", "rows":1, "breadcrumbs": {"tablename": "General tracking codes"}}',
  `source_code` text NOT NULL COMMENT '{"type": "@textarea", "label": "Source code"}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `home_banner_section`
--

CREATE TABLE IF NOT EXISTS `home_banner_section` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","breadcrumbs":{"tablename":"Home Page","submenu":"Banner section"}}',
  `add_other_video_format` tinyint(1) DEFAULT '0' COMMENT '{"label":"I want add more video formats"}',
  `video_format` varchar(255) DEFAULT NULL COMMENT '{"label":"Video Format"}',
  `banner_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Image","dimensions":"1600,810"}',
  `video` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Video","dimensions":"1600,810"}',
  `video_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg2","label":"Alternative video image","dimensions":"1920,1080"}',
  `banner_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Banner title"}',
  `banner_subtitle` varchar(255) DEFAULT NULL COMMENT '{"label":"Banner subtitle"}',
  `banner_started_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Started button text"}',
  `banner_type` varchar(255) NOT NULL COMMENT '{"type":"dropdown","label":"Banner type","data":{"1":"Image","2":"Video"}}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_banner_section`
--

INSERT INTO `home_banner_section` (`id`, `add_other_video_format`, `video_format`, `banner_image`, `video`, `video_image`, `banner_title`, `banner_subtitle`, `banner_started_text`, `banner_type`, `created`, `updated`, `owner`) VALUES
('0bd85fe0-c8dd-3e14-c5e2-9ebd0888f351', 0, NULL, '', '', '', 'image2', 'test image2', 'GET STARTED', '1', '2015-10-03 10:39:21', NULL, 'admin'),
('ae09997b-093d-e634-d981-63cab6c69d66', 1, '2', '', '', '', 'video', 'video prueba', 'GET STARTED', '2', '2015-10-04 19:51:28', NULL, 'admin'),
('e3f885fd-aec8-e804-99ba-f3021848cefe', 0, NULL, '', '', '', 'image1', 'Test subtitle', 'GET STARTED', '1', '2015-10-03 10:38:41', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_banner_video`
--

CREATE TABLE IF NOT EXISTS `home_banner_video` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","breadcrumbs":{"tablename":"Services","submenu":"Topic"}}',
  `video` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Video","dimensions":"1600,810"}',
  `video_format` varchar(255) NOT NULL COMMENT '{"type":"dropdown","label":"video format","data":{"1":"video/webm","2":"video/mp4","3":"video/ogg"}}',
  `home_banner` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `video_home_banner_id_fk1` (`home_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_banner_video`
--

INSERT INTO `home_banner_video` (`id`, `video`, `video_format`, `home_banner`, `created`, `updated`, `owner`) VALUES
('5791ffbf-6329-ad74-b53c-2f2039ee6a14', '', '3', 'ae09997b-093d-e634-d981-63cab6c69d66', '2015-10-04 22:45:36', NULL, 'admin'),
('962d7705-1960-29c4-4d57-9b5d8783fb63', '', '1', 'ae09997b-093d-e634-d981-63cab6c69d66', '2015-10-04 22:45:36', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section2`
--

CREATE TABLE IF NOT EXISTS `home_page_section2` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Home Page","submenu":"Section2"}}',
  `image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"College image","dimensions":"800,810"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `subtitle_p1` varchar(255) DEFAULT NULL COMMENT '{"label":"Subtitle paragraph1"}',
  `subtitle_p2` varchar(255) DEFAULT NULL COMMENT '{"label":"Subtitle paragraph2"}',
  `search_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"search button text"}',
  `description` text COMMENT '{"label":"Description","type":"@redactor"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_section2`
--

INSERT INTO `home_page_section2` (`id`, `image`, `title`, `subtitle_p1`, `subtitle_p2`, `search_button_text`, `description`, `created`, `updated`, `owner`) VALUES
('bd0c3b63-9eb1-3d84-e5d6-c45e32474ab8', '', 'College Search', 'Harvard receives over 37,000 applications. It admits approximately 2,100', 'Penn State receives over 66,500 applications. It admits over 8,100 to its main campus', 'Search with us', '<p>Trying to find a best fit does not necessarily mean the most expensive \r\nor highest ranking institution. For  some people, finding the best fit \r\nmay mean focusing on college majors and available financial aid provided\r\n and/or private scholarships. After all, what good is                   \r\n      it to be admitted to an Ivy League institution, if the degree you \r\nwant to earn is not offered? Not much if you are interested in a \r\nspecific career track.</p>', '2015-10-04 20:19:17', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section3`
--

CREATE TABLE IF NOT EXISTS `home_page_section3` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Home Page","submenu":"Section3"}}',
  `image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Image","dimensions":"1600,880"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_section3`
--

INSERT INTO `home_page_section3` (`id`, `image`, `title`, `description`, `created`, `updated`, `owner`) VALUES
('921b418f-d6a8-7f84-393c-7a5a969aa9e5', 'Promo Image', 'Financial Aid and Scholarship', 'One of the most important and often determining aspects of the college search and match process is related to the financing of higher education. The affordability question is an important part of the process and requires your', '2015-10-04 20:22:17', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section4`
--

CREATE TABLE IF NOT EXISTS `home_page_section4` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Home Page","submenu":"Section4"}}',
  `image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Image","dimensions":"1600,810"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_section4`
--

INSERT INTO `home_page_section4` (`id`, `image`, `title`, `description`, `created`, `updated`, `owner`) VALUES
('4153669e-ea1a-f804-95ae-99a0cd9db4ba', '', 'International Students', 'If you or your family is located out of the country, we provide the services on a limited-term basis through the use of Skype, ooVoo, Hangouts, or other video over internet service providers. Please contact us to inquire about specialized pricing for you and your situation. ', '2015-10-04 20:25:07', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section5`
--

CREATE TABLE IF NOT EXISTS `home_page_section5` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Home Page","submenu":"Section5"}}',
  `left_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Left Image","dimensions":"857,900"}',
  `right_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg2","label":"Right Image","dimensions":"879,810"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `subtitle_p1` varchar(255) DEFAULT NULL COMMENT '{"label":"Subtitle paragraph1"}',
  `subtitle_p2` varchar(255) DEFAULT NULL COMMENT '{"label":"Subtitle paragraph2"}',
  `get_started_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Get started button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_section5`
--

INSERT INTO `home_page_section5` (`id`, `left_image`, `right_image`, `title`, `subtitle_p1`, `subtitle_p2`, `get_started_button_text`, `created`, `updated`, `owner`) VALUES
('81932d12-7a1d-7f04-210e-6796f81a5ed7', 'Promo Image', 'Promo Image', 'Graduate and Professional Search', 'We all know that graduate and professional education can be a difficult and enduring task. ', 'I am available to provide individual based sessions on a limited-term basis to assist with the graduate school and professional school admissions process. ', 'GET STARTED', '2015-10-04 20:28:09', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section6`
--

CREATE TABLE IF NOT EXISTS `home_page_section6` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Home Page","submenu":"Section6"}}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description"}',
  `left_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Left Image","dimensions":"500,347"}',
  `left_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Left image title"}',
  `left_btn_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Left image button text"}',
  `middle_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg2","label":"Middle Image","dimensions":"500,347"}',
  `middle_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Left image title"}',
  `middle_btn_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Middle image button text"}',
  `right_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg3","label":"Right Image","dimensions":"500,347"}',
  `right_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Left image title"}',
  `right_btn_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Left image button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_section6`
--

INSERT INTO `home_page_section6` (`id`, `title`, `description`, `left_image`, `left_title`, `left_btn_text`, `middle_image`, `middle_title`, `middle_btn_text`, `right_image`, `right_title`, `right_btn_text`, `created`, `updated`, `owner`) VALUES
('1779185c-1a87-2a34-f9ab-31944a98f0ed', ' Success Stories', ' I have a limited clientele, and every student I work with receives the individual attention that makes her or him highly prepared for college search and application process. I provide… ', 'Promo Image', 'In ferox quadrata', 'GET STARTED', 'Promo Image', 'Nunquam visum idoleum', 'GET STARTED', 'Promo Image', 'Demittos fermium est', 'GET STARTED', '2015-10-04 20:32:49', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `i18n_category`
--

CREATE TABLE IF NOT EXISTS `i18n_category` (
  `category` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `i18n_category`
--

INSERT INTO `i18n_category` (`category`, `description`) VALUES
('AboutusBannerSection', NULL),
('AboutusPageSection2', NULL),
('AboutusPageSection3', NULL),
('admin', NULL),
('Administration', NULL),
('app', NULL),
('AuthModule.main', NULL),
('backend', NULL),
('CompanyInfo', NULL),
('ContactusBannerSection', NULL),
('ContactusPageSection2', NULL),
('ContactusPageSection3', NULL),
('ErrorPage', NULL),
('Footer', NULL),
('HomeBannerSection', NULL),
('HomeBannerVideo', NULL),
('HomePageSection2', NULL),
('HomePageSection3', NULL),
('HomePageSection4', NULL),
('HomePageSection5', NULL),
('HomePageSection6', NULL),
('Label', NULL),
('labels', NULL),
('MainMenu', NULL),
('Plan', NULL),
('SeoModel', NULL),
('SeoModelMeta', NULL),
('SeoModelTrackingCode', NULL),
('ServiceBannerSection', NULL),
('ServicePageSection2', NULL),
('sideMenu', NULL),
('SocialLinks', NULL),
('TbEditableField.editable', NULL),
('ThankPage', NULL),
('Topic', NULL),
('TrackingCode', NULL),
('transfer', NULL),
('UserModule.user', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `i18n_language`
--

CREATE TABLE IF NOT EXISTS `i18n_language` (
  `id` varchar(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `enable` int(11) DEFAULT '1',
  `isDefault` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `i18n_language`
--

INSERT INTO `i18n_language` (`id`, `name`, `enable`, `isDefault`) VALUES
('en', 'English', 1, 1),
('es', 'Español', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `i18n_message`
--

CREATE TABLE IF NOT EXISTS `i18n_message` (
  `id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `translation` text,
  PRIMARY KEY (`id`,`language`),
  KEY `FKi18n_messa482734` (`language`),
  KEY `FKi18n_messa868058` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `i18n_message`
--

INSERT INTO `i18n_message` (`id`, `language`, `translation`) VALUES
(1, 'en', 'Choose {name}'),
(2, 'en', 'password'),
(3, 'en', 'Enter username and password to continue.'),
(4, 'en', 'Remember me next time'),
(5, 'en', 'E-mail'),
(6, 'en', 'password'),
(7, 'en', 'Lost Password?'),
(8, 'en', 'Login'),
(9, 'en', 'Enter your e-mail address below and we will send you instructions how to recover a password.'),
(10, 'en', 'username or email'),
(11, 'en', 'Incorrect symbols (A-z0-9).'),
(12, 'en', 'Back to login'),
(13, 'en', 'Restore'),
(14, 'en', 'Home'),
(15, 'en', 'Welcome to '),
(16, 'en', 'Atras'),
(17, 'en', 'Perfil'),
(18, 'en', 'Profile'),
(19, 'en', 'Edit'),
(20, 'en', 'Change password'),
(21, 'en', 'Administration'),
(22, 'en', 'Enterprise'),
(23, 'en', 'Logout'),
(24, 'en', 'General'),
(25, 'en', 'Menu'),
(26, 'en', 'Footer'),
(27, 'en', 'Social media'),
(28, 'en', 'Company Info'),
(29, 'en', 'Home Page'),
(30, 'en', 'Banner Section'),
(31, 'en', 'Section2'),
(32, 'en', 'Section3'),
(33, 'en', 'Section4'),
(34, 'en', 'Section5'),
(35, 'en', 'Section6'),
(36, 'en', 'About Us'),
(37, 'en', 'Contact Us'),
(38, 'en', 'Services'),
(39, 'en', 'Plans'),
(40, 'en', 'Languages'),
(41, 'en', 'Translations'),
(42, 'en', 'Categorías'),
(43, 'en', 'Seo'),
(44, 'en', 'Seo Model'),
(45, 'en', 'Url Rules'),
(46, 'en', 'Permisos'),
(47, 'en', 'Assignments'),
(48, 'en', 'role|roles'),
(49, 'en', 'task|tasks'),
(50, 'en', 'operation|operations'),
(51, 'en', 'Administration'),
(52, 'en', 'General tracking code'),
(53, 'en', 'Usuarios'),
(54, 'en', 'Perfil'),
(55, 'en', 'Editar'),
(56, 'en', 'Cambiar Contraseña'),
(57, 'en', 'Salir'),
(58, 'en', 'Registration'),
(59, 'en', 'Go to Home'),
(60, 'en', 'Plan'),
(61, 'en', 'Are you sure want to delete this item'),
(62, 'en', 'Actions'),
(63, 'en', 'ID'),
(64, 'en', 'Title'),
(65, 'en', 'Created'),
(66, 'en', 'Updated'),
(67, 'en', 'Owner'),
(68, 'en', 'Add item'),
(69, 'en', 'The data was save with success'),
(70, 'en', 'Section 2'),
(71, 'en', 'ID'),
(72, 'en', 'Background image Alt'),
(73, 'en', 'Title'),
(74, 'en', 'Description'),
(75, 'en', 'Weeknights title'),
(76, 'en', 'Weeknights time text'),
(77, 'en', 'Weekends title'),
(78, 'en', 'Weekends time text'),
(79, 'en', 'Contact button text'),
(80, 'en', 'Created'),
(81, 'en', 'Updated'),
(82, 'en', 'Owner'),
(83, 'en', 'Background image'),
(84, 'en', 'ID'),
(85, 'en', 'Banner Image Alt'),
(86, 'en', 'Title'),
(87, 'en', 'Description'),
(88, 'en', 'Created'),
(89, 'en', 'Updated'),
(90, 'en', 'Owner'),
(91, 'en', 'Banner Image'),
(92, 'en', 'Add'),
(93, 'en', 'Create {name}'),
(94, 'en', 'Text'),
(95, 'en', 'ID'),
(96, 'en', 'Title'),
(97, 'en', 'Time planned'),
(98, 'en', 'Description'),
(99, 'en', 'To order'),
(100, 'en', 'Plan'),
(101, 'en', 'Created'),
(102, 'en', 'Updated'),
(103, 'en', 'Owner'),
(104, 'en', 'Add new topic'),
(105, 'en', 'Seo Options'),
(106, 'en', 'id'),
(107, 'en', 'url'),
(108, 'en', 'title'),
(109, 'en', 'description'),
(110, 'en', 'keywords'),
(111, 'en', 'modelClassName'),
(112, 'en', 'Enabled facebook integration'),
(113, 'en', 'Enabled twitter integration'),
(114, 'en', 'Article Publisher'),
(115, 'en', 'Article Section'),
(116, 'en', 'Twitter card'),
(117, 'en', 'Facebook image'),
(118, 'en', 'Twitter image'),
(119, 'en', 'id'),
(120, 'en', 'name'),
(121, 'en', 'content'),
(122, 'en', 'seo_model_id'),
(123, 'en', 'Add meta line'),
(124, 'en', 'id'),
(125, 'en', 'name'),
(126, 'en', 'code'),
(127, 'en', 'apply over body section'),
(128, 'en', 'seo_model_id'),
(129, 'en', 'Add tracking code'),
(130, 'en', 'Back'),
(131, 'en', 'Save item'),
(132, 'en', 'Reset form'),
(133, 'en', 'Remove'),
(134, 'en', 'Correct Seo Errors'),
(135, 'en', 'Error, had been an error saving item.'),
(136, 'en', 'Success, item was saved.'),
(137, 'en', 'Enter'),
(138, 'en', 'Edit '),
(139, 'en', 'Edit {name}'),
(140, 'en', 'Success, the changes were saved.'),
(141, 'en', 'Up'),
(142, 'en', 'Down'),
(143, 'en', 'View'),
(144, 'en', 'View {name}'),
(145, 'en', 'Update item'),
(146, 'en', 'Remove item'),
(147, 'en', 'Data was saved with success'),
(148, 'en', 'Are you sure you want to delete this item?'),
(149, 'en', 'ID'),
(150, 'en', 'Image Alt'),
(151, 'en', 'Video Alt'),
(152, 'en', 'Alternative video image Alt'),
(153, 'en', 'Banner title'),
(154, 'en', 'Banner subtitle'),
(155, 'en', 'Started button text'),
(156, 'en', 'Banner type'),
(157, 'en', 'Created'),
(158, 'en', 'Updated'),
(159, 'en', 'Owner'),
(160, 'en', 'Image'),
(161, 'en', 'Video'),
(162, 'en', 'Alternative video image'),
(163, 'en', 'The image dimensions are 1600x810px'),
(164, 'en', 'The image dimensions are 1920x1080px'),
(165, 'en', 'Image'),
(166, 'en', 'Video'),
(167, 'en', 'Select'),
(168, 'en', 'Select de video'),
(169, 'en', 'Section 3'),
(170, 'en', 'No'),
(171, 'en', 'Yes'),
(172, 'en', 'ID'),
(173, 'en', 'Title'),
(174, 'en', 'Use map view'),
(175, 'en', 'Longitude'),
(176, 'en', 'Latitude'),
(177, 'en', 'Map image Alt'),
(178, 'en', 'Created'),
(179, 'en', 'Updated'),
(180, 'en', 'Owner'),
(181, 'en', 'Map image'),
(182, 'en', 'The image dimensions are 1600x440px'),
(183, 'en', 'General tracking codes'),
(184, 'en', 'ID'),
(185, 'en', 'Source code'),
(186, 'en', 'Up to'),
(187, 'en', 'row(s)'),
(188, 'en', 'Add item'),
(189, 'en', 'The data was save with success'),
(190, 'en', 'Error Page'),
(191, 'en', 'Thank you'),
(192, 'en', 'ID'),
(193, 'en', 'Background image Alt'),
(194, 'en', 'Title'),
(195, 'en', 'Message text'),
(196, 'en', 'Button text'),
(197, 'en', 'Created'),
(198, 'en', 'Updated'),
(199, 'en', 'Owner'),
(200, 'en', 'Background image'),
(201, 'en', 'Thanks page'),
(202, 'en', 'ID'),
(203, 'en', 'Background image Alt'),
(204, 'en', 'Title'),
(205, 'en', 'Subtitle'),
(206, 'en', 'Description'),
(207, 'en', 'Home button text'),
(208, 'en', 'Services button text'),
(209, 'en', 'Created'),
(210, 'en', 'Updated'),
(211, 'en', 'Owner'),
(212, 'en', 'Background image'),
(213, 'en', 'Main menu'),
(214, 'en', 'ID'),
(215, 'en', 'Portrait image Alt'),
(216, 'en', 'Home option text'),
(217, 'en', 'About option text'),
(218, 'en', 'Service option text'),
(219, 'en', 'Contact option text'),
(220, 'en', 'Created'),
(221, 'en', 'Updated'),
(222, 'en', 'Owner'),
(223, 'en', 'Portrait image'),
(224, 'en', 'The image dimensions are 133x134px'),
(225, 'en', 'ID'),
(226, 'en', 'Copy right information'),
(227, 'en', 'Developer by text'),
(228, 'en', 'Developer by link text'),
(229, 'en', 'Developer site url'),
(230, 'en', 'Created'),
(231, 'en', 'Updated'),
(232, 'en', 'Owner'),
(233, 'en', 'ID'),
(234, 'en', 'Facebook url'),
(235, 'en', 'Youtube url'),
(236, 'en', 'Linkedin url'),
(237, 'en', 'Created'),
(238, 'en', 'Updated'),
(239, 'en', 'Owner'),
(240, 'en', 'The email isn''t correct'),
(241, 'en', 'Email already exists!'),
(242, 'en', 'ID'),
(243, 'en', 'Main logo Alt'),
(244, 'en', 'Secondary logo Alt'),
(245, 'en', 'name'),
(246, 'en', 'email'),
(247, 'en', 'phone1'),
(248, 'en', 'phone2'),
(249, 'en', 'Address line1'),
(250, 'en', 'Created'),
(251, 'en', 'Updated'),
(252, 'en', 'Owner'),
(253, 'en', 'Main logo'),
(254, 'en', 'Secondary logo'),
(255, 'en', 'The image dimensions are 260x70px'),
(256, 'en', 'The image dimensions are 219x59px'),
(257, 'en', 'Company Address'),
(258, 'en', 'ID'),
(259, 'en', 'Banner Image Alt'),
(260, 'en', 'Title'),
(261, 'en', 'Description'),
(262, 'en', 'Biography Image Alt'),
(263, 'en', 'Biography title'),
(264, 'en', 'Biography subtitle'),
(265, 'en', 'Biography'),
(266, 'en', 'Contact me button text'),
(267, 'en', 'Created'),
(268, 'en', 'Updated'),
(269, 'en', 'Owner'),
(270, 'en', 'Banner Image'),
(271, 'en', 'Biography Image'),
(272, 'en', 'The image dimensions are 1600x484px'),
(273, 'en', 'The image dimensions are 540x410px'),
(274, 'en', 'ID'),
(275, 'en', 'Left image Alt'),
(276, 'en', 'Mission Title'),
(277, 'en', 'Mission text'),
(278, 'en', 'Services Title'),
(279, 'en', 'Services text'),
(280, 'en', 'Committed Title'),
(281, 'en', 'Committed text'),
(282, 'en', 'Created'),
(283, 'en', 'Updated'),
(284, 'en', 'Owner'),
(285, 'en', 'Left image'),
(286, 'en', 'The image dimensions are 800x960px'),
(287, 'en', 'ID'),
(288, 'en', 'Right image Alt'),
(289, 'en', 'Vision Title'),
(290, 'en', 'Vision text'),
(291, 'en', 'Clientele Title'),
(292, 'en', 'Services text'),
(293, 'en', 'Committed Title'),
(294, 'en', 'Committed text'),
(295, 'en', 'Created'),
(296, 'en', 'Updated'),
(297, 'en', 'Owner'),
(298, 'en', 'Right image'),
(299, 'en', 'The image dimensions are 799x960px'),
(300, 'en', 'The image dimensions are 1600x680px'),
(301, 'en', 'ID'),
(302, 'en', 'Banner Image Alt'),
(303, 'en', 'Title'),
(304, 'en', 'Description'),
(305, 'en', 'Created'),
(306, 'en', 'Updated'),
(307, 'en', 'Owner'),
(308, 'en', 'Banner Image'),
(309, 'en', 'ID'),
(310, 'en', 'Left image Alt'),
(311, 'en', 'Form Title'),
(312, 'en', 'From button text'),
(313, 'en', 'Created'),
(314, 'en', 'Updated'),
(315, 'en', 'Owner'),
(316, 'en', 'Left image'),
(317, 'en', 'The image dimensions are 800x900px'),
(318, 'en', 'Form button text'),
(319, 'en', 'Select'),
(320, 'en', 'ID'),
(321, 'en', 'College image Alt'),
(322, 'en', 'Title'),
(323, 'en', 'Subtitle paragraph1'),
(324, 'en', 'Subtitle paragraph2'),
(325, 'en', 'search button text'),
(326, 'en', 'Description'),
(327, 'en', 'Created'),
(328, 'en', 'Updated'),
(329, 'en', 'Owner'),
(330, 'en', 'College image'),
(331, 'en', 'The image dimensions are 800x810px'),
(332, 'en', 'ID'),
(333, 'en', 'Image Alt'),
(334, 'en', 'Title'),
(335, 'en', 'Description'),
(336, 'en', 'Created'),
(337, 'en', 'Updated'),
(338, 'en', 'Owner'),
(339, 'en', 'Image'),
(340, 'en', 'The image dimensions are 1600x880px'),
(341, 'en', 'ID'),
(342, 'en', 'Image Alt'),
(343, 'en', 'Title'),
(344, 'en', 'Description'),
(345, 'en', 'Created'),
(346, 'en', 'Updated'),
(347, 'en', 'Owner'),
(348, 'en', 'Image'),
(349, 'en', 'ID'),
(350, 'en', 'Left Image Alt'),
(351, 'en', 'Right Image Alt'),
(352, 'en', 'Title'),
(353, 'en', 'Subtitle paragraph1'),
(354, 'en', 'Subtitle paragraph2'),
(355, 'en', 'Get started button text'),
(356, 'en', 'Created'),
(357, 'en', 'Updated'),
(358, 'en', 'Owner'),
(359, 'en', 'Left Image'),
(360, 'en', 'Right Image'),
(361, 'en', 'The image dimensions are 857x900px'),
(362, 'en', 'The image dimensions are 879x810px'),
(363, 'en', 'ID'),
(364, 'en', 'Title'),
(365, 'en', 'Description'),
(366, 'en', 'Left Image Alt'),
(367, 'en', 'Left image title'),
(368, 'en', 'Left image button text'),
(369, 'en', 'Middle Image Alt'),
(370, 'en', 'Middle image button text'),
(371, 'en', 'Right Image Alt'),
(372, 'en', 'Created'),
(373, 'en', 'Updated'),
(374, 'en', 'Owner'),
(375, 'en', 'Left Image'),
(376, 'en', 'Middle Image'),
(377, 'en', 'Right Image'),
(378, 'en', 'The image dimensions are 500x347px'),
(379, 'en', 'Video format'),
(380, 'en', 'I want add more video formats'),
(381, 'en', 'video/webm'),
(382, 'en', 'video/mp4'),
(383, 'en', 'video/ogg'),
(384, 'en', 'ID'),
(385, 'en', 'Video Alt'),
(386, 'en', 'video format'),
(387, 'en', 'Home Banner'),
(388, 'en', 'Created'),
(389, 'en', 'Updated'),
(390, 'en', 'Owner'),
(391, 'en', 'Video'),
(392, 'en', 'Add new video format'),
(393, 'en', 'Add the video'),
(394, 'en', 'Select the video'),
(395, 'en', 'Alternative video image');

-- --------------------------------------------------------

--
-- Table structure for table `i18n_source`
--

CREATE TABLE IF NOT EXISTS `i18n_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `FKi18n_sourc696102` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=396 ;

--
-- Dumping data for table `i18n_source`
--

INSERT INTO `i18n_source` (`id`, `category`, `message`) VALUES
(1, 'admin', 'Choose {name}'),
(2, 'admin', 'password'),
(3, 'admin', 'Enter username and password to continue.'),
(4, 'UserModule.user', 'Remember me next time'),
(5, 'UserModule.user', 'E-mail'),
(6, 'UserModule.user', 'password'),
(7, 'admin', 'Lost Password?'),
(8, 'admin', 'Login'),
(9, 'admin', 'Enter your e-mail address below and we will send you instructions how to recover a password.'),
(10, 'UserModule.user', 'username or email'),
(11, 'UserModule.user', 'Incorrect symbols (A-z0-9).'),
(12, 'admin', 'Back to login'),
(13, 'admin', 'Restore'),
(14, 'admin', 'Home'),
(15, 'admin', 'Welcome to '),
(16, 'admin', 'Atras'),
(17, 'backend', 'Perfil'),
(18, 'backend', 'Profile'),
(19, 'backend', 'Edit'),
(20, 'backend', 'Change password'),
(21, 'backend', 'Administration'),
(22, 'Administration', 'Enterprise'),
(23, 'backend', 'Logout'),
(24, 'sideMenu', 'General'),
(25, 'sideMenu', 'Menu'),
(26, 'sideMenu', 'Footer'),
(27, 'sideMenu', 'Social media'),
(28, 'sideMenu', 'Company Info'),
(29, 'sideMenu', 'Home Page'),
(30, 'sideMenu', 'Banner Section'),
(31, 'sideMenu', 'Section2'),
(32, 'sideMenu', 'Section3'),
(33, 'sideMenu', 'Section4'),
(34, 'sideMenu', 'Section5'),
(35, 'sideMenu', 'Section6'),
(36, 'sideMenu', 'About Us'),
(37, 'sideMenu', 'Contact Us'),
(38, 'sideMenu', 'Services'),
(39, 'sideMenu', 'Plans'),
(40, 'sideMenu', 'Languages'),
(41, 'sideMenu', 'Translations'),
(42, 'sideMenu', 'Categorías'),
(43, 'sideMenu', 'Seo'),
(44, 'sideMenu', 'Seo Model'),
(45, 'sideMenu', 'Url Rules'),
(46, 'Label', 'Permisos'),
(47, 'AuthModule.main', 'Assignments'),
(48, 'AuthModule.main', 'role|roles'),
(49, 'AuthModule.main', 'task|tasks'),
(50, 'AuthModule.main', 'operation|operations'),
(51, 'admin', 'Administration'),
(52, 'labels', 'General tracking code'),
(53, 'Administration', 'Usuarios'),
(54, 'admin', 'Perfil'),
(55, 'admin', 'Editar'),
(56, 'admin', 'Cambiar Contraseña'),
(57, 'admin', 'Salir'),
(58, 'admin', 'Registration'),
(59, 'admin', 'Go to Home'),
(60, 'sideMenu', 'Plan'),
(61, 'admin', 'Are you sure want to delete this item'),
(62, 'admin', 'Actions'),
(63, 'Plan', 'ID'),
(64, 'Plan', 'Title'),
(65, 'Plan', 'Created'),
(66, 'Plan', 'Updated'),
(67, 'Plan', 'Owner'),
(68, 'admin', 'Add item'),
(69, 'admin', 'The data was save with success'),
(70, 'sideMenu', 'Section 2'),
(71, 'ServicePageSection2', 'ID'),
(72, 'ServicePageSection2', 'Background image Alt'),
(73, 'ServicePageSection2', 'Title'),
(74, 'ServicePageSection2', 'Description'),
(75, 'ServicePageSection2', 'Weeknights title'),
(76, 'ServicePageSection2', 'Weeknights time text'),
(77, 'ServicePageSection2', 'Weekends title'),
(78, 'ServicePageSection2', 'Weekends time text'),
(79, 'ServicePageSection2', 'Contact button text'),
(80, 'ServicePageSection2', 'Created'),
(81, 'ServicePageSection2', 'Updated'),
(82, 'ServicePageSection2', 'Owner'),
(83, 'ServicePageSection2', 'Background image'),
(84, 'ServiceBannerSection', 'ID'),
(85, 'ServiceBannerSection', 'Banner Image Alt'),
(86, 'ServiceBannerSection', 'Title'),
(87, 'ServiceBannerSection', 'Description'),
(88, 'ServiceBannerSection', 'Created'),
(89, 'ServiceBannerSection', 'Updated'),
(90, 'ServiceBannerSection', 'Owner'),
(91, 'ServiceBannerSection', 'Banner Image'),
(92, 'admin', 'Add'),
(93, 'admin', 'Create {name}'),
(94, 'admin', 'Text'),
(95, 'Topic', 'ID'),
(96, 'Topic', 'Title'),
(97, 'Topic', 'Time planned'),
(98, 'Topic', 'Description'),
(99, 'Topic', 'To order'),
(100, 'Topic', 'Plan'),
(101, 'Topic', 'Created'),
(102, 'Topic', 'Updated'),
(103, 'Topic', 'Owner'),
(104, 'transfer', 'Add new topic'),
(105, 'app', 'Seo Options'),
(106, 'SeoModel', 'id'),
(107, 'SeoModel', 'url'),
(108, 'SeoModel', 'title'),
(109, 'SeoModel', 'description'),
(110, 'SeoModel', 'keywords'),
(111, 'SeoModel', 'modelClassName'),
(112, 'SeoModel', 'Enabled facebook integration'),
(113, 'SeoModel', 'Enabled twitter integration'),
(114, 'SeoModel', 'Article Publisher'),
(115, 'SeoModel', 'Article Section'),
(116, 'SeoModel', 'Twitter card'),
(117, 'SeoModel', 'Facebook image'),
(118, 'SeoModel', 'Twitter image'),
(119, 'SeoModelMeta', 'id'),
(120, 'SeoModelMeta', 'name'),
(121, 'SeoModelMeta', 'content'),
(122, 'SeoModelMeta', 'seo_model_id'),
(123, 'transfer', 'Add meta line'),
(124, 'SeoModelTrackingCode', 'id'),
(125, 'SeoModelTrackingCode', 'name'),
(126, 'SeoModelTrackingCode', 'code'),
(127, 'SeoModelTrackingCode', 'apply over body section'),
(128, 'SeoModelTrackingCode', 'seo_model_id'),
(129, 'transfer', 'Add tracking code'),
(130, 'admin', 'Back'),
(131, 'admin', 'Save item'),
(132, 'admin', 'Reset form'),
(133, 'admin', 'Remove'),
(134, 'app', 'Correct Seo Errors'),
(135, 'admin', 'Error, had been an error saving item.'),
(136, 'admin', 'Success, item was saved.'),
(137, 'TbEditableField.editable', 'Enter'),
(138, 'admin', 'Edit '),
(139, 'admin', 'Edit {name}'),
(140, 'admin', 'Success, the changes were saved.'),
(141, 'admin', 'Up'),
(142, 'admin', 'Down'),
(143, 'admin', 'View'),
(144, 'admin', 'View {name}'),
(145, 'admin', 'Update item'),
(146, 'admin', 'Remove item'),
(147, 'admin', 'Data was saved with success'),
(148, 'admin', 'Are you sure you want to delete this item?'),
(149, 'HomeBannerSection', 'ID'),
(150, 'HomeBannerSection', 'Image Alt'),
(151, 'HomeBannerSection', 'Video Alt'),
(152, 'HomeBannerSection', 'Alternative video image Alt'),
(153, 'HomeBannerSection', 'Banner title'),
(154, 'HomeBannerSection', 'Banner subtitle'),
(155, 'HomeBannerSection', 'Started button text'),
(156, 'HomeBannerSection', 'Banner type'),
(157, 'HomeBannerSection', 'Created'),
(158, 'HomeBannerSection', 'Updated'),
(159, 'HomeBannerSection', 'Owner'),
(160, 'HomeBannerSection', 'Image'),
(161, 'HomeBannerSection', 'Video'),
(162, 'HomeBannerSection', 'Alternative video image'),
(163, 'admin', 'The image dimensions are 1600x810px'),
(164, 'admin', 'The image dimensions are 1920x1080px'),
(165, 'admin', 'Image'),
(166, 'admin', 'Video'),
(167, 'admin', 'Select'),
(168, 'admin', 'Select de video'),
(169, 'sideMenu', 'Section 3'),
(170, 'admin', 'No'),
(171, 'admin', 'Yes'),
(172, 'ContactusPageSection3', 'ID'),
(173, 'ContactusPageSection3', 'Title'),
(174, 'ContactusPageSection3', 'Use map view'),
(175, 'ContactusPageSection3', 'Longitude'),
(176, 'ContactusPageSection3', 'Latitude'),
(177, 'ContactusPageSection3', 'Map image Alt'),
(178, 'ContactusPageSection3', 'Created'),
(179, 'ContactusPageSection3', 'Updated'),
(180, 'ContactusPageSection3', 'Owner'),
(181, 'ContactusPageSection3', 'Map image'),
(182, 'admin', 'The image dimensions are 1600x440px'),
(183, 'sideMenu', 'General tracking codes'),
(184, 'TrackingCode', 'ID'),
(185, 'TrackingCode', 'Source code'),
(186, 'labels', 'Up to'),
(187, 'labels', 'row(s)'),
(188, 'labels', 'Add item'),
(189, 'labels', 'The data was save with success'),
(190, 'sideMenu', 'Error Page'),
(191, 'sideMenu', 'Thank you'),
(192, 'ErrorPage', 'ID'),
(193, 'ErrorPage', 'Background image Alt'),
(194, 'ErrorPage', 'Title'),
(195, 'ErrorPage', 'Message text'),
(196, 'ErrorPage', 'Button text'),
(197, 'ErrorPage', 'Created'),
(198, 'ErrorPage', 'Updated'),
(199, 'ErrorPage', 'Owner'),
(200, 'ErrorPage', 'Background image'),
(201, 'sideMenu', 'Thanks page'),
(202, 'ThankPage', 'ID'),
(203, 'ThankPage', 'Background image Alt'),
(204, 'ThankPage', 'Title'),
(205, 'ThankPage', 'Subtitle'),
(206, 'ThankPage', 'Description'),
(207, 'ThankPage', 'Home button text'),
(208, 'ThankPage', 'Services button text'),
(209, 'ThankPage', 'Created'),
(210, 'ThankPage', 'Updated'),
(211, 'ThankPage', 'Owner'),
(212, 'ThankPage', 'Background image'),
(213, 'sideMenu', 'Main menu'),
(214, 'MainMenu', 'ID'),
(215, 'MainMenu', 'Portrait image Alt'),
(216, 'MainMenu', 'Home option text'),
(217, 'MainMenu', 'About option text'),
(218, 'MainMenu', 'Service option text'),
(219, 'MainMenu', 'Contact option text'),
(220, 'MainMenu', 'Created'),
(221, 'MainMenu', 'Updated'),
(222, 'MainMenu', 'Owner'),
(223, 'MainMenu', 'Portrait image'),
(224, 'admin', 'The image dimensions are 133x134px'),
(225, 'Footer', 'ID'),
(226, 'Footer', 'Copy right information'),
(227, 'Footer', 'Developer by text'),
(228, 'Footer', 'Developer by link text'),
(229, 'Footer', 'Developer site url'),
(230, 'Footer', 'Created'),
(231, 'Footer', 'Updated'),
(232, 'Footer', 'Owner'),
(233, 'SocialLinks', 'ID'),
(234, 'SocialLinks', 'Facebook url'),
(235, 'SocialLinks', 'Youtube url'),
(236, 'SocialLinks', 'Linkedin url'),
(237, 'SocialLinks', 'Created'),
(238, 'SocialLinks', 'Updated'),
(239, 'SocialLinks', 'Owner'),
(240, 'admin', 'The email isn''t correct'),
(241, 'admin', 'Email already exists!'),
(242, 'CompanyInfo', 'ID'),
(243, 'CompanyInfo', 'Main logo Alt'),
(244, 'CompanyInfo', 'Secondary logo Alt'),
(245, 'CompanyInfo', 'name'),
(246, 'CompanyInfo', 'email'),
(247, 'CompanyInfo', 'phone1'),
(248, 'CompanyInfo', 'phone2'),
(249, 'CompanyInfo', 'Address line1'),
(250, 'CompanyInfo', 'Created'),
(251, 'CompanyInfo', 'Updated'),
(252, 'CompanyInfo', 'Owner'),
(253, 'CompanyInfo', 'Main logo'),
(254, 'CompanyInfo', 'Secondary logo'),
(255, 'admin', 'The image dimensions are 260x70px'),
(256, 'admin', 'The image dimensions are 219x59px'),
(257, 'CompanyInfo', 'Company Address'),
(258, 'AboutusBannerSection', 'ID'),
(259, 'AboutusBannerSection', 'Banner Image Alt'),
(260, 'AboutusBannerSection', 'Title'),
(261, 'AboutusBannerSection', 'Description'),
(262, 'AboutusBannerSection', 'Biography Image Alt'),
(263, 'AboutusBannerSection', 'Biography title'),
(264, 'AboutusBannerSection', 'Biography subtitle'),
(265, 'AboutusBannerSection', 'Biography'),
(266, 'AboutusBannerSection', 'Contact me button text'),
(267, 'AboutusBannerSection', 'Created'),
(268, 'AboutusBannerSection', 'Updated'),
(269, 'AboutusBannerSection', 'Owner'),
(270, 'AboutusBannerSection', 'Banner Image'),
(271, 'AboutusBannerSection', 'Biography Image'),
(272, 'admin', 'The image dimensions are 1600x484px'),
(273, 'admin', 'The image dimensions are 540x410px'),
(274, 'AboutusPageSection2', 'ID'),
(275, 'AboutusPageSection2', 'Left image Alt'),
(276, 'AboutusPageSection2', 'Mission Title'),
(277, 'AboutusPageSection2', 'Mission text'),
(278, 'AboutusPageSection2', 'Services Title'),
(279, 'AboutusPageSection2', 'Services text'),
(280, 'AboutusPageSection2', 'Committed Title'),
(281, 'AboutusPageSection2', 'Committed text'),
(282, 'AboutusPageSection2', 'Created'),
(283, 'AboutusPageSection2', 'Updated'),
(284, 'AboutusPageSection2', 'Owner'),
(285, 'AboutusPageSection2', 'Left image'),
(286, 'admin', 'The image dimensions are 800x960px'),
(287, 'AboutusPageSection3', 'ID'),
(288, 'AboutusPageSection3', 'Right image Alt'),
(289, 'AboutusPageSection3', 'Vision Title'),
(290, 'AboutusPageSection3', 'Vision text'),
(291, 'AboutusPageSection3', 'Clientele Title'),
(292, 'AboutusPageSection3', 'Services text'),
(293, 'AboutusPageSection3', 'Committed Title'),
(294, 'AboutusPageSection3', 'Committed text'),
(295, 'AboutusPageSection3', 'Created'),
(296, 'AboutusPageSection3', 'Updated'),
(297, 'AboutusPageSection3', 'Owner'),
(298, 'AboutusPageSection3', 'Right image'),
(299, 'admin', 'The image dimensions are 799x960px'),
(300, 'admin', 'The image dimensions are 1600x680px'),
(301, 'ContactusBannerSection', 'ID'),
(302, 'ContactusBannerSection', 'Banner Image Alt'),
(303, 'ContactusBannerSection', 'Title'),
(304, 'ContactusBannerSection', 'Description'),
(305, 'ContactusBannerSection', 'Created'),
(306, 'ContactusBannerSection', 'Updated'),
(307, 'ContactusBannerSection', 'Owner'),
(308, 'ContactusBannerSection', 'Banner Image'),
(309, 'ContactusPageSection2', 'ID'),
(310, 'ContactusPageSection2', 'Left image Alt'),
(311, 'ContactusPageSection2', 'Form Title'),
(312, 'ContactusPageSection2', 'From button text'),
(313, 'ContactusPageSection2', 'Created'),
(314, 'ContactusPageSection2', 'Updated'),
(315, 'ContactusPageSection2', 'Owner'),
(316, 'ContactusPageSection2', 'Left image'),
(317, 'admin', 'The image dimensions are 800x900px'),
(318, 'ContactusPageSection2', 'Form button text'),
(319, 'TbEditableField.editable', 'Select'),
(320, 'HomePageSection2', 'ID'),
(321, 'HomePageSection2', 'College image Alt'),
(322, 'HomePageSection2', 'Title'),
(323, 'HomePageSection2', 'Subtitle paragraph1'),
(324, 'HomePageSection2', 'Subtitle paragraph2'),
(325, 'HomePageSection2', 'search button text'),
(326, 'HomePageSection2', 'Description'),
(327, 'HomePageSection2', 'Created'),
(328, 'HomePageSection2', 'Updated'),
(329, 'HomePageSection2', 'Owner'),
(330, 'HomePageSection2', 'College image'),
(331, 'admin', 'The image dimensions are 800x810px'),
(332, 'HomePageSection3', 'ID'),
(333, 'HomePageSection3', 'Image Alt'),
(334, 'HomePageSection3', 'Title'),
(335, 'HomePageSection3', 'Description'),
(336, 'HomePageSection3', 'Created'),
(337, 'HomePageSection3', 'Updated'),
(338, 'HomePageSection3', 'Owner'),
(339, 'HomePageSection3', 'Image'),
(340, 'admin', 'The image dimensions are 1600x880px'),
(341, 'HomePageSection4', 'ID'),
(342, 'HomePageSection4', 'Image Alt'),
(343, 'HomePageSection4', 'Title'),
(344, 'HomePageSection4', 'Description'),
(345, 'HomePageSection4', 'Created'),
(346, 'HomePageSection4', 'Updated'),
(347, 'HomePageSection4', 'Owner'),
(348, 'HomePageSection4', 'Image'),
(349, 'HomePageSection5', 'ID'),
(350, 'HomePageSection5', 'Left Image Alt'),
(351, 'HomePageSection5', 'Right Image Alt'),
(352, 'HomePageSection5', 'Title'),
(353, 'HomePageSection5', 'Subtitle paragraph1'),
(354, 'HomePageSection5', 'Subtitle paragraph2'),
(355, 'HomePageSection5', 'Get started button text'),
(356, 'HomePageSection5', 'Created'),
(357, 'HomePageSection5', 'Updated'),
(358, 'HomePageSection5', 'Owner'),
(359, 'HomePageSection5', 'Left Image'),
(360, 'HomePageSection5', 'Right Image'),
(361, 'admin', 'The image dimensions are 857x900px'),
(362, 'admin', 'The image dimensions are 879x810px'),
(363, 'HomePageSection6', 'ID'),
(364, 'HomePageSection6', 'Title'),
(365, 'HomePageSection6', 'Description'),
(366, 'HomePageSection6', 'Left Image Alt'),
(367, 'HomePageSection6', 'Left image title'),
(368, 'HomePageSection6', 'Left image button text'),
(369, 'HomePageSection6', 'Middle Image Alt'),
(370, 'HomePageSection6', 'Middle image button text'),
(371, 'HomePageSection6', 'Right Image Alt'),
(372, 'HomePageSection6', 'Created'),
(373, 'HomePageSection6', 'Updated'),
(374, 'HomePageSection6', 'Owner'),
(375, 'HomePageSection6', 'Left Image'),
(376, 'HomePageSection6', 'Middle Image'),
(377, 'HomePageSection6', 'Right Image'),
(378, 'admin', 'The image dimensions are 500x347px'),
(379, 'HomeBannerSection', 'Video format'),
(380, 'HomeBannerSection', 'I want add more video formats'),
(381, 'admin', 'video/webm'),
(382, 'admin', 'video/mp4'),
(383, 'admin', 'video/ogg'),
(384, 'HomeBannerVideo', 'ID'),
(385, 'HomeBannerVideo', 'Video Alt'),
(386, 'HomeBannerVideo', 'video format'),
(387, 'HomeBannerVideo', 'Home Banner'),
(388, 'HomeBannerVideo', 'Created'),
(389, 'HomeBannerVideo', 'Updated'),
(390, 'HomeBannerVideo', 'Owner'),
(391, 'HomeBannerVideo', 'Video'),
(392, 'transfer', 'Add new video format'),
(393, 'admin', 'Add the video'),
(394, 'admin', 'Select the video'),
(395, 'admin', 'Alternative video image');

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE IF NOT EXISTS `main_menu` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Main menu"}}',
  `portrait_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Portrait image","dimensions":"133,134"}',
  `home` varchar(255) DEFAULT NULL COMMENT '{"label":"Home option text"}',
  `about` varchar(255) DEFAULT NULL COMMENT '{"label":"About option text"}',
  `service` varchar(255) DEFAULT NULL COMMENT '{"label":"Service option text"}',
  `contact` varchar(255) DEFAULT NULL COMMENT '{"label":"Contact option text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`id`, `portrait_image`, `home`, `about`, `service`, `contact`, `created`, `updated`, `owner`) VALUES
('207457fa-4400-6184-b1c1-484d65198f32', 'Portrait image', 'Home', 'About us', 'Services', 'Contact us', '2015-10-01 19:29:05', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE IF NOT EXISTS `plan` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Services","submenu":"Plan"}}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `title`, `created`, `updated`, `owner`) VALUES
('b141d5e3-ee37-1ef4-2918-672b044c1ac4', 'plan2', '2015-10-02 11:15:10', NULL, 'admin'),
('bd5140ad-d77a-4ed4-99f2-87094bd69b78', 'plan1', '2015-09-29 18:41:26', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  KEY `FKprofiles815537` (`user_id`),
  KEY `profiles_ibfk_1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `profiles_fields`
--

CREATE TABLE IF NOT EXISTS `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(255) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `seo_model`
--

CREATE TABLE IF NOT EXISTS `seo_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_en` varchar(255) DEFAULT NULL,
  `url_es` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_es` varchar(255) DEFAULT NULL,
  `description_en` text,
  `description_es` text,
  `keywords_en` varchar(255) DEFAULT NULL,
  `keywords_es` varchar(255) DEFAULT NULL,
  `include_fb` tinyint(1) DEFAULT '0',
  `include_tw` tinyint(1) DEFAULT '0',
  `article_publisher` varchar(255) DEFAULT '',
  `article_section` varchar(255) DEFAULT '',
  `twitter_card` varchar(255) DEFAULT '',
  `modelClassName` varchar(255) NOT NULL,
  `seo_url_rule_id` int(11) DEFAULT NULL,
  `plan_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_model_seo_url_rule_ididx` (`seo_url_rule_id`),
  KEY `seo_model_plan_ididx` (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `seo_model`
--

INSERT INTO `seo_model` (`id`, `url_en`, `url_es`, `title_en`, `title_es`, `description_en`, `description_es`, `keywords_en`, `keywords_es`, `include_fb`, `include_tw`, `article_publisher`, `article_section`, `twitter_card`, `modelClassName`, `seo_url_rule_id`, `plan_id`) VALUES
(1, 'plan1', 'plan1', 'plan1', 'plan1', NULL, '', NULL, '', 0, 0, NULL, NULL, NULL, 'Plan', NULL, 'bd5140ad-d77a-4ed4-99f2-87094bd69b78');

-- --------------------------------------------------------

--
-- Table structure for table `seo_model_meta`
--

CREATE TABLE IF NOT EXISTS `seo_model_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `seo_model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_model_id_idx1` (`seo_model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_model_tracking_code`
--

CREATE TABLE IF NOT EXISTS `seo_model_tracking_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `apply_over_body` tinyint(1) DEFAULT '0',
  `seo_model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_model_id_idx2` (`seo_model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_url_rule`
--

CREATE TABLE IF NOT EXISTS `seo_url_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route` varchar(1023) DEFAULT NULL,
  `caseSensitive` tinyint(1) NOT NULL,
  `str_params` text,
  `urlSuffix` varchar(255) DEFAULT NULL,
  `defaultParams` text,
  `matchValue` tinyint(1) NOT NULL,
  `verb` varchar(255) DEFAULT NULL,
  `parsingOnly` tinyint(1) NOT NULL,
  `references` text,
  `routePattern` varchar(1023) DEFAULT NULL,
  `template` varchar(1023) DEFAULT NULL,
  `append` tinyint(1) NOT NULL,
  `hasHostInfo` tinyint(1) NOT NULL,
  `order` int(11) DEFAULT NULL COMMENT '@order',
  `modelClassName` varchar(255) DEFAULT NULL,
  `primaryKeyParam` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_banner_section`
--

CREATE TABLE IF NOT EXISTS `service_banner_section` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Service page","submenu":"Banner section"}}',
  `banner_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Banner Image","dimensions":"1600,484"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description","type":"@redactor"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_banner_section`
--

INSERT INTO `service_banner_section` (`id`, `banner_image`, `title`, `description`, `created`, `updated`, `owner`) VALUES
('f625d3d4-f3f2-abd4-6149-29d6b75d8bbc', '', 'Services1', '<p>\r\n                We offer educational planning and consulting services to students and their families.  While it is\r\n                preferred to meet with our clients in person, we understand that you may only be able to meet through\r\n                internet-based video technologies such as Skype, ooVoo, Hangouts, etc.  We also provide some educational\r\n                planning consultations through telephone, although it is less desirable. So join our Janus Scholars\r\n                and fulfill your educational dreams.\r\n            </p><p>\r\n                If time and money are impediments to joining our one-on-one consultations as a Janus Scholar, you may\r\n                want to become a Summer Academy Scholar and receive the guidance, information, and feedback that will\r\n                give you the confidence needed in your college search.\r\n            </p><p>\r\n                Consistent with the HECA Professional Ethical Standards, we never receive money from colleges or other\r\n                organizations for referrals and placements, and we never guarantee students (and their families)\r\n                admissions to any college, university, or postsecondary institution.\r\n            </p><p>\r\n                Consistent with the HECA Professional Ethical Standards, we never receive money from colleges or other\r\n                organizations for referrals and placements, and we never guarantee students (and their families)\r\n                admissions to any college, university, or postsecondary institution.\r\n            </p>', '2015-10-02 10:56:15', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `service_page_section2`
--

CREATE TABLE IF NOT EXISTS `service_page_section2` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Service page","submenu":"Section 2"}}',
  `background_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Background image","dimensions":"1600,680"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `description` text COMMENT '{"label":"Description"}',
  `nights_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Weeknights title"}',
  `nights_hours` varchar(255) DEFAULT NULL COMMENT '{"label":"Weeknights time text"}',
  `weekend_title` varchar(255) DEFAULT NULL COMMENT '{"label":"Weekends title"}',
  `weekend_hours` varchar(255) DEFAULT NULL COMMENT '{"label":"Weekends time text"}',
  `contact_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Contact button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_page_section2`
--

INSERT INTO `service_page_section2` (`id`, `background_image`, `title`, `description`, `nights_title`, `nights_hours`, `weekend_title`, `weekend_hours`, `contact_button_text`, `created`, `updated`, `owner`) VALUES
('d3c62010-15c2-8b44-d145-cde561d58745', '', 'Hours of Availability', 'Understanding that most high school students attend school and their parents work during traditional business hours, we offer our educational planning and consulting services during most weeknights and weekends. ', 'Most Weeknights', '5:30pm - 9:00pm', 'Most Weekends', '5:30pm - 9:00pm', 'Contact us', '2015-10-02 10:59:28', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE IF NOT EXISTS `social_links` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Social Media"}}',
  `facebook` varchar(255) DEFAULT NULL COMMENT '{"type" : "@url", "label" : "Facebook url"}',
  `youtube` varchar(255) DEFAULT NULL COMMENT '{"type" : "@url", "label" : "Youtube url"}',
  `linkedin` varchar(255) DEFAULT NULL COMMENT '{"type" : "@url", "label" : "Linkedin url"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `facebook`, `youtube`, `linkedin`, `created`, `updated`, `owner`) VALUES
('11860024-9b39-5ef4-d5e7-1947d600deb7', 'http://facebook.com', 'http://youtube.com', 'http://linkedin.com', '2015-10-01 19:36:03', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `system_company`
--

CREATE TABLE IF NOT EXISTS `system_company` (
  `id` varchar(50) NOT NULL COMMENT '@uid',
  `name` varchar(255) NOT NULL,
  `logo_image_alt` varchar(100) NOT NULL COMMENT 'recipeImg',
  `description` text COMMENT '@ckeditor',
  `mision` text COMMENT '@redactor',
  `vision` text COMMENT '@redactor',
  `phone` varchar(125) DEFAULT NULL,
  `fax` varchar(125) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL COMMENT '@url',
  `email` varchar(255) NOT NULL COMMENT '@email',
  `full_adress` text COMMENT '@html',
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `customer_support` text NOT NULL COMMENT '@redactor',
  `terms_conditions` text COMMENT '@ckeditor',
  `shared_facebook` varchar(200) NOT NULL COMMENT '@url',
  `shared_google` varchar(200) NOT NULL COMMENT '@url',
  `shared_twitter` varchar(200) NOT NULL COMMENT '@url',
  `shared_youtube` varchar(200) NOT NULL COMMENT '@url',
  `msg_newUser` text NOT NULL COMMENT '@redactor',
  `msg_contactUs` text NOT NULL COMMENT '@redactor',
  `msg_newsletter` text NOT NULL COMMENT '@redactor',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `fax` (`fax`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_company`
--

INSERT INTO `system_company` (`id`, `name`, `logo_image_alt`, `description`, `mision`, `vision`, `phone`, `fax`, `url`, `email`, `full_adress`, `longitude`, `latitude`, `customer_support`, `terms_conditions`, `shared_facebook`, `shared_google`, `shared_twitter`, `shared_youtube`, `msg_newUser`, `msg_contactUs`, `msg_newsletter`) VALUES
('ccc606ed-a16c-a854-c117-9ac834b90a4d', 'iReevo', 'iReevo', '<p><span  style="background-color: initial;"></span></p>', NULL, NULL, '305-447-2300', '786-522-0627', 'http://www.iReevo.com', 'info@iReevo.com', '<div>4138 SW 16th, Terrace</div><div>Miami, FL 33134</div>', -80.2627, 25.7564, '', NULL, 'https://www.facebook.com/pages/Havana-Air/604401099671245?sk=timeline', 'http://www.google.com', 'http://www.twiter.com', 'http://www.youtube.com', '<p>welcome</p>', '<p>welcome</p>', '<p>welcome</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1443303286),
('m150222_034944_init_seo', 1443303594),
('m150224_210502_create_default_DB', 1443303290),
('m150304_143251_2015_03_04_create_tables', 1443303292),
('m150320_145153_social_networks_2015_03_20', 1443303595),
('m150320_165529_tracking_code_2015_03_20', 1443303595),
('m150413_145928_social_networks_images_2015_04_13', 1443303596),
('m150413_151242_social_networks_twitter_card_2015_04_13', 1443303596),
('m150929_174539_2015_09_29_topic_order', 1443548811),
('m150930_190701_general_tracking_code_add_2015_09_30', 1443640132),
('m151001_112609_adding_error_tanks_pages_2015_10_01', 1443699297),
('m151004_203733_2015_09_22_banner_video_formats', 1443992163);

-- --------------------------------------------------------

--
-- Table structure for table `text`
--

CREATE TABLE IF NOT EXISTS `text` (
  `id` varchar(50) NOT NULL COMMENT '@uid',
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_es` varchar(255) NOT NULL,
  `description` text NOT NULL COMMENT '@redactor',
  `description_en` text NOT NULL,
  `description_es` text NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `text_category_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtext193647` (`text_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `text_category`
--

CREATE TABLE IF NOT EXISTS `text_category` (
  `id` varchar(50) NOT NULL COMMENT '@uid',
  `category` varchar(255) NOT NULL,
  `description` text COMMENT '@redactor',
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thank_page`
--

CREATE TABLE IF NOT EXISTS `thank_page` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"General","submenu":"Thanks page"}}',
  `background_image` varchar(255) NOT NULL COMMENT '{"type":"recipeImg1","label":"Background image","dimensions":"1600,810"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `subtitle` varchar(255) DEFAULT NULL COMMENT '{"label":"Subtitle"}',
  `description` text COMMENT '{"label":"Description"}',
  `home_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Home button text"}',
  `services_button_text` varchar(255) DEFAULT NULL COMMENT '{"label":"Services button text"}',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thank_page`
--

INSERT INTO `thank_page` (`id`, `background_image`, `title`, `subtitle`, `description`, `home_button_text`, `services_button_text`, `created`, `updated`, `owner`) VALUES
('1492855b-5c89-fbf4-51a6-5f4032757687', '', 'Thank you', 'for trust in our expertise', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut  laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commo', 'Go Homepage', 'Our Services', '2015-10-01 19:47:27', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
  `id` varchar(50) NOT NULL COMMENT '{"type":"@uid","rows":1,"breadcrumbs":{"tablename":"Services","submenu":"Topic"}}',
  `orderr` int(11) DEFAULT '1' COMMENT '{"label":"Topic Order"}',
  `title` varchar(255) DEFAULT NULL COMMENT '{"label":"Title"}',
  `time` varchar(255) DEFAULT NULL COMMENT '{"label":"Time planned"}',
  `description` text COMMENT '{"label":"Description"}',
  `plan` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topics_plan_id_fk1` (`plan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `orderr`, `title`, `time`, `description`, `plan`, `created`, `updated`, `owner`) VALUES
('539e0041-d6cf-96c4-fda3-495ae75bc1b5', 2, 'prueba2', 'tiempo', 'prueba2', 'b141d5e3-ee37-1ef4-2918-672b044c1ac4', '2015-10-02 11:15:10', NULL, 'admin'),
('5b1bcf51-f62f-a354-0db0-fb19747bbb1e', 1, 'prueba1', '3:00 PM - 5: 00 PM', '<p>sdsdasdasdd</p>', 'bd5140ad-d77a-4ed4-99f2-87094bd69b78', '2015-09-29 18:41:26', NULL, 'admin'),
('86c25ebe-a835-b4e4-79bc-22d507278360', 1, 'prueba', 'tiempo', 'prueba', 'b141d5e3-ee37-1ef4-2918-672b044c1ac4', '2015-10-02 11:15:10', NULL, 'admin'),
('d85464b2-5629-edf4-3d3a-8fbddf45b124', 2, 'prueba3', '1:00 PM - 3: 00 PM', '<p>dfdgfdgfg</p>', 'bd5140ad-d77a-4ed4-99f2-87094bd69b78', '2015-09-29 18:49:32', NULL, 'admin'),
('dc6690a4-b78b-0d64-018e-659533ae8ac5', 3, 'prueba2', '7:00 PM - 9: 00 PM', '<p>ertertrtrtrtrt</p>', 'bd5140ad-d77a-4ed4-99f2-87094bd69b78', '2015-09-29 18:41:26', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `createtime`, `lastvisit`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@ireevo.com', '309d34ef076838ffac800b82ddcdb336', 1261146094, 1380542420, 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `home_banner_video`
--
ALTER TABLE `home_banner_video`
  ADD CONSTRAINT `video_home_banner_id_fk1` FOREIGN KEY (`home_banner`) REFERENCES `home_banner_section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `i18n_message`
--
ALTER TABLE `i18n_message`
  ADD CONSTRAINT `FKi18n_messa482734` FOREIGN KEY (`language`) REFERENCES `i18n_language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKi18n_messa868058` FOREIGN KEY (`id`) REFERENCES `i18n_source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `i18n_source`
--
ALTER TABLE `i18n_source`
  ADD CONSTRAINT `FKi18n_sourc696102` FOREIGN KEY (`category`) REFERENCES `i18n_category` (`category`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `FKprofiles815537` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seo_model`
--
ALTER TABLE `seo_model`
  ADD CONSTRAINT `seo_model_plan_idfk` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `seo_model_seo_url_rule_idfk` FOREIGN KEY (`seo_url_rule_id`) REFERENCES `seo_url_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seo_model_meta`
--
ALTER TABLE `seo_model_meta`
  ADD CONSTRAINT `seo_model_id_fk1` FOREIGN KEY (`seo_model_id`) REFERENCES `seo_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seo_model_tracking_code`
--
ALTER TABLE `seo_model_tracking_code`
  ADD CONSTRAINT `seo_model_id_fk2` FOREIGN KEY (`seo_model_id`) REFERENCES `seo_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `text`
--
ALTER TABLE `text`
  ADD CONSTRAINT `text_ibfk_1` FOREIGN KEY (`text_category_id`) REFERENCES `text_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `topic`
--
ALTER TABLE `topic`
  ADD CONSTRAINT `topics_plan_id_fk1` FOREIGN KEY (`plan`) REFERENCES `plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
