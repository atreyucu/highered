<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
    public $name;
    public $email;
    public $subject;
    public $body;
//    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('name, email, body', 'required'),
            // email has to be a valid email address
            array('subject', 'safe'),
            array('email', 'email'),
            // verifyCode needs to be entered correctly
//            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name' => Yii::t('contact-us','Your name'),
            'email' => Yii::t('contact-us','Your email'),
            'body' => Yii::t('contact-us','Message'),
//            'verifyCode' => 'Verification Code',
        );
    }

    public function process(){
        /** @var CompanyInfo $company_info */
        $company_info = CompanyInfo::model()->find();
       $to_email = isset($company_info->email) ? $company_info->email : Yii::app()->params['contactEmail'];
       $to_name = isset($company_info->name) ? $company_info->name: Yii::app()->params['contactName'];
        $message = '<h2> Your message was received </h2><br>';
        $message= $message.'<p>Thanks for contacting us we will reply to you shortly.</p> <br><br>';
        $subject1 = isset($this->subject) ? $this->subject : $this->name. ' '. Yii::t('contact-us','wants contact you');
        $subject = Yii::t('contact-us','Thanks for contact us');

        $message1 = t("Message from: ").'<br><br>'.
            Yii::t('contact-us',"Name: ").$to_name.'<br>'.
            Yii::t('contact-us',"Email: ").$to_email.'<br>'.
            Yii::t('contact-us',"Message: "). $message;

        if($this->sendMail($this->name, $this->email, $to_email, $subject1, $message1) && $this->sendMail($to_name, $to_email, $this->email, $subject, $message, $description = 'Email notification')){
            return true;
        }
        else{
            return false;
        }

    }

    public function sendMail($from_name,$from_email, $to_email, $subject,$message, $description = 'Contact') {

        $mail = new YiiMailer('contact', array('message' => nl2br($message), 'name' => $from_email, 'description' => $description));

        //set properties
        $mail->setFrom($from_email, $from_name);
        $mail->setSubject($subject);
        $mail->setTo($to_email);

        if ($mail->send()) {
            Yii::app()->user->setFlash('success', Yii::t('contact-us','Thanks for contact us. We answers you soon as possible.'));
            return true;
        } else {
            Yii::app()->user->setFlash('error', Yii::t('contact-us','Error sending email: ').$mail->getError());
            return false;
        }

    }
}