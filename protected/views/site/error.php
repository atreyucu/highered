
<!--------------------------  End Header --------------->
<div class="error-404">
    <div class="container">
        <div class="error">
            <h2>Error <?php echo $code; ?></h2>
            <p><?php echo CHtml::encode($message); ?></p>
        </div>
    </div>
</div>


