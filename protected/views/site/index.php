<?php
/* @var $this SiteController */
$this->breadcrumbs = array(
    Yii::t('admin','Home'),
);

/*$this->widget(
    'application.extensions.bootstrap.widgets.TbJumbotron',
    array(
//        'heading' => t('Welcome to our Backend').' '.Yii::app()->name,
        'heading' => t('Welcome to ').Yii::app()->name.t(' Backend'),
    )
);*/

if (isset($entity[0])) {

    $this->widget(
        'application.extensions.bootstrap.widgets.TbJumbotron',
        array(
            'heading' => Yii::t('admin','Welcome to ') . $entity[0]->name,
        )
    );
} else {

    $this->widget(
        'application.extensions.bootstrap.widgets.TbJumbotron',
        array(
            'heading' => Yii::t('admin','Welcome to ') . Yii::app()->name,
        )
    );
}


?>


