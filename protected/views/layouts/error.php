<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 7/22/14
 * Time: 10:36 PM
 */


$base_url = Yii::app()->request->baseUrl;
$cs = Yii::app()->clientScript;


$entity_data=SystemCompany::model()->findByPk("ccc606ed-a16c-a854-c117-9ac834b90a4d");
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->widget('application.modules.irseo.widgets.IRSeo'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <!--------------------------  End Head Styles ------------------>
    <?php
    Yii::app()->clientScript->scriptMap = array(
        'bootstrap.min.css' => false,
        'font-awesome.min.css' => false,
        'bootstrap-yii.css' => false,
        'jquery-ui-bootstrap.css' => false,

        'jquery.min.js' => false,
        'jquery.js' => false,
        'bootstrap-noconflict.js' => false,
        'bootbox.min.js' => false,
        'notify.min.js' => false,
        'bootstrap.min.js' => false,
    ); // OJO
    ?>

    <!--------------------------  Head Scripts ------------------>
    <script src="<?php echo $base_url?>/static/js/html5shiv.js"></script>
    <script src="<?php echo $base_url?>/static/js/respond.min.js"></script>

    <script src="<?php echo $base_url?>/static/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo $base_url?>/static/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url?>/static/js/jquery-ui.min.js"></script>
    <!--------------------------  Styles ------------------>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <?php

    $cs->registerCssFile($base_url . '/static/css/font-awesome.css');
    $cs->registerCssFile($base_url . '/static/css/bootstrap.css');
    $cs->registerCssFile($base_url . '/static/css/jquery-ui.min.css');
    $cs->registerCssFile($base_url . '/static/css/style.css');

    ?>
    <!--------------------------  End Styles --------------->


</head>

<body>

<!--------------------------  Header ------------------->
<nav class="navbar navbar-default">
    <div class="hidden-xs" id="top-navbar">
        <div class="container">
            <div class="row">
                <ul>
                    <li><a href="/about" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='about' && Yii::app()->controller->action->id=='about') ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'About Us') ?></a></li>
                    <li><a href="/requeriments" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='requeriments' && Yii::app()->controller->action->id=='requeriments') ? 'active' : ''; ?>" ><?php echo Yii::t('frontend', 'Requirements') ?></a></li>
                    <li><a href="/ofac" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='ofac' && Yii::app()->controller->action->id=='ofac') ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'OFAC') ?></a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"
                                            href="#" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='tsp' && (Yii::app()->controller->action->id=='tsp' || Yii::app()->controller->action->id=='tspLogin')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Tsp Agents') ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="/tsp"><?php echo Yii::t('frontend', 'TSP Register')?></a></li>
                            <li><a href="/tsp-login"><?php echo Yii::t('frontend', 'Log In') ?></a></li>
                        </ul>
                    </li>
                    <li><a href="contactus" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='contact' && Yii::app()->controller->action->id=='contact') ? 'active' : ''; ?>" ><?php echo Yii::t('frontend', 'Contact Us') ?></a></li>
                </ul>
                <div class="pull-right lang-selector">
                    <?php
                    if ($_SESSION['language'] == 'en' || $_SESSION['language'] =='' || !isset($_SESSION['language'])):?>
                    <a id="lang_en" class="current-lang" href="/changelanguage/en">ENG</a><a id="lang_es"
                                                                                             style="cursor: pointer"
                                                                                             href="/changelanguage/es">SPA</a>
                </div>
                <a class="pull-right my-request hidden-xs" href="/request"><?php echo Yii::t('frontend', 'MY REQUEST') ?></a>
                <?php else: ?>
                <a id="lang_en" href="/changelanguage/en">ENG</a><a id="lang_es" class="current-lang"
                                                                    href="/changelanguage/es">SPA</a></div>
            <a class="pull-right my-request hidden-xs" href="<?php echo $this->createUrl('/frontend/request/request')?>"><?php echo Yii::t('frontend', 'MY REQUEST') ?></a>
            <?php endif; ?>
        </div>
    </div>
    </div>
    <div class="container" id="main-navbar">
        <div class="row">
            <div class="navbar-header"><a href="/"><?php echo CHtml::image($entity_data->imageAR->getFileUrl('logo_app'), $entity_data->logo_image_alt);?></a></div>
            <button class="xs-nav-toggler btn btn-default pull-right visible-xs navbar-toggle" data-target="#navigation"
                    data-toggle="collapse" type="button"><i class="fa fa-bars"></i></button>
            <button class="xs-nav-search btn btn-default pull-left visible-xs" data-target="#search-bar"
                    data-toggle="collapse" type="button"><i class="fa fa-search"></i></button><span
                class="right-links hidden-xs">
            <button class="btn search-btn" data-target="#search-bar" data-toggle="collapse" type="button"><i
                    class="fa fa-search"></i></button><a class="contact-link" href="tel:<?php echo str_replace('-','',$entity_data->phone);?>"><i class="fa fa-phone">&nbsp;</i><?php echo $entity_data->phone;?></a><a
                    class="contact-link" href="mailto:<?php echo $entity_data->email;?>"><i class="fa fa-envelope-o">&nbsp;</i><?php echo $entity_data->email;?></a></span>
            <ul id="navigation" class="nav navbar-nav collapse navbar-collapse">
                <li><a href="/" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='default' && Yii::app()->controller->action->id=='index') ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Home'); ?></a></li>
                <li><a href="/flights" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='flight' && Yii::app()->controller->action->id=='flightcategory')? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Flights'); ?></a></li>
                <li><a href="/tours" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='tour' && (Yii::app()->controller->action->id=='tourcategory' || Yii::app()->controller->action->id=='tourdetails')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Tour Programs'); ?></a></li>
                <li><a href="/hotels" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='hotel' && (Yii::app()->controller->action->id=='hotelcategory' || Yii::app()->controller->action->id=='hoteldetails')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Hotels'); ?></a></li>
                <li><a href="/cars" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='car' && (Yii::app()->controller->action->id=='carcategory' || Yii::app()->controller->action->id=='cardetails')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Car Rental'); ?></a></li>
                <li><a href="/restaurants" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='restaurant' && (Yii::app()->controller->action->id=='restaurantcategory' || Yii::app()->controller->action->id=='restaurantdetails')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Restaurants'); ?></a></li>
                <li><a href="/culturalIdeas" class="<?php echo (Yii::app()->controller->module->id=='frontend' && Yii::app()->controller->id=='travel' && (Yii::app()->controller->action->id=='travelcategory' || Yii::app()->controller->action->id=='traveldetails')) ? 'active' : ''; ?>"><?php echo Yii::t('frontend', 'Cultural Ideas'); ?></a></li>
            </ul>

        </div>
    </div>
    <div class="visible-xs" id="bottom-navbar">
        <ul>
            <li><a href="tel:<?php echo str_replace('-','',$entity_data->phone);?>"><i class="fa fa-phone">&nbsp;</i><?php echo $entity_data->phone;?>/a></li>
            <li><a href="mailto:<?php echo $entity_data->email;?>"><i class="fa fa-envelope-o">&nbsp;</i><?php echo $entity_data->email;?></a></li>
        </ul>
        <div class="pull-right lang-selector"><a class="current-lang" href="#">ENG</a><a href="#">SPA</a></div>
    </div>
    <div class="collapse" id="search-bar">
        <form class="form-inline">
            <div class="form-group">
                <label class="sr-only" for="navbar-search-text"><?php echo Yii::t('frontend', 'Search text') ?></label>
                <input type="text" class="form-control" name="navbar-search-text">
            </div>
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</nav>
<!--------------------------  End Header --------------->

<?php echo $content; ?>

<!--------------------------  Footer ------------------->

<div class="footer">
    <div class="red-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <table>
                        <tbody>
                        <tr>
                            <td class="table-img-link"><a href="/"><i class="fa fa-plane"></i></a></td>
                            <td class="hidden-xs">
                                <div class="bar-text">
                                    <h1><?php echo CHtml::link($this->common_data['footer']['plane_title'],$this->createUrl('/flights')) ?></h1>

                                    <p><?php echo $this->common_data['footer']['plane_description'] ?></p>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <table>
                        <tbody>
                        <tr>
                            <td class="table-img-link"><a href="/"><i class="fa fa-suitcase"></i></a></td>
                            <td class="hidden-xs">
                                <div class="bar-text">
                                    <h1><?php echo CHtml::link($this->common_data['footer']['suitcase_title'],$this->createUrl('/hotels')) ?></h1>

                                    <p><?php echo $this->common_data['footer']['suitcase_description'] ?></p>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <table>
                        <tbody>
                        <tr>
                            <td class="table-img-link"><a class="img-link" href="/"><i class="fa fa-star"></i></a></td>
                            <td class="hidden-xs">
                                <div class="bar-text">
                                    <h1><?php echo CHtml::link($this->common_data['footer']['star_title'],$this->createUrl('/tours')) ?></h1>

                                    <p><?php echo $this->common_data['footer']['star_description'] ?></p>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container bottom-bar">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4 col-sm-push-9 col-md-push-8 col-lg-push-8 newsletter">
                <div class="row">
                    <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 col-xs-push-6 col-sm-push-0">
                        <h1><?php echo Yii::t('frontend', 'Newsletter') ?></h1>

                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="newsletter-email"
                                           name="newsletter_email" onkeyup="validate_newsletter_form()"><span
                                        class="input-group-btn">
                        <button class="btn btn-primary" id="newsletter-start"><i class="fa fa-arrow-right"></i></button></span>
                                </div>
                                <div class="alert alert-danger" style="display: none; margin-top: 2px;"
                                     id="newsletter-result"></div>
                                <div class="full-form" style="display: none">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter Name"
                                               id="newsletter-name" name="newsletter_name"
                                               onkeyup="validate_newsletter_form()">
                                    </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter Last Name"
                                               id="newsletter-last-name" name="newsletter_last_name"
                                               onkeyup="validate_newsletter_form()">
                                    </div>
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Enter Message" rows="3" type="text"
                                                  id="newsletter-message" name="newsletter_message"
                                                  onkeyup="validate_newsletter_form()"></textarea>
                                    </div>
                                    <button class="btn btn-primary" id="newsletter-send"
                                            onclick="addNewsLetter()"><?php echo Yii::t('frontend', 'SEND EMAIL') ?></button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 col-xs-pull-6 col-sm-pull-0">
                        <h1><?php echo Yii::t('frontend', 'Customer Support') ?></h1><a class="phone" href="tel:<?php echo str_replace('-','',$entity_data->phone);?>"><?php echo $entity_data->phone;?></a><a
                            class="hidden-xs email" href="mailto:<?php echo $entity_data->email;?>"><?php echo $entity_data->email;?></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4 col-sm-pull-3 col-md-pull-4 col-lg-pull-4 socialize">
                <h1><?php echo Yii::t('frontend', "Let's socialize") ?></h1>

                <div class="social-btns">
                    <?php if($entity_data->shared_facebook!='') :?>
                        <div><a class="btn social-facebook" href="<?php echo $entity_data->shared_facebook;?>"><i class="fa fa-facebook"></i></a></div>
                    <?php endif?>
                    <?php if($entity_data->shared_twitter!='') :?>
                        <div><a class="btn social-twitter" href="<?php echo $entity_data->shared_twitter;?>"><i class="fa fa-twitter"></i></a></div>
                    <?php endif?>
                    <?php if($entity_data->shared_google!='') :?>
                        <div><a class="btn social-google-plus" href="<?php echo $entity_data->shared_google;?>"><i class="fa fa-google-plus"></i></a></div>
                    <?php endif?>
                    <?php if($entity_data->shared_youtube!='') :?>
                        <div><a class="btn social-youtube" href="<?php echo $entity_data->shared_youtube;?>"><i class="fa fa-youtube"></i></a></div>
                    <?php endif?>
                </div>
                <a href="#"><?php echo CHtml::image($entity_data->imageAR->getFileUrl('logo_app'), $entity_data->logo_image_alt,array('class'=>'hidden-xs'))?></a>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-sm-pull-3 col-md-pull-4 col-lg-pull-4 footer-links">
                <h1><?php echo Yii::t('frontend', 'Useful links') ?></h1>
                <?php if ($this->common_data['footer']['link_url1']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url1'] ?>"><?php echo $this->common_data['footer']['link_text1'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url2']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url2'] ?>"><?php echo $this->common_data['footer']['link_text2'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url3']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url3'] ?>"><?php echo $this->common_data['footer']['link_text3'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url4']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url4'] ?>"><?php echo $this->common_data['footer']['link_text4'] ?></a><?php endif; ?>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-sm-pull-3 col-md-pull-4 col-lg-pull-4 footer-links">
                <h1><?php echo Yii::t('frontend', 'Travel specialist') ?></h1>
                <?php if ($this->common_data['footer']['link_url5']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url5'] ?>"><?php echo $this->common_data['footer']['link_text5'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url6']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url6'] ?>"><?php echo $this->common_data['footer']['link_text6'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url7']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url7'] ?>"><?php echo $this->common_data['footer']['link_text7'] ?></a><?php endif; ?>
                <?php if ($this->common_data['footer']['link_url8']): ?><a
                    href="<?php echo $this->common_data['footer']['link_url8'] ?>"><?php echo $this->common_data['footer']['link_text8'] ?></a><?php endif; ?>
            </div>
        </div>
    </div>
    <div class="gray-bar"><span><a href="/"><?php echo Yii::t('frontend', 'Home') ?></a></span>
        <span><a href="/about"><?php echo Yii::t('frontend', 'About') ?></a></span>
        <span><a href="/privacy-police"><?php echo Yii::t('frontend', 'Privacy Policy') ?></a></span>
        <span><a href="/requeriments"><?php echo Yii::t('frontend', 'Requirements') ?></a></span>

        <span><a href="/contactus"><?php echo Yii::t('frontend', 'Contact') ?></a></span>

        <div class="back-top-wrapper">
            <button class="control-indicator control-indicator-dark-red center-block btn" id="back-to-top"><i
                    class="fa fa-angle-up"></i></button>
        </div>
    </div>
</div>
<!--------------------------  End Footer --------------->

<!--------------------------  Scripts --------------->

<script src="<?php echo $base_url?>/static/js/jquery.validate.min.js"></script>
<script src="<?php echo $base_url?>/static/js/custom.js"></script>
<script src="<?php echo $base_url?>/static/js/frontend.js"></script>
<script src="<?php echo $base_url?>/static/js/hotels.js"></script>
<script src="<?php echo $base_url?>/static/js/flight.js"></script>

<!--------------------------  End Scripts --------------->

</body>
</html>
