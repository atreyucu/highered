<?php

class DayOrderUpdaterCommand extends CConsoleCommand
{
    public function actionUpdate(){
        $tours = Tour::model()->findAll();

        foreach($tours as $tour){
            $tour_day_criteria = new CDbCriteria();
            $tour_day_criteria->compare('tour_id', $tour->id);
            $tour_day_criteria->order = 'created asc';
            $tour_days = TourDay::model()->findAll($tour_day_criteria);

            $order = 1;

            foreach($tour_days as $day)
            {
                $day->day_order = $order;
                $order++;
                $day->save();
            }
        }
    }
}
