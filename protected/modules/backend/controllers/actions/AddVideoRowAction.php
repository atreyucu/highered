<?php

class AddVideoRowAction extends CAction
{
    public function run()
    {
        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = new TbActiveForm();
        $form->showErrors = false;
        $form->type = 'inline';
        $data = new HomeBannerVideo();
        return $this->controller->renderPartial('backend.views.homeBannerSection._videoForm', array('form' => $form, 'data' => $data));
    }
} 