<?php

class AddTopicRowAction extends CAction
{
    public function run()
    {
        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = new TbActiveForm();
        $form->showErrors = false;
        $form->type = 'inline';
        $data = new Topic();
        return $this->controller->renderPartial('backend.views.plan._topicForm', array('form' => $form, 'data' => $data));
    }
} 