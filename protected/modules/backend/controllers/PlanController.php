

<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

class PlanController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view','updateAttribute'),
				'users'=>array('@'),
				),
            /*array('allow',
	            'actions'=>array('index', 'view','update','admin'),
	            'users'=>user()->getPermissionbyRoles(array('Level-1','Level-2','Level-3')),
	        ),*/
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>mod('user')->getAdmins(),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

public function actionView($id) {
    $model = $this->loadModel($id, 'Plan');
    $t_manager = TopicManager::load($model);
    $this->render('view', array(
    't_manager'=>$t_manager,
    'model' => $model,
    ));
}

public function actionCreate() {
    $model = new Plan;


    $this->performAjaxValidation($model, 'plan-form');

if (isset($_POST['Plan'])) {
        $model->setAttributes($_POST['Plan']);
        if ($model->save()) {
            $topics = (array)$_POST['Topic'];
            foreach($topics as $topic){
                $temp_topic = new Topic();
                $temp_topic->setAttributes($topic);
                $temp_topic->plan = $model->id;
                $temp_topic->save();
            }

            if (Yii::app()->getRequest()->getIsAjaxRequest())
                Yii::app()->end();
            else{
                Yii::app()->user->setFlash('success',Yii::t('admin','Success, item was saved.'));
                if (Yii::app()->request->getParam('id'))
                {
                    if(Yii::app()->request->getParam('action')=='create')
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                else
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                }
                else
                    $this->redirect(array('admin'));
            }
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving item.'));
        }
    }

    $t_manager = TopicManager::load($model);

    $this->render('create', array( 'model' => $model, 't_manager'=>$t_manager));
}

public function actionUpdate($id) {
    $model = $this->loadModel($id, 'Plan');

    $this->performAjaxValidation($model, 'plan-form');

    if (isset($_POST['Plan'])) {
        $model->setAttributes($_POST['Plan']);
        if ($model->save()) {
            $topics = (array)$_POST['Topic'];

            $del_top_criteria = new CDbCriteria();
            $del_top_criteria->addNotInCondition('id',array_keys($topics));
            $del_top_criteria->compare('plan',$model->id);

            Topic::model()->deleteAll($del_top_criteria);

            foreach($topics as $key=>$topic){
                if(substr_count($key,'newtopic')>0){
                    $temp_topic = new Topic();
                    $temp_topic->setAttributes($topic);
                    $temp_topic->plan = $model->id;
                    $temp_topic->save();
                }
                else{
                    $topic_obj = Topic::model()->findByPk($key);
                    $topic_obj->setAttributes($topic);
                    $topic_obj->save();
                }
            }

            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the changes were saved.'));
            $this->redirect(array('admin'));
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving the item.'));
        }
    }

    $t_manager = TopicManager::load($model);

    $this->render('update', array(
        'model' => $model,
        't_manager'=>$t_manager
    ));
}

public function actionDelete($id)
{
    if(isset($id)){
        if($this->loadModel($id,"Plan")->delete()){
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the item was deleted.'));
            $this->redirect(array('admin'));
        }
        else{
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, exist a native error to delete the item: '.$id.', to resolve this problem, please contact with the database administrator.'));
        }
    }
    else {
        Yii::app()->user->setFlash('error',Yii::t('admin','Error, the item '.$id.' is not defined.'));
    }
}

public function actionAdmin() {
    $model = new Plan('search');
    $model->unsetAttributes();

    if (isset($_GET['Plan']))
        $model->setAttributes($_GET['Plan']);

    $this->render('admin', array(
        'model' => $model,
    ));
}

public function actionUpdateAttribute($model)
{
    if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
        Yii::import("bootstrap.widgets.TbEditableSaver");
        $editableSaver = new TbEditableSaver($model);
        $editableSaver->update();
        app()->end();
    }
}

}