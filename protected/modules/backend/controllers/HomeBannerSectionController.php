

<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

class HomeBannerSectionController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view','updateAttribute'),
				'users'=>array('@'),
				),
            /*array('allow',
	            'actions'=>array('index', 'view','update','admin'),
	            'users'=>user()->getPermissionbyRoles(array('Level-1','Level-2','Level-3')),
	        ),*/
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>mod('user')->getAdmins(),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

public function actionView($id) {
    $model = $this->loadModel($id, 'HomeBannerSection');
    $v_manager = VideoManager::load($model);
    $this->render('view', array(
    'model' => $model,
     'v_manager' => $v_manager,
    ));
}

public function actionCreate() {
    $model = new HomeBannerSection;

    $this->performAjaxValidation($model, 'home-banner-section-form');

    if (isset($_POST['HomeBannerSection'])) {
        $model->setAttributes($_POST['HomeBannerSection']);

        if ($model->save()) {
            $upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerSection/';

            if($_FILES['HomeBannerSection']["tmp_name"]['recipeImg2']){
                $temp_extension = explode('.',$_FILES['HomeBannerSection']["name"]['recipeImg2']);
                $video_path = $upload_dir.'video_'.$model->id.'_normal.'.$temp_extension[1];
                move_uploaded_file($_FILES['HomeBannerSection']["tmp_name"]['recipeImg2'],$video_path);
            }


            $upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerVideo/';

            $videos = (array)$_POST['HomeBannerVideo'];

            foreach($videos as $key=>$video){
                $temp_vid = new HomeBannerVideo();
                $temp_vid->setAttributes($video);
                $temp_vid->home_banner = $model->id;
                $temp_vid->save();
                $temp_extension = explode('.',$_FILES['HomeBannerVideo']["name"][$key]['recipeImg1']);
                $video_path = $upload_dir.'video_'.$temp_vid->id.'_normal.'.$temp_extension[1];
                move_uploaded_file($_FILES['HomeBannerVideo']["tmp_name"][$key]['recipeImg1'],$video_path);
            }

            if (Yii::app()->getRequest()->getIsAjaxRequest())
                Yii::app()->end();
            else{
                Yii::app()->user->setFlash('success',Yii::t('admin','Success, item was saved.'));
                if (Yii::app()->request->getParam('id'))
                {
                    if(Yii::app()->request->getParam('action')=='create')
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                else
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                }
                else
                    $this->redirect(array('admin'));
            }
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving item.'));
        }
    }

    $v_manager = VideoManager::load($model);

    $this->render('create', array( 'model' => $model, 'v_manager'=>$v_manager));
}

public function actionUpdate($id) {
    $model = $this->loadModel($id, 'HomeBannerSection');

    $this->performAjaxValidation($model, 'home-banner-section-form');

    if (isset($_POST['HomeBannerSection'])) {
        $model->setAttributes($_POST['HomeBannerSection']);
        if ($model->save()) {
            $upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerSection/';

            if($_FILES['HomeBannerSection']["tmp_name"]['recipeImg2']){
                $temp_extension = explode('.',$_FILES['HomeBannerSection']["name"]['recipeImg2']);
                $video_path = $upload_dir.'video_'.$model->id.'_normal.'.$temp_extension[1];
                move_uploaded_file($_FILES['HomeBannerSection']["tmp_name"]['recipeImg2'],$video_path);
            }


            $upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerVideo/';

            $videos = (array)$_POST['HomeBannerVideo'];

            //ddump($videos);

            $del_vid_criteria = new CDbCriteria();
            $del_vid_criteria->addNotInCondition('id',array_keys($videos));
            $del_vid_criteria->compare('home_banner',$model->id);


            HomeBannerVideo::model()->deleteAll($del_vid_criteria);
            foreach($videos as $key=>$video){
                if(substr_count($key,'newvideo')){
                    $temp_vid = new HomeBannerVideo();
                    $temp_vid->setAttributes($video);
                    $temp_vid->home_banner = $model->id;
                    $temp_vid->save();
                    $temp_extension = explode('.',$_FILES['HomeBannerVideo']["name"][$key]['recipeImg1']);
                    $video_path = $upload_dir.'video_'.$temp_vid->id.'_normal.'.$temp_extension[1];
                    move_uploaded_file($_FILES['HomeBannerVideo']["tmp_name"][$key]['recipeImg1'],$video_path);
                }
                else{
                    $vid_obj = HomeBannerVideo::model()->findByPk($key);
                    $vid_obj->setAttributes($video);
                    $vid_obj->save();
                    if($_FILES['HomeBannerVideo']["tmp_name"][$key]['recipeImg1']){
                        $temp_extension = explode('.',$_FILES['HomeBannerVideo']["name"][$key]['recipeImg1']);
                        $video_path = $upload_dir.'video_'.$vid_obj->id.'_normal.'.$temp_extension[1];
                        move_uploaded_file($_FILES['HomeBannerVideo']["tmp_name"][$key]['recipeImg1'],$video_path);
                    }

                }

            }

            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the changes were saved.'));
            $this->redirect(array('admin'));
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving the item.'));
        }
    }

    $v_manager = VideoManager::load($model);

    $this->render('update', array(
        'model' => $model,
        'v_manager'=>$v_manager
    ));
}

public function actionDelete($id)
{
    if(isset($id)){
        if($this->loadModel($id,"HomeBannerSection")->delete()){
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the item was deleted.'));
            $this->redirect(array('admin'));
        }
        else{
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, exist a native error to delete the item: '.$id.', to resolve this problem, please contact with the database administrator.'));
        }
    }
    else {
        Yii::app()->user->setFlash('error',Yii::t('admin','Error, the item '.$id.' is not defined.'));
    }
}

public function actionAdmin() {
    $model = new HomeBannerSection('search');
    $model->unsetAttributes();

    if (isset($_GET['HomeBannerSection']))
        $model->setAttributes($_GET['HomeBannerSection']);

    $this->render('admin', array(
        'model' => $model,
    ));
}

public function actionUpdateAttribute($model)
{
    if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
        Yii::import("bootstrap.widgets.TbEditableSaver");
        $editableSaver = new TbEditableSaver($model);
        $editableSaver->update();
        app()->end();
    }
}

}