<?php

class DefaultController extends Controller
{
    public function actions()
    {
        return array(
            'addTopicRow' => 'backend.controllers.actions.AddTopicRowAction',
            'addVideoRow' => 'backend.controllers.actions.AddVideoRowAction',
        );
    }
}