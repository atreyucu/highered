
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'aboutus-page-section3-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_right_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 799x960px')
                                    )); 

//                 echo $form->textFieldGroup($model, 'right_image',
//            				            array(
//                                            'wrapperHtmlOptions' => array(
//                                                'class' => 'col-sm-5',
//                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Right image',
//                                            'append' => 'text',
//                                        )
//                                    ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'vision_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Vision Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<?php echo $form->textAreaGroup($model,'vision_text',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-12',
        ),
        'widgetOptions' => array(
            'htmlOptions' => array('rows' => 4),
        ),
    )
); ?>
<!--                        --><?php //echo $form->textFieldGroup($model, 'vision_text',
//				            array(
//                                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//                                //'hint' =>Yii::t('admin','Please, insert ').' Vision text',
//                                'append' => Yii::t('admin','Text')
//                            )
//                        ); ?>
<!--                    -->
    
                        <?php echo $form->textFieldGroup($model, 'clientele_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Clientele Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<?php echo $form->textAreaGroup($model,'Clientele_text',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-12',
        ),
        'widgetOptions' => array(
            'htmlOptions' => array('rows' => 4),
        ),
    )
); ?>
                    
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'Clientele_text',
//				            array(
//                                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//                                //'hint' =>Yii::t('admin','Please, insert ').' Services text',
//                                'append' => Yii::t('admin','Text')
//                            )
//                        ); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'committed_title', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Committed Title',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!---->
<?php //echo $form->textAreaGroup($model,'committed_text',
//    array(
//        'wrapperHtmlOptions' => array(
//            'class' => 'col-sm-12',
//        ),
//        'widgetOptions' => array(
//            'htmlOptions' => array('rows' => 4),
//        ),
//    )
//); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'committed_text',
//				            array(
//                                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//                                //'hint' =>Yii::t('admin','Please, insert ').' Committed text',
//                                'append' => Yii::t('admin','Text')
//                            )
//                        ); ?>
<!--                    -->
<!--            -->
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





