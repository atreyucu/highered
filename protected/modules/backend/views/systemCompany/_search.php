
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'name', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' name',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_logo_image_alt->getFileUrl('normal'), '', array('width' => '100px')),
                            //'hint' => Yii::t('admin','Please, insert logo_image_alt')
                                    )); 

                 echo $form->textFieldGroup($model, 'logo_image_alt',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' logo_image_alt',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->textFieldGroup($model, 'description',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' description',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->redactorGroup($model, 'mision',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->redactorGroup($model, 'vision',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->textFieldGroup($model, 'phone', array(
                'maxlength' => 125,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' phone',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'fax', array(
                'maxlength' => 125,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' fax',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->urlFieldGroup($model,'url',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert ').' url',
                              'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->emailFieldGroup($model,'email',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert').' email',
                              'append' => '@'
                        )
                    ); ?>
        <?php echo $form->textFieldGroup($model, 'full_adress',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' full_adress',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model,'longitude',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				 //'hint' => Yii::t('admin','Please, insert').' longitude',
				 'append' => '.00'
			)
		); ?>
        <?php echo $form->textFieldGroup($model,'latitude',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				 //'hint' => Yii::t('admin','Please, insert').' latitude',
				 'append' => '.00'
			)
		); ?>
        <?php echo $form->redactorGroup($model, 'customer_support',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->textFieldGroup($model, 'terms_conditions',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' terms_conditions',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->urlFieldGroup($model,'shared_facebook',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert ').' shared_facebook',
                              'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->urlFieldGroup($model,'shared_google',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert ').' shared_google',
                              'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->urlFieldGroup($model,'shared_twitter',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert ').' shared_twitter',
                              'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->urlFieldGroup($model,'shared_youtube',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                             //'hint' => Yii::t('admin','Please, insert ').' shared_youtube',
                              'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->redactorGroup($model, 'msg_newUser',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->redactorGroup($model, 'msg_contactUs',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->redactorGroup($model, 'msg_newsletter',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
