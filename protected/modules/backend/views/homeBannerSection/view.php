
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,Yii::t('sideMenu', 'Banner section')   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"Banner section"))
);

?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Home Banner Section</h5>
    </div>
    <div class="widget-content nopadding">


<?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(

                    array(
                        'class' => 'bootstrap.widgets.TbEditableColumn',
                        'name' => 'banner_type',
                        'sortable' => false,
                        'editable' => array(
                            'type'=>'select',
                            'apply'=>true,
                            'placement' => 'right',
                            'inputclass' => 'span3',
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'source' => array('1'=>Yii::t('admin','Image'),'2'=>Yii::t('admin','Video'),),
                            'success' => "js: function(response, newValue) {
                                if (!response.success)
                                    $('#success').modal('toggle');
                                    setTimeout(function(){
                                        $('#success').modal('toggle');
                                    }, 2000);
                            }",
                        )
                    ),


                    array(
                            'label' => Yii::t('admin','Image'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_banner_image->getFileUrl('normal'), $model->banner_image , array('width'=> '100px')),
                        ),
                    array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'banner_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),

                    array(
                            'label' => Yii::t('admin','Video'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_video->getFileUrl('normal'), $model->video , array('width'=> '100px')),
                        ),
                    array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'video',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),

                    array(
                            'label' => Yii::t('admin','Alternative video image'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_video_image->getFileUrl('normal'), $model->video_image , array('width'=> '100px')),
                        ),
                    array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'video_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'banner_title',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'banner_subtitle',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'banner_started_text',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
    ),

)); ?>
        </div>
    </div>


<div id="video_tab" class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#video_panel_tab1" data-toggle="tab">
                Others video formats
            </a>
        </li>

    </ul>
    <div class="tab-content">

        <div class="tab-pane in active" id="topic_panel_tab1">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>

                    <th width="150">Video</th>
                    <th width="150">Video Format</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_videoView', array('v_manager' => $v_manager));
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>