<?php $upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/homeBannerVideo/';?>
<?php foreach($v_manager->getItems() as $video):?>

    <tr>
        <td width="150"><?php echo 'video_'.$video->id.'_normal'?></td>
        <td width="150">
            <?php if($video->video_format == 1):?>
                video/webm
            <?php elseif($video->video_format == 2):?>
                video/mp4
            <?php else:?>
                video/ogg
            <?php endif?>
        </td>
    </tr>
<?php endforeach;?>