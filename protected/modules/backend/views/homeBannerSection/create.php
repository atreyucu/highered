

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Banner section')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Banner section"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'v_manager'=>$v_manager,
    'buttons' => 'create',
));

