<?php $index = $data->isNewRecord ? ("newvideo" . str_replace('.','',str_replace(' ', '' ,microtime()))) : $data->primaryKey ?>

<tr id="<?php echo $index?>"  class="video <?php echo $data->hasErrors() ? 'error' : '' ?>">

    <td>
        <?php echo $form->fileFieldGroup($data, "[$index]recipeImg1",array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($data->_video->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','Add the video'),
            'widgetOptions' => array(
                'htmlOptions' => array('placeholder' => 'Video'),
            ),
        ));
        ?>
        <?php echo Yii::t('admin','Select the video')?>
    </td>
    <td>
        <?php echo $form->dropDownListGroup($data,"[$index]video_format",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => array(1=>Yii::t('admin','video/webm'),2=>Yii::t('admin','video/mp4'),3=>Yii::t('admin','video/ogg')),
                    // 'htmlOptions' => array('multiple' => true),
                ),
                //'hint' => Yii::t('admin','Please, select').' Banner type',
                'prepend' => Yii::t('admin','Select')
            )
        ); ?>



    </td>
    <td width="100">
        <?php if (substr_count($index, 'newgall') > 0){

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:$(this).parents("tr").remove();'
                ));
        }
        else{
            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => "js:$(this).parents('tr').remove();$.get('/backend/tourGallery/delete?id=$index',function(data){
                    })"
                ));
        }

        ?>
    </td>
</tr>