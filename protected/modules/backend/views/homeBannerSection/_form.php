
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'home-banner-section-form',
    'enableClientValidation'=>true,

));


?>

<div class="widget-box">
<div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
    <h5>Home Banner Section</h5>
</div>
<div class="widget-content nopadding">


<?php echo $form->dropDownListGroup($model,'banner_type',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'data' => array(1=>Yii::t('admin','Image'),2=>Yii::t('admin','Video'),),
            // 'htmlOptions' => array('multiple' => true),
        ),
        //'hint' => Yii::t('admin','Please, select').' Banner type',
        'prepend' => Yii::t('admin','Select')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'banner_title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Banner title',
        'append' => Yii::t('admin','Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'banner_subtitle', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Banner subtitle',
        'append' => Yii::t('admin','Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'banner_started_text', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Started button text',
        'append' => Yii::t('admin','Text')
    )
); ?>
        
                        <div id="img">
    
                            <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'append' => CHtml::image($model->_banner_image->getFileUrl('normal'), '', array('width' => '100px')),
                                'hint' => Yii::t('admin','The image dimensions are 1600x810px')
                                        ));

    //                 echo $form->textFieldGroup($model, 'banner_image',
    //            				            array(
    //                                            'wrapperHtmlOptions' => array(
    //                                                'class' => 'col-sm-5',
    //                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Image',
    //                                            'append' => 'text',
    //                                        )
    //                                    ); ?>
                        </div>


<div id="video">
                    
    

                    
    
                        <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
//                            'append' => CHtml::image($model->_video_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','Select de video')
                                    ));



//                 echo $form->textFieldGroup($model, 'video_image',
//            				            array(
//                                            'wrapperHtmlOptions' => array(
//                                                'class' => 'col-sm-5',
//                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Alternative video image',
//                                            'append' => 'text',
//                                        )
//                                    ); ?>


                        <?php echo $form->dropDownListGroup($model,'video_format',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(1=>Yii::t('admin','video/webm'),2=>Yii::t('admin','video/mp4'),3=>Yii::t('admin','video/ogg')),
                                    // 'htmlOptions' => array('multiple' => true),
                                ),
                                //'hint' => Yii::t('admin','Please, select').' Banner type',
                                'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>



                            <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'append' => CHtml::image($model->_video->getFileUrl('normal'), '', array('width' => '100px')),
                                'hint' => Yii::t('admin','The image dimensions are 1600x810px')
                            ));

//                            echo $form->textFieldGroup($model, 'video',
//                                array(
//                                    'wrapperHtmlOptions' => array(
//                                        'class' => 'col-sm-5',
//                                    ),//'hint' =>Yii::t('admin','Please, insert ').' Video',
//                                    'append' => 'text',
//                                )
//                            ); ?>

                        <?php echo $form->checkBoxGroup($model, 'add_other_video_format'); ?>
    </div>
    </div>
</div>


<div id="video_tab" class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#video_panel_tab1" data-toggle="tab">
                Others video formats
            </a>
        </li>

    </ul>
    <div class="tab-content">

        <div class="tab-pane in active" id="video_panel_tab1">
            <?php $tlabel = CActiveRecord::model($v_manager->class) ?>
            <table id="videoTable" class="offset1 table table-bordered table-stripped" style="width: 1200px;">
                <thead>
                <tr>
                    <th>Video</th>
                    <th>Video Format</th>
                    <th>
                        <?php echo CHtml::ajaxLink(
                            CHtml::tag('i', array('class' => 'glyphicon glyphicon-plus', 'title' => Yii::t('transfer', 'Add new video format')), ''),
                            array('//backend/default/addVideoRow'),
                            array('type' => 'POST', 'success' => 'function(data){ $("#videoTable tbody").append(data); $("#metaTable .error").tooltip(); }'));
                        ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $form->type = 'inline';
                foreach ($v_manager->getItems() as $id => $data) {
                    $this->renderPartial('_videoForm', array('form' => $form, 'id' => $id, 'data' => $data));
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        if($('#HomeBannerSection_banner_type option:selected').attr('value') == '1'){
            $('#img').show();
            $('#video').hide();
        }
        else{
            $('#img').hide();
            $('#video').show();
        }
        $('#HomeBannerSection_banner_type').change(function(){
            if($('#HomeBannerSection_banner_type option:selected').attr('value') == '1'){
                $('#img').show();
                $('#video').hide();
            }
            else{
                $('#img').hide();
                $('#video').show();
            }
        });

        if($('#HomeBannerSection_add_other_video_format').attr('checked')){
            $('#video_tab').show();
        }
        else{
            $('#video_tab').hide();
        }

        $('#HomeBannerSection_add_other_video_format').change(function(){
            if($('#HomeBannerSection_add_other_video_format').attr('checked')){
                $('#video_tab').show();
            }
            else{
                $('#video_tab').hide();
            }
        });




    });
</script>





