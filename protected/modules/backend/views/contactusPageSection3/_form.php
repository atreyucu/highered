
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'contactus-page-section3-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'use_map'); ?>


                      <div id="cords">
                    
    
                        <?php echo $form->textFieldGroup($model, 'longitude', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Longitude',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'latitude', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Latitude',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                          </div>

                      <div id="img">
                    
    
                        <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_map_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 1600x440px')
                                    )); 

//                 echo $form->textFieldGroup($model, 'map_image',
//            				            array(
//                                            'wrapperHtmlOptions' => array(
//                                                'class' => 'col-sm-5',
//                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Map image',
//                                            'append' => 'text',
//                                        )
//                                    ); ?>
                    
            </div>
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>


<script type="text/javascript">
    if($('#ContactusPageSection3_use_map').attr('checked')){
        $('#cords').show();
        $('#img').hide();
    }
    else{
        $('#cords').hide();
        $('#img').show();
    }

    $('#ContactusPageSection3_use_map').change(function(){
        if($('#ContactusPageSection3_use_map').attr('checked')){
            $('#cords').show();
            $('#img').hide();
        }
        else{
            $('#cords').hide();
            $('#img').show();
        }
    });
</script>





