

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Section6')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Section6"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

