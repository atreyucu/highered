
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'error-page-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_background_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 1600x810px')
                                    )); 

//                 echo $form->textFieldGroup($model, 'background_image',
//            				            array(
//                                            'wrapperHtmlOptions' => array(
//                                                'class' => 'col-sm-5',
//                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Background image',
//                                            'append' => 'text',
//                                        )
//                                    ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'message', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Message text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'button_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Button text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





