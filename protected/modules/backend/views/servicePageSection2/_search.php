
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_background_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 1600x680px')
                                    )); 

                 echo $form->textFieldGroup($model, 'background_image',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Background image',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->textFieldGroup($model, 'title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'description',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Description',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'nights_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Weeknights title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'nights_hours', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Weeknights time text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'weekend_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Weekends title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'weekend_hours', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Weekends time text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'contact_button_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Contact button text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'created',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' created',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'updated',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' updated',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'owner', array(
                'maxlength' => 100,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' owner',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
