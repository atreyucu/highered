

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Social Media')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Social Media"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

