

<?php


$this->breadcrumbs = array(
    $model->adminNames[3] , Yii::t('sideMenu', 'Topic')  => array('admin'),
    Yii::t('admin',
        'Edit ',
        array('{name}'=>$model->adminNames[1])
    ),
);

$this->title = Yii::t('admin',
    'Edit {name}',
    array('{name}'=> Yii::t('sideMenu',"Topic"))
);

$this->renderPartial('_form', array(
		'model' => $model));

