<?php $index = $data->isNewRecord ? ("newtopic" . str_replace('.','',str_replace(' ', '' ,microtime()))) : $data->primaryKey ?>

<tr class="topic <?php echo $data->hasErrors() ? 'error' : '' ?>">
    <td style="width: 270px;">

        <?php echo $form->textFieldGroup($data, "[$index]title",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Banner mobile image alt',
                'append' => 'text',
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Title'),
                ),
            )
        ); ?>


    </td>
    <td style="width: 270px;">
        <?php echo $form->textFieldGroup($data, "[$index]time",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Banner mobile image alt',
                'append' => 'text',
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Time planned'),
                ),
            )
        ); ?>

    </td>
    <td>
        <?php echo $form->redactorGroup($data, "[$index]description",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-8',
                ),
                'widgetOptions' => array(
                    'editorOptions' => array(
                        'rows' => 10,
                        'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                        'minHeight' => 30
                    )
                ),
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Topic Description'),
                ),
            )
        ); ?>

        <?php echo $form->hiddenField($data, "[$index]orderr",
            array(
            )

        ); ?>
    </td>
    <td style="width: 65px;">
        <?php if (substr_count($index, 'newtopic') > 0){

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_topic_contents($(this).parents("tr").prev(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_topic_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:$(this).parents("tr").remove();$("#hid_top_numb").val(parseInt($("#hid_top_numb").val() - 1))'
                ));
        }
        else{
            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_topic_contents($(this).parents("tr").prev(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_topic_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => "js:$('#hid_topic_numb').val(parseInt($('#hid_topic_numb').val() - 1));$(this).parents('tr').remove();$.get('/backend/topic/delete?id=$index',function(data){
                    })"
                ));

        }

        ?>
    </td>
</tr>
