

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Plan')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Plan"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    't_manager'=>$t_manager,
    'buttons' => 'create'));

