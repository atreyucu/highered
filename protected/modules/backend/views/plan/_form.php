
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'plan-form',
    'enableClientValidation'=>true,

));
?>


<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Plan General Information</h5>
    </div>
    <div class="widget-content nopadding">
        
            
    
                        <?php echo $form->textFieldGroup($model, 'title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

 </div>
    </div>




<div class="tabbable inline">
<ul id="myTab" class="nav nav-tabs tab-bricky">
    <li class="active">
        <a href="#topic_panel_tab1" data-toggle="tab">
            Topics
        </a>
    </li>
    <li>

<!--    <li>-->
<!--        <a href="#topic_panel_tab2" data-toggle="tab">-->
<!--            Seo-->
<!--        </a>-->
<!--    </li>-->
</ul>
<div class="tab-content">

<div class="tab-pane in active" id="topic_panel_tab1">
    <?php $tlabel = CActiveRecord::model($t_manager->class) ?>
    <table id="topicTable" class="offset1 table table-bordered table-stripped" style="width: 1070px;">
        <thead>
        <tr>
            <th><?php echo $tlabel->getAttributeLabel('title') ?></th>
            <th><?php echo $tlabel->getAttributeLabel('time') ?></th>
            <th><?php echo $tlabel->getAttributeLabel('description') ?></th>
            <th>
                <?php echo CHtml::ajaxLink(
                    CHtml::tag('i', array('class' => 'glyphicon glyphicon-plus', 'title' => Yii::t('transfer', 'Add new topic')), ''),
                    array('//backend/default/addTopicRow'),
                    array('type' => 'POST', 'data'=> array('top_numb'=>new CJavaScriptExpression('js:$("#hid_top_numb").val()')) ,'success' => 'function(data){ $("#topicTable tbody").append(data); add_topic(); $("#metaTable .error").tooltip(); $("#hid_top_numb").val(parseInt($("#hid_top_numb").val()) + 1); }'));
                ?>
            </th>
        </tr>
        </thead>
        <tbody>

        <input type="hidden" value="<?php echo count($t_manager->getItems()) + 1?>" id="hid_top_numb">
        <?php
        $form->type = 'inline';
        foreach ($t_manager->getItems() as $id => $data) {
            $this->renderPartial('_topicForm', array('form' => $form, 'id' => $id, 'data' => $data));
        }
        ?>
        </tbody>
    </table>
    <!-- </div>
 </div>-->
</div>


<!--<div class="tab-pane" id="topic_panel_tab2">-->
<!--    --><?php //$model->seoFormWidget($form); ?>
<!--</div>-->

</div>
</div>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





