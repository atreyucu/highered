<?php

class BackendModule extends CWebModule
{
    public function getName()
    {
        return 'Backend';
    }

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->attachBehavior('ycm', array(
            'class' => 'application.components.ModuleBehavior'
        ));

        // import the module-level models and components
        $this->setImport(array(
            'backend.models.*',
            'backend.components.*',
        ));
    }


    public function getAdminMenus()
    {
        parent::getAdminMenus();

        $menu = array(
            array(
                'url' => '#',
                'icon' => 'tree-conifer',
                'label' => Yii::t('sideMenu', 'General'),
                'visible' => user()->isAdmin,
                'items' => array(
                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Menu'),
                        'url' => array('/backend/mainMenu/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'mainMenu',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Footer'),
                        'url' => array('/backend/footer/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'footer',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Social media'),
                        'url' => array('/backend/socialLinks/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'socialLinks',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Company Info'),
                        'url' => array('/backend/companyInfo/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'companyInfo',
                        'visible' => user()->isAdmin,
                    ),

                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Error Page'),
                        'url' => array('/backend/errorPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'errorPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'info-sign',
                        'label' => Yii::t('sideMenu', 'Thank you'),
                        'url' => array('/backend/thankPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'thankPage',
                        'visible' => user()->isAdmin,
                    ),

                ),
            ),
            array(
               'url' => '#',
               'icon' => 'tree-conifer',
               'label' => Yii::t('sideMenu', 'Home Page'),
               'visible' => user()->isAdmin,
               'items' => array(
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Banner Section'),
                       'url' => array('/backend/homeBannerSection/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homeBannerSection',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section2'),
                       'url' => array('/backend/homePageSection2/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePageSection2',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section3'),
                       'url' => array('/backend/homePageSection3/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePageSection3',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section4'),
                       'url' => array('/backend/homePageSection4/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePageSection4',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section5'),
                       'url' => array('/backend/homePageSection5/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePageSection5',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section6'),
                       'url' => array('/backend/homePageSection6/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePageSection6',
                       'visible' => user()->isAdmin,
                   ),
               )
            ),
            array(
               'url' => '#',
               'icon' => 'tree-conifer',
               'label' => Yii::t('sideMenu', 'About Us'),
               'visible' => user()->isAdmin,
               'items' => array(
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Banner Section'),
                       'url' => array('/backend/aboutusBannerSection/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'aboutusBannerSection',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section2'),
                       'url' => array('/backend/aboutusPageSection2/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'aboutusPageSection2',
                       'visible' => user()->isAdmin,
                   ),
                   array(
                       'icon' => 'info-sign',
                       'label' => Yii::t('sideMenu', 'Section3'),
                       'url' => array('/backend/aboutusPageSection3/admin'),
                       'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'aboutusPageSection3',
                       'visible' => user()->isAdmin,
                   ),
               )
            ),
            array(
                   'url' => '#',
                   'icon' => 'tree-conifer',
                   'label' => Yii::t('sideMenu', 'Contact Us'),
                   'visible' => user()->isAdmin,
                    'items' => array(
                        array(
                            'icon' => 'info-sign',
                            'label' => Yii::t('sideMenu', 'Banner Section'),
                            'url' => array('/backend/contactusBannerSection/admin'),
                            'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'contactusBannerSection',
                            'visible' => user()->isAdmin,
                        ),
                        array(
                            'icon' => 'info-sign',
                            'label' => Yii::t('sideMenu', 'Section2'),
                            'url' => array('/backend/contactusPageSection2/admin'),
                            'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'contactusPageSection2',
                            'visible' => user()->isAdmin,
                        ),
                        array(
                            'icon' => 'info-sign',
                            'label' => Yii::t('sideMenu', 'Section3'),
                            'url' => array('/backend/contactusPageSection3/admin'),
                            'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'contactusPageSection3',
                            'visible' => user()->isAdmin,
                        ),
                    )
                ),
            array(
                   'url' => '#',
                   'icon' => 'tree-conifer',
                   'label' => Yii::t('sideMenu', 'Services'),
                   'visible' => user()->isAdmin,
                   'items' => array(

                           array(
                               'icon' => 'info-sign',
                               'label' => Yii::t('sideMenu', 'Banner Section'),
                               'url' => array('/backend/serviceBannerSection/admin'),
                               'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'serviceBannerSection',
                               'visible' => user()->isAdmin,
                           ),
                           array(
                               'icon' => 'info-sign',
                               'label' => Yii::t('sideMenu', 'Section2'),
                               'url' => array('/backend/servicePageSection2/admin'),
                               'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'servicePageSection2',
                               'visible' => user()->isAdmin,
                           ),
                           array(
                               'icon' => 'info-sign',
                               'label' => Yii::t('sideMenu', 'Plans'),
                               'url' => array('/backend/plan/admin'),
                               'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'plan',
                               'visible' => user()->isAdmin,
                           ),
                   )
                ),

        );
        return $menu;
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }
}
