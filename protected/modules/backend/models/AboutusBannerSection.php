
<?php

Yii::import('backend.models._base.BaseAboutusBannerSection');

class AboutusBannerSection extends BaseAboutusBannerSection
{
    /**
    * @param string $className
    * @return AboutusBannerSection    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('AboutusBannerSections','aboutusbannersection','aboutusbannersections','About us'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('banner_image', 'textField'),
                    array('title', 'textField'),
                    array('description', 'wysiwyg'),
                    array('biography_image', 'textField'),
                    array('biography_title', 'textField'),
                    array('biography_subtitle', 'textField'),
                    array('biography_description', 'wysiwyg'),
                    array('contact_me_btn_text', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'banner_image',
				'title',
				'description',
				'biography_image',
				'biography_title',
				'biography_subtitle',
				/*
				'biography_description',
				'contact_me_btn_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'banner_image',
				'title',
				'description',
				'biography_image',
				'biography_title',
				'biography_subtitle',
				/*
				'biography_description',
				'contact_me_btn_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}