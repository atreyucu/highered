
<?php

Yii::import('backend.models._base.BaseServicePageSection2');

class ServicePageSection2 extends BaseServicePageSection2
{
    /**
    * @param string $className
    * @return ServicePageSection2    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('ServicePageSection2s','servicepagesection2','servicepagesection2s','Service page'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('background_image', 'textField'),
                    array('title', 'textField'),
                    array('description', 'textField'),
                    array('nights_title', 'textField'),
                    array('nights_hours', 'textField'),
                    array('weekend_title', 'textField'),
                    array('weekend_hours', 'textField'),
                    array('contact_button_text', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'background_image',
				'title',
				'description',
				'nights_title',
				'nights_hours',
				'weekend_title',
				/*
				'weekend_hours',
				'contact_button_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'background_image',
				'title',
				'description',
				'nights_title',
				'nights_hours',
				'weekend_title',
				/*
				'weekend_hours',
				'contact_button_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}