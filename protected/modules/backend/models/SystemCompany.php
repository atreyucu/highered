
<?php

Yii::import('backend.models._base.BaseSystemCompany');

class SystemCompany extends BaseSystemCompany
{
    /**
    * @param string $className
    * @return SystemCompany    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
        $this->id = rand_uniqid();
        $this->adminNames[3] = Yii::t('sideMenu', 'SystemCompany');
    }




    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('SystemCompanies','systemcompany','systemcompanies','SystemCompany'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('name', 'textField'),
                    array('logo_image_alt', 'textField'),
                    array('description', 'textField'),
                    array('mision', 'wysiwyg'),
                    array('vision', 'wysiwyg'),
                    array('phone', 'textField'),
                    array('fax', 'textField'),
                    array('url', 'textField'),
                    array('email', 'textField'),
                    array('full_adress', 'textField'),
                    array('longitude', 'currency'),
                    array('latitude', 'currency'),
                    array('customer_support', 'wysiwyg'),
                    array('terms_conditions', 'textField'),
                    array('shared_facebook', 'textField'),
                    array('shared_google', 'textField'),
                    array('shared_twitter', 'textField'),
                    array('shared_youtube', 'textField'),
                    array('msg_newUser', 'wysiwyg'),
                    array('msg_contactUs', 'wysiwyg'),
                    array('msg_newsletter', 'wysiwyg'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'name',
				'logo_image_alt',
				'description',
				'mision',
				'vision',
				'phone',
				/*
				'fax',
				'url',
				'email',
				'full_adress',
				'longitude',
				'latitude',
				'customer_support',
				'terms_conditions',
				'shared_facebook',
				'shared_google',
				'shared_twitter',
				'shared_youtube',
				'msg_newUser',
				'msg_contactUs',
				'msg_newsletter',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'name',
				'logo_image_alt',
				'description',
				'mision',
				'vision',
				'phone',
				/*
				'fax',
				'url',
				'email',
				'full_adress',
				'longitude',
				'latitude',
				'customer_support',
				'terms_conditions',
				'shared_facebook',
				'shared_google',
				'shared_twitter',
				'shared_youtube',
				'msg_newUser',
				'msg_contactUs',
				'msg_newsletter',
			*/
            ),
        );
}



}