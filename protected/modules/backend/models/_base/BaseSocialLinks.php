<?php

/**
 * This is the model base class for the table "social_links".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SocialLinks".
 * This code was improve iReevo Team
 * Columns in table "social_links" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $facebook
 * @property string $youtube
 * @property string $linkedin
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property ImageARBehavior $imageAR

 */
abstract class BaseSocialLinks extends I18NInTableAdapter {

/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'social_links';
	}

	public static function label($n = 1) {
		return self::model()->t_model('SocialLinks|SocialLinks', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id', 'length', 'max'=>50),
			array('facebook, youtube, linkedin', 'length', 'max'=>255),
			array('owner', 'length', 'max'=>100),
			array('created, updated', 'safe'),
			array('facebook, youtube, linkedin, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, facebook, youtube, linkedin, created, updated, owner', 'safe', 'on'=>'search'),

		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('SocialLinks','ID'),
			'facebook' => Yii::t('SocialLinks','Facebook url'),
			'youtube' => Yii::t('SocialLinks','Youtube url'),
			'linkedin' => Yii::t('SocialLinks','Linkedin url'),
			'created' => Yii::t('SocialLinks','Created'),
			'updated' => Yii::t('SocialLinks','Updated'),
			'owner' => Yii::t('SocialLinks','Owner'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('facebook', $this->facebook, true);
		$criteria->compare('youtube', $this->youtube, true);
		$criteria->compare('linkedin', $this->linkedin, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}