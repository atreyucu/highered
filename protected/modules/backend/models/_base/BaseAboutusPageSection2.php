<?php

/**
 * This is the model base class for the table "aboutus_page_section2".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AboutusPageSection2".
 * This code was improve iReevo Team
 * Columns in table "aboutus_page_section2" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $left_image
 * @property string $mission_title
 * @property string $mission_text
 * @property string $services_title
 * @property string $services_text
 * @property string $committed_title
 * @property string $committed_text
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property ImageARBehavior $imageAR

 */
abstract class BaseAboutusPageSection2 extends I18NInTableAdapter {

/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    public $recipeImg1;
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                '_left_image' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => 'recipeImg1', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img1_',
                    'relativeWebRootFolder' => '/images/AboutusPageSection2',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                                        'process' => array('resize' => array(800,960, 1)),
                                        ),
                    // and override the default :
                    ),
                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'aboutus_page_section2';
	}

	public static function label($n = 1) {
		return self::model()->t_model('AboutusPageSection2|AboutusPageSection2s', $n);
	}

	public static function representingColumn() {
		return 'left_image';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id', 'length', 'max'=>50),
			array('left_image, mission_title, services_title, committed_title', 'length', 'max'=>255),
			array('owner', 'length', 'max'=>100),
			array('mission_text, services_text, committed_text, created, updated', 'safe'),
			array('mission_title, mission_text, services_title, services_text, committed_title, committed_text, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
            array('recipeImg1', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'safe'),
			array('id, left_image, mission_title, mission_text, services_title, services_text, committed_title, committed_text, created, updated, owner', 'safe', 'on'=>'search'),

		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('AboutusPageSection2','ID'),
			'left_image' => Yii::t('AboutusPageSection2','Left image Alt'),
			'mission_title' => Yii::t('AboutusPageSection2','Mission Title'),
			'mission_text' => Yii::t('AboutusPageSection2','Mission text'),
			'services_title' => Yii::t('AboutusPageSection2','Services Title'),
			'services_text' => Yii::t('AboutusPageSection2','Services text'),
			'committed_title' => Yii::t('AboutusPageSection2','Committed Title'),
			'committed_text' => Yii::t('AboutusPageSection2','Committed text'),
			'created' => Yii::t('AboutusPageSection2','Created'),
			'updated' => Yii::t('AboutusPageSection2','Updated'),
			'owner' => Yii::t('AboutusPageSection2','Owner'),
    'recipeImg1' => Yii::t('AboutusPageSection2','Left image'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('left_image', $this->left_image, true);
		$criteria->compare('mission_title', $this->mission_title, true);
		$criteria->compare('mission_text', $this->mission_text, true);
		$criteria->compare('services_title', $this->services_title, true);
		$criteria->compare('services_text', $this->services_text, true);
		$criteria->compare('committed_title', $this->committed_title, true);
		$criteria->compare('committed_text', $this->committed_text, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}