<?php

/**
 * This is the model base class for the table "company_info".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CompanyInfo".
 * This code was improve iReevo Team
 * Columns in table "company_info" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $main_logo
 * @property string $secondary_logo
 * @property string $name
 * @property string $email
 * @property string $phone1
 * @property string $phone2
 * @property string $address
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property ImageARBehavior $imageAR

 */
abstract class BaseCompanyInfo extends I18NInTableAdapter {

/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    public $recipeImg1;
    public $recipeImg2;
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                '_main_logo' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => 'recipeImg1', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img1_',
                    'relativeWebRootFolder' => '/images/CompanyInfo',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                                        'process' => array('resize' => array(260,70, 1)),
                                        ),
                    // and override the default :
                    ),
                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                                '_secondary_logo' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => 'recipeImg2', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img2_',
                    'relativeWebRootFolder' => '/images/CompanyInfo',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                                        'process' => array('resize' => array(219,59, 1)),
                                        ),
                    // and override the default :
                    ),
                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'company_info';
	}

	public static function label($n = 1) {
		return self::model()->t_model('CompanyInfo|CompanyInfos', $n);
	}

	public static function representingColumn() {
		return 'main_logo';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id', 'length', 'max'=>50),
			array('main_logo, secondary_logo, name, email, phone1, phone2, address', 'length', 'max'=>255),
			array('owner', 'length', 'max'=>100),
			array('created, updated', 'safe'),
			array('email', 'email','message'=>Yii::t('admin',"The email isn't correct")),
			array('email', 'unique','message'=>Yii::t('admin',"Email already exists!")),
			array('address, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
            array('recipeImg1', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'safe'),
            array('recipeImg2', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg2', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg2', 'safe'),
			array('id, main_logo, secondary_logo, name, email, phone1, phone2, address, created, updated, owner', 'safe', 'on'=>'search'),

		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('CompanyInfo','ID'),
			'main_logo' => Yii::t('CompanyInfo','Main logo Alt'),
			'secondary_logo' => Yii::t('CompanyInfo','Secondary logo Alt'),
			'name' => Yii::t('CompanyInfo','name'),
			'email' => Yii::t('CompanyInfo','email'),
			'phone1' => Yii::t('CompanyInfo','phone1'),
			'phone2' => Yii::t('CompanyInfo','phone2'),
			'address' => Yii::t('CompanyInfo','Company Address'),
			'created' => Yii::t('CompanyInfo','Created'),
			'updated' => Yii::t('CompanyInfo','Updated'),
			'owner' => Yii::t('CompanyInfo','Owner'),
    'recipeImg1' => Yii::t('CompanyInfo','Main logo'),
    'recipeImg2' => Yii::t('CompanyInfo','Secondary logo'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('main_logo', $this->main_logo, true);
		$criteria->compare('secondary_logo', $this->secondary_logo, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone1', $this->phone1, true);
		$criteria->compare('phone2', $this->phone2, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}