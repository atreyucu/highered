
<?php

Yii::import('backend.models._base.BaseHomePageSection6');

class HomePageSection6 extends BaseHomePageSection6
{
    /**
    * @param string $className
    * @return HomePageSection6    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('HomePageSection6s','homepagesection6','homepagesection6s','Home Page'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('title', 'textField'),
                    array('description', 'textField'),
                    array('left_image', 'textField'),
                    array('left_title', 'textField'),
                    array('left_btn_text', 'textField'),
                    array('middle_image', 'textField'),
                    array('middle_title', 'textField'),
                    array('middle_btn_text', 'textField'),
                    array('right_image', 'textField'),
                    array('right_title', 'textField'),
                    array('right_btn_text', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'title',
				'description',
				'left_image',
				'left_title',
				'left_btn_text',
				'middle_image',
				/*
				'middle_title',
				'middle_btn_text',
				'right_image',
				'right_title',
				'right_btn_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'title',
				'description',
				'left_image',
				'left_title',
				'left_btn_text',
				'middle_image',
				/*
				'middle_title',
				'middle_btn_text',
				'right_image',
				'right_title',
				'right_btn_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}