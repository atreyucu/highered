<?php

Yii::import('ext.components.TabularInputManagerEx');

class VideoManager extends TabularInputManagerEx
{
    public $class = 'HomeBannerVideo';

    public $model;
    public $dependantAttr = 'home_banner';

    /**
     * @static
     * @param $model
     * @return TourDateManager
     */
    public static function load($model)
    {
        $instance = new self();
        $instance->model = $model;
        $instance->fetchItemsOrdered('created');
        return $instance;
    }
}