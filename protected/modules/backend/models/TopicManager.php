<?php

Yii::import('ext.components.TabularInputManagerEx');

class TopicManager extends TabularInputManagerEx
{
    public $class = 'Topic';

    public $model;
    public $dependantAttr = 'plan';

    /**
     * @static
     * @param $model
     * @return TourDateManager
     */
    public static function load($model)
    {
        $instance = new self();
        $instance->model = $model;
        $instance->fetchItemsOrdered('orderr');
        return $instance;
    }
}