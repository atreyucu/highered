<?php

/**
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $apply_over_body
 * @property integer $seo_model_id
 *
 * @property SeoModel $seoModel
 * @property ImageARBehavior $imageAR
 */
abstract class BaseSeoModelTrackingCode extends I18NInTableAdapter
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('SeoModelTrackingCode', 'SeoModelTrackingCode|SeoModelTrackingCodes', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function tableName()
    {
        return 'seo_model_tracking_code';
    }

    public function i18nAttributes()
    {
        return array();
    }

    public function scopes(){
        return array(
            'head_tc'=>array(
                'condition'=>"apply_over_body=0"
            ),
            'body_tc'=>array(
                'condition'=>"apply_over_body=1"
            )
        );
    }

    public function rules()
    {
        return array(
            array('name, code, seo_model_id', 'required'),
            array('seo_model_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('id, name, code, seo_model_id, apply_over_body', 'safe', 'on' => 'search,update,insert'),
            array('SeoModel', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'seoModel' => array(self::BELONGS_TO, 'SeoModel', 'seo_model_id'),
        );
    }

    public function pivotModels()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('SeoModelTrackingCode', 'id'),
            'name' => Yii::t('SeoModelTrackingCode', 'name'),
            'code' => Yii::t('SeoModelTrackingCode', 'code'),
            'apply_over_body' => Yii::t('SeoModelTrackingCode', 'apply over body section'),
            'seo_model_id' => Yii::t('SeoModelTrackingCode', 'seo_model_id'),

        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('seo_model_id', $this->seo_model_id);
        $criteria->compare('apply_over_body', $this->apply_over_body);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}