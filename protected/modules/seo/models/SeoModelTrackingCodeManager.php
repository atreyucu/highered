<?php

Yii::import('ext.components.TabularInputManagerEx');

class SeoModelTrackingCodeManager extends TabularInputManagerEx
{
    public $class = 'SeoModelTrackingCode';

    public $model;
    public $dependantAttr = 'seo_model_id';

    /**
     * @static
     * @param $model
     * @return SeoModelTrackingCodeManager
     */
    public static function load($model)
    {
        $instance = new self();
        $instance->model = $model;
        $instance->fetchItems();
        return $instance;
    }
}