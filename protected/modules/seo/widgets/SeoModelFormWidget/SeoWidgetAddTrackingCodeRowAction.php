<?php

class SeoWidgetAddTrackingCodeRowAction extends CAction
{
    public function run()
    {
        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = new TbActiveForm();
        $form->showErrors = false;
        $form->type = 'inline';
        $data = new SeoModelTrackingCode();
        return $this->controller->renderPartial('seo.widgets.SeoModelFormWidget.views._trackingCodeRow', array('form' => $form, 'data' => $data));
    }
} 