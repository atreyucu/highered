<?php

class SeoPageWidget extends CWidget
{
    public function run()
    {
        $lang = Yii::app()->language;
        $pathInfo = Yii::app()->request->pathInfo;

        if (preg_match('/((?P<lang>\w{2})\/)?(?P<pathInfo>.*)/', $pathInfo, $matches)) {
            $pathInfo = $matches['pathInfo'];
            if (isset($matches['lang']) && $matches['lang'] && in_array($matches['lang'], array_keys(Language::choices()))) {
                $lang = $matches['lang'];
            }
        }
        /** @var SeoModel $seo */
        if($pathInfo == '')
            $pathInfo = '/';
        $seo = SeoModel::model()->findByAttributes(array(I18NInTableAdapter::_attr('url', $lang) => $pathInfo));

        if (!$seo) {
            $model = SeoUrlRule::model()->findByAttributes(array('route' => $pathInfo));
            if ($model) {
                $seo = $model->getSeoModel(false);
            }
        }

        /** @var SeoModelBehavior $model */
        if ($seo) {
            $instance = $seo->getRelatedInstance();
            $replacementMap = $seo->getInstanceReplacementAttributes($instance);

            if ($seo->title) {
                echo "<title>" . strtr($seo->title, $replacementMap) . "</title>";
            }

            if ($seo->description) {
                cs()->registerMetaTag(strtr($seo->description, $replacementMap), 'description');
            }

            if ($seo->keywords) {
                cs()->registerMetaTag(strtr($seo->keywords, $replacementMap), 'keywords');
            }


            if($seo->include_fb){
                if(isset($_SESSION['language']) && $_SESSION['language']){
                    if($_SESSION['language']=='es'){
                        cs()->registerMetaTag('es_Es','og:locale');
                    }
                    else{
                        cs()->registerMetaTag('en_US','og:locale');
                    }
                }
                else{
                    cs()->registerMetaTag('es_Es','og:locale');
                }
                cs()->registerMetaTag('article','og:type');
                cs()->registerMetaTag(strtr($seo->title, $replacementMap),'og:title');
                cs()->registerMetaTag(strtr($seo->description, $replacementMap),'og:description');
                cs()->registerMetaTag(Yii::app()->request->hostInfo . '/'. Yii::app()->request->pathInfo,'og:url');
                cs()->registerMetaTag(Yii::app()->name,'og:site_name');
                cs()->registerMetaTag($seo->article_publisher,'article:publisher');
                cs()->registerMetaTag($seo->article_section,'article:section');
                cs()->registerMetaTag(date('Y-m-d Y:s:i'),'article:published_time');
                cs()->registerMetaTag(date('Y-m-d Y:s:i'),'article:modified_time');
                cs()->registerMetaTag(date('Y-m-d Y:s:i'),'og:updated_time');

                if($seo->_fb_image->getFileUrl('normal') != ''){
                    cs()->registerMetaTag(Yii::app()->request->hostInfo . $seo->_fb_image->getFileUrl('normal'),'og:image');
                }
                else{
                    cs()->registerMetaTag('','og:image');
                }
            }

            if($seo->include_tw){
                if($seo->twitter_card){
                    cs()->registerMetaTag($seo->twitter_card,'twitter:card');
                }

                cs()->registerMetaTag(Yii::app()->name,'twitter:site');
                cs()->registerMetaTag(strtr($seo->title, $replacementMap),'twitter:title');
                cs()->registerMetaTag(strtr($seo->description, $replacementMap),'twitter:description');
                if($seo->_tw_image->getFileUrl('normal') != ''){
                    cs()->registerMetaTag(Yii::app()->request->hostInfo . $seo->_tw_image->getFileUrl('normal'),'twitter:image:src');
                }
                else{
                    cs()->registerMetaTag('','twitter:image:src');
                }

            }

            /** @var SeoModelMeta $meta */
            foreach ($seo->meta as $meta) {
                cs()->registerMetaTag(strtr($meta->content, $replacementMap), $meta->name);
            }


            foreach ($seo->head_tc as $tcode) {
               cs()->registerScript($tcode->name,$tcode->code);
            }


        }
    }
} 