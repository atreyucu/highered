<?php

class m150320_165529_tracking_code_2015_03_20 extends CDbMigration
{
    /*
	public function up()
	{
	}

	public function down()
	{
		echo "m150320_165529_tracking_code_2015_03_20 does not support migration down.\n";
		return false;
	}
    */


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('seo_model_tracking_code', array(
            'id' => 'int(11) NOT NULL AUTO_INCREMENT',
            'name' => 'varchar(255) NOT NULL',
            'code' => 'text NOT NULL',
            'apply_over_body' => 'TINYINT( 1 ) NULL DEFAULT 0',
            'seo_model_id' => 'int(11) NOT NULL',
            'PRIMARY KEY (`id`)',
        ), 'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci');
        $this->addForeignKey('seo_model_id_fk2', 'seo_model_tracking_code', 'seo_model_id', 'seo_model', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('seo_model_id_idx2', 'seo_model_tracking_code', 'seo_model_id');
	}

	public function safeDown()
	{
        $this->dropForeignKey('seo_model_id_fk2', 'seo_model_tracking_code');
        $this->dropTable('seo_model_tracking_code');
	}

}