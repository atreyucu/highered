<?php

class m150413_145928_social_networks_images_2015_04_13 extends CDbMigration
{
	/*public function up()
	{
	}

	public function down()
	{
		echo "m150413_145928_social_networks_images_2015_04_13 does not support migration down.\n";
		return false;
	}*/

    public function safeUp()
    {
        $this->execute("ALTER TABLE `seo_model` ADD `article_publisher` varchar( 255 ) NULL DEFAULT '' AFTER `include_tw`");
        $this->execute("ALTER TABLE `seo_model` ADD `article_section` varchar( 255 ) NULL DEFAULT '' AFTER `article_publisher`");
    }

    public function safeDown()
    {
        $this->dropColumn('seo_model','article_publisher');
        $this->dropColumn('seo_model','article_section');
    }
}