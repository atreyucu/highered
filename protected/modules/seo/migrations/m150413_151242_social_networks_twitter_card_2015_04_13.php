<?php

class m150413_151242_social_networks_twitter_card_2015_04_13 extends CDbMigration
{
    /*public function up()
    {
    }

    public function down()
    {
        echo "m150413_145928_social_networks_images_2015_04_13 does not support migration down.\n";
        return false;
    }*/

    public function safeUp()
    {
        $this->execute("ALTER TABLE `seo_model` ADD `twitter_card` varchar( 255 ) NULL DEFAULT '' AFTER `article_section`");
    }

    public function safeDown()
    {
        $this->dropColumn('seo_model','twitter_card');
    }
}