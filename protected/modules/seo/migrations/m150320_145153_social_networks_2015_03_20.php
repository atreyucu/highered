<?php

class m150320_145153_social_networks_2015_03_20 extends CDbMigration
{
    /*
	public function up()
	{
	}

	public function down()
	{
		echo "m150320_145153_social_networks_2015_03_20 does not support migration down.\n";
		return false;
	}*/


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->execute("ALTER TABLE `seo_model` ADD `include_fb` TINYINT( 1 ) NULL DEFAULT 0 AFTER `keywords`");
        $this->execute("ALTER TABLE `seo_model` ADD `include_tw` TINYINT( 1 ) NULL DEFAULT 0 AFTER `include_fb`");
	}

	public function safeDown()
	{
        $this->dropColumn('seo_model','include_fb');
        $this->dropColumn('seo_model','include_tw');
	}

}