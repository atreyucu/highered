<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Restore");
$this->breadcrumbs = array(
    UserModule::t("Login") => array('/user/login'),
    UserModule::t("Restore"),
);
$this->title = UserModule::t("Restore");
/** @var $form TbActiveForm */
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'recovery-form',
));

echo $form->textFieldGroup($model, 'login_or_email', array(
    'hint' => UserModule::t("Please enter your login or email addres."),
));

echo TbHtml::formActions(array(
    TbHtml::submitButton(UserModule::t("Restore"))
));
$this->endWidget();
?>
