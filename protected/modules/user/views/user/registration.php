<?php
/** @var $this Controller */
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Registration");
$this->breadcrumbs = array(
    UserModule::t("Registration"),
);
$this->title = UserModule::t("Registration");
/** @var $form TbActiveForm */
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'registration-form',
));

echo $form->textFieldGroup($model, 'username');
echo $form->textFieldGroup($model, 'email');
echo $form->passwordFieldRow($model, 'password', array(
    'hint' => UserModule::t("Minimal password length 4 symbols.")
));
echo $form->passwordFieldRow($model, 'verifyPassword');


$this->profileEditFields($profile);
if (UserModule::doCaptcha('registration'))
    echo $form->captchaRow($model, 'verifyCode', array(
        'hint' => UserModule::t("Please enter the letters as they are shown in the image above.")
            . '<br/>'
            . UserModule::t("Letters are not case-sensitive."),
    ));

echo $this->renderFormFooter();
echo TbHtml::formActions(array(
    TbHtml::submitButton(UserModule::t("Register"))
));

$this->endWidget();
?>
