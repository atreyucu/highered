<?php
/** @var $this Controller */
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login");
$this->breadcrumbs = array(
    UserModule::t("Login"),
);
$this->title = UserModule::t("Login");
/** @var $form TbActiveForm */
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-form',
));
//$model =User::model();

echo $form->textFieldGroup(User::model()->getAttribute('username'), 'username');
echo $form->passwordFieldRow($model, 'password');
echo $form->checkBoxRow($model, 'rememberMe');
echo '<div class="control-group "><div class="controls">';
echo CHtml::link(UserModule::t("Register"), Yii::app()->getModule('user')->registrationUrl)
    . ' | ' . CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl);
echo '</div></div>';
echo $this->renderFormFooter();
echo TbHtml::formActions(array(
    TbHtml::submitButton(UserModule::t("Login"))
));

$this->endWidget();
$this->widget('ext.hoauth.widgets.HOAuth');
