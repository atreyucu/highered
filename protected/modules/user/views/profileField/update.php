<?php
$this->breadcrumbs = array(
    UserModule::t('Profile Fields') => array('admin'),
    $model->title => array('view', 'id' => $model->id),
    UserModule::t('Update'),
);


$this->title = UserModule::t('Update ProfileField ') . " " . $model->id;
$this->boxButtons = array();
$this->boxButtons[] = array('icon' => 'eye-open', 'label' => 'Ver', 'url' => array('/user/profileField/view', 'id' => $model->id));
$this->boxButtons[] = array('icon' => 'pencil', 'label' => 'Editar', 'url' => array('/user/profileField/update', 'id' => $model->id));
$this->boxButtons[] = array('icon' => 'trash', 'label' => 'Eliminar',
    'url' => array('/user/profileField/delete', 'id' => $model->id),
    'linkOptions' => array(
        'submit' => array('/user/profileField/delete', 'id' => $model->id),
        'confirm' => UserModule::t('Are you sure to delete this item?')
    )
);
//$this->boxButtons += array_merge($this->boxButtons, $this->menu);

?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>