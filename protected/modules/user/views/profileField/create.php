<?php
$this->breadcrumbs = array(
    UserModule::t('Profile Fields') => array('admin'),
    UserModule::t('Create'),
);

$this->title = UserModule::t("Create Profile Field");
//$this->boxButtons = $this->menu;
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>