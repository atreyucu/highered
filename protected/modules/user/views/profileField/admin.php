<?php
$this->breadcrumbs = array(
    UserModule::t('Profile Fields') => array('admin'),
    UserModule::t('Manage'),
);
$this->title = UserModule::t("Manage Profile Fields");

$this->beginWidget('ext.unicorn.widgets.IRBox', array(
    'title' => UserModule::t("Manage Profile Fields"),
));

$this->widget('application.extensions.bootstrap.widgets.TbGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        'id',
        'varname',
        array(
            'name' => 'title',
            'value' => 'UserModule::t($data->title)',
        ),
        'field_type',
        'field_size',
        //'field_size_min',
        array(
            'name' => 'required',
            'value' => 'ProfileField::itemAlias("required",$data->required)',
        ),
        //'match',
        //'range',
        //'error_message',
        //'other_validator',
        //'default',
        'position',
        array(
            'name' => 'visible',
            'value' => 'ProfileField::itemAlias("visible",$data->visible)',
        ),
        //*/
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        ),
    ),
));

$this->endWidget();
