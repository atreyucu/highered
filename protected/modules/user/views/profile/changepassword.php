<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Change Password");
$this->breadcrumbs = array(
    UserModule::t("Profile") => array('/user/profile'),
    UserModule::t("Change Password"),
);
$this->title = UserModule::t("Change password");
/** @var $form TbActiveForm */
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'password-form',
    'type' => 'horizontal',
));

echo $form->passwordFieldGroup(
    $model,
    'password',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-xs-12 .col-sm-6 .col-md-8',
            'hint' => UserModule::t("Minimal password length 4 symbols.")
        ),
        'prepend' => '<i class="glyphicon glyphicon-lock"></i>'
    )
);

echo $form->passwordFieldGroup(
    $model,
    'verifyPassword',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-xs-12 .col-sm-6 .col-md-8',

        ),
        'prepend' => '<i class="glyphicon glyphicon-lock"></i>'
    )
);
?>


<div class="form-actions">
  <span class="pull-right">
  <?php echo CHtml::submitButton(UserModule::t("Save"), array('class' => 'btn btn-primary')); ?>
        </span>
</div>

<?php $this->endWidget() ?>

