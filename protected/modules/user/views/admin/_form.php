<?php
echo TbHtml::beginFormTb('horizontal', '', 'post', array('enctype' => 'multipart/form-data'));
echo TbHtml::errorSummary($model);
echo TbHtml::errorSummary($profile);

echo TbHtml::activeTextFieldControlGroup($model, 'username', array('size' => 20, 'maxlength' => 20));

echo TbHtml::activePasswordFieldControlGroup($model, 'password', array('size' => 60, 'maxlength' => 128));

echo TbHtml::activeTextFieldControlGroup($model, 'email', array('size' => 60, 'maxlength' => 128));

echo TbHtml::activeDropDownListControlGroup($model, 'superuser', User::itemAlias('AdminStatus'));

echo TbHtml::activeDropDownListControlGroup($model, 'status', User::itemAlias('UserStatus'));
$this->profileEditFields($profile);


echo $this->renderFormFooter();
echo '<div class="form-actions">';
echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array(
    'class' => 'btn btn-primary'
));
echo '</div>';
echo CHtml::endForm();
