<?php
/**
 * AuthWebUser class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package auth.components
 */

/**
 * Web user that allows for passing access checks when enlisted as an administrator.
 *
 * @property boolean $isAdmin whether the user is an administrator.
 */
class AuthWebUser extends CWebUser
{
    /**
     * @var string[] a list of names for the users that should be treated as administrators.
     */
    public $admins = array('admin');

    /**
     * Initializes the component.
     */
    public function init()
    {
        parent::init();
        $this->setIsAdmin(in_array($this->name, $this->admins));
    }

    /**
     * Returns whether the logged in user is an administrator.
     * @return boolean the result.
     */
    public function getIsAdmin()
    {
        return $this->getState('__isAdmin', false);
    }

    /**
     * Sets the logged in user as an administrator.
     * @param boolean $value whether the user is an administrator.
     */
    public function setIsAdmin($value)
    {
        $this->setState('__isAdmin', $value);
    }

    /**
     * Performs access check for this user.
     * @param string $operation the name of the operation that need access check.
     * @param array $params name-value pairs that would be passed to business rules associated
     * with the tasks and roles assigned to the user.
     * @param boolean $allowCaching whether to allow caching the result of access check.
     * @return boolean whether the operations can be performed by this user.
     */
    public function checkAccess($operation, $params = array(), $allowCaching = true)
    {
        if ($this->getIsAdmin()) {
            return true;
        }

        return parent::checkAccess($operation, $params, $allowCaching);
    }

    /**
     * Return if user have permision to see.
     * param $role is the name of the role
     * @return boolean
     */
    public static function havePermission($roles = array())
    {
        if (Yii::app()->user->isGuest) {
            return false;
        } else {
            if (Yii::app()->user->isAdmin) {
                return true;
            }
            $rolesassiment = Yii::app()->db->createCommand()
                ->select('*')
                ->from('authassignment')
                ->where('userid = :user', array(':user' => Yii::app()->user->id))
                ->queryAll();
            foreach ($roles as $rol) {
                foreach ($rolesassiment as $item) {
                    if ($item['itemname'] == $rol) {
                        return true;
                    }
                }
            }
            return false;
            // return self::$_admin;
        }
    }

    /**
     * Return if user have permision to see.
     * param $role is the name of the role
     * @return boolean
     */
    public function getPermissionbyRoles($roles = array())
    {

        $result = mod('user')->getAdmins();
        foreach ($roles as $rol) {
            $users = Yii::app()->db->createCommand()
                ->select('users.username')
                ->from('authassignment authassignment')
                ->join('users users', 'authassignment.userid = users.id')
                ->where('authassignment.itemname = :rol', array(':rol' => $rol))
                ->queryAll();
            if (count($users) > 0) {
                foreach ($users as $user) {
                    $result[] = $user['username'];
                }
            }
        }
        return $result;

    }

}
