<?php $common_data = Yii::app()->getController()->common_data; ?>
<?php $company_info = Yii::app()->getController()->get_company_info(); ?>


<?php $action_id = Yii::app()->controller->action->id;?>

<div class="site-menu">
    <a href="" class="close-btn"><i class="fa fa-times fa-inverse"></i></a>
    <div class="flex-wrapper">
        <a class="menu-portrait" href="mailto:<?php echo $company_info['email']?>"><img src="<?php echo $common_data['menu']['portrait_image']?>" alt="<?php echo $common_data['menu']['portrait_alt']?>"></a>
        <ul class="navigation">
            <li><a href="<?php echo $this->createUrl('/')?>" class="nav-link hvr-underline-from-left-white <?php if($action_id=='index'):?>active<?php endif;?>"><?php echo $common_data['menu']['home']?></a></li>
            <li><a href="<?php echo $this->createUrl('/frontend/default/about_us')?>" class="nav-link hvr-underline-from-left-white <?php if($action_id=='about_us'):?>active<?php endif;?>"><?php echo $common_data['menu']['about']?></a></li>
            <li><a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="nav-link hvr-underline-from-left-white <?php if($action_id=='services'):?>active<?php endif;?>"><?php echo $common_data['menu']['service']?></a></li>
            <li><a href="<?php echo $this->createUrl('/frontend/default/contact_us')?>" class="nav-link hvr-underline-from-left-white <?php if($action_id=='contact_us'):?>active<?php endif;?>"><?php echo $common_data['menu']['contact']?></a></li>
        </ul>
        <footer class="menu-footer text-center">
            <a href="index.html" class="menu-logo"><img class="responsive-image" src="<?php echo $common_data['menu']['logo']?>" alt="<?php echo $common_data['menu']['logo_alt']?>"></a>
            <ul class="menu-socials">
                <?php if($common_data['menu']['facebook']):?><li><a href="<?php echo $common_data['menu']['facebook']?>"><i class="fa fa-facebook fa-inverse"></i></a></li><?php endif;?>
                <?php if($common_data['menu']['youtube']):?><li><a href="<?php echo $common_data['menu']['youtube']?>"><i class="fa fa-youtube fa-inverse"></i></a></li><?php endif;?>
                <?php if($common_data['menu']['linkedin']):?><li><a href="<?php echo $common_data['menu']['linkedin']?>"><i class="fa fa-linkedin fa-inverse"></i></a></li><?php endif;?>
            </ul>
        </footer>
    </div>
</div>