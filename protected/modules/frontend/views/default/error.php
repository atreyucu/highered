
<a href="index.html" class="site-logo mobile-small"><img class="responsive-image" src="<?php echo $logo?>" alt="<?php echo $logo_alt?>"></a>

<div class="error-page" style="background: url('<?php echo $background_image?>?1443697098') no-repeat scroll center center / cover rgba(0, 0, 0, 0);">
    <div class="dark-mask"></div>
    <div class="error-text text-center">
        <h1 class="jumbo-text"><?php echo $title?></h1>
        <h3 class="banner-caption"><?php echo $message?></h3>
        <a href="index.html" class="site-btn btn-transparent"><?php echo $button_text?></a>
    </div>
</div>