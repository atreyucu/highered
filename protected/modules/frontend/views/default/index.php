
<a href="" class="menu-trigger"><i class="fa fa-bars fa-2x fa-inverse"></i></a>

<?php Yii::app()->getController()->renderPartial('_menu', array());?>
<?php $logo = Yii::app()->getController()->common_data['header']['logo']; ?>
<?php $logo_alt = Yii::app()->getController()->common_data['header']['logo_alt']; ?>

<section class="cd-section visible">
    <div>
        <a href="<?php echo $this->createUrl('/')?>" class="site-logo"><img class="responsive-image" src="<?php echo $logo?>" alt="<?php echo $logo_alt?>"></a>
        <div class="main-carousel">
            <!-- This first slide is a html markup example when a video is desired, for a regular image slide see
                 the other "slide-item"-->
            <!-- Personally, i don't like inline style, but this is needed to have control over witch image will be
                 displayed as background when the video tag get hidden in mobile devices-->
            <?php if($banners):?>
                <?php foreach($banners as $key=>$banner):?>
                    <?php if($banner['type'] == 2):?>
                        <div class="slide-item video-slide"
                             style="background: url('<?php echo $banner['video_img']?>') no-repeat center center / cover;">
                            <video loop muted autoplay poster="<?php echo $banner['video_img']?>" class="bg-video">
<!--                                <source src="/static/videos/WEBM/Slide-Show.webm" type="video/webm">-->
                                    <source src="<?php echo $banner['video']?>" type="<?php echo $banner['video_format']?>">
<!--                                <source src="/static/videos/OGV/Slide-Show.ogv" type="video/ogg">-->
                                <?php foreach($banner['other_vid'] as $vid):?>
                                    <source src="<?php echo $vid['video']?>" type="<?php echo $vid['format']?>">
                                <?php endforeach;?>
                            </video>
                            <div class="slide-content text-center">
                                <h1 class="jumbo-caption"><?php echo $banner['title']?></h1>
                                <p class="banner-caption"><?php echo $banner['subtitle']?></p>
                                <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent"><?php echo $banner['start_text']?></a>
                            </div>
                        </div>
                    <?php else:?>
                        <div class="slide-item bg-image">
                            <img src="<?php echo $banner['image']?>" alt="<?php echo $banner['image_alt']?>" class="image-to-bg"/>
                            <div class="slide-content text-center">
                                <h1 class="jumbo-caption"><?php echo $banner['title']?> </h1>
                                <p class="banner-caption"><?php echo $banner['subtitle']?></p>
                                <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent"><?php echo $banner['start_text']?></a>
                            </div>
                        </div>
                    <?php endif?>
                <?php endforeach?>
            <?php else:?>
                <div class="slide-item video-slide"
                     style="background: url('/static/videos/Snapshots/Slide-Show.jpg') no-repeat center center / cover;">
                    <video loop muted autoplay poster="/static/videos/Snapshots/Slide-Show.jpg" class="bg-video">
                        <source src="/static/videos/WEBM/Slide-Show.webm" type="video/webm">
                        <source src="/static/videos/MP4/Slide-Show.mp4" type="video/mp4">
                        <source src="/static/videos/OGV/Slide-Show.ogv" type="video/ogg">
                    </video>
                    <div class="slide-content text-center">
                        <h1 class="jumbo-caption">Join us Today</h1>
                        <p class="banner-caption">It's time to secure your future</p>
                        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent">get started</a>
                    </div>
                </div>

                <div class="slide-item bg-image">
                    <img src="/static/img/carousel/image-01.jpg" alt="" class="image-to-bg"/>
                    <div class="slide-content text-center">
                        <h1 class="jumbo-caption">Cur index mori? </h1>
                        <p class="banner-caption">Imber de primus historia, carpseris pes! </p>
                        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent">get started</a>
                    </div>
                </div>
            <?php endif?>

        </div>
    </div>
</section>

<section class="cd-section">
    <div>
        <div class="section-2">
            <div class="promo-image bg-image">
                <img src="<?php echo $image?>" alt="<?php echo $image_alt?>" class="image-to-bg">
                <div class="image-mask"></div>
            </div>
            <div class="section-2-content">
                <div class="section-title text-center">
                    <h2 class="title"><?php echo $title?></h2>
                </div>
                <div class="fact text-center">
                    <?php echo $subtitle_p1?>
                </div>
                <div class="fact text-center">
                    <?php echo $subtitle_p2?>
                </div>
                <div class="text-center">
                    <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn"><?php echo $search_button_text?></a>
                </div>
                <?php echo $description?>
                <div class="dark-strip"></div>
            </div>
        </div>
    </div>
</section>

<section class="cd-section">
    <div>
        <div class="section-3">
            <div class="section-3-content">
                <div class="section-title text-center">
                    <h2 class="title"><?php echo $title2?></h2>
                </div>
                <div class="fact text-center">
                    <?php echo $description2?>
                </div>
            </div>
            <div class="promo-image bg-image">
                <img src="<?php echo $image2?>" alt="<?php echo $image2_alt?>" class="image-to-bg">
            </div>
        </div>
    </div>
</section>

<section class="cd-section">
    <div style="background-image: url('<?php echo $image3?>');">
        <div class="section-4">
            <div class="banner-text">
                <h2 class="text-center"><?php echo $title3?></h2>
                <p class="text-center">
                    <?php echo $description3?>
                </p>
            </div>
        </div>
    </div>
</section>

<section class="cd-section">
    <div>
        <div class="section-5">
            <div class="promo-image bg-image">
                <img class="image-to-bg" src="<?php echo $left_image?>" alt="<?php echo $left_image_alt?>">
            </div>
            <div class="section-5-content">
                <div class="section-title">
                    <h2 class="title text-center"><?php echo $title4?></h2>
                </div>
                <div class="fact">
                    <?php echo $subtitle_p12?>
                </div>
                <div class="fact">
                    <?php echo $subtitle_p22?>
                </div>
                <div class="text-center">
                    <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn"><?php echo $get_started_button_text?></a>
                </div>
            </div>
            <div class="promo-image bg-image">
                <img class="image-to-bg" src="<?php echo $right_image?>" alt="<?php echo $right_image_alt?>">
            </div>
        </div>
    </div>
</section>

<section class="cd-section">
    <div>
        <div class="section-6">
            <div class="section-title">
                <h2 class="title"><?php echo $title5?></h2>
            </div>
            <div class="story-text">
                <?php echo $description5?>
            </div>
            <div class="services">
                <div class="service-1">
                    <img src="<?php echo $left_image5?>" alt="<?php echo $left_image5_alt?>">
                    <div class="service-hover">
                        <p class="hover-text"><?php echo $left_title5?></p>
                        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent"><?php echo $left_btn_text5?></a>
                    </div>
                </div>

                <div class="service-2">
                    <img src="<?php echo $middle_image5?>" alt="<?php echo $middle_image5_alt?>">
                    <div class="service-hover">
                        <p class="hover-text"><?php echo $middle_title5?></p>
                        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent"><?php echo $middle_btn_text5?></a>
                    </div>
                </div>

                <div class="service-3">
                    <img src="<?php echo $right_image5?>" alt="<?php echo $right_image5_alt?>">
                    <div class="service-hover">
                        <p class="hover-text"><?php echo $right_title5?></p>
                        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn btn-transparent"><?php echo $right_btn_text5?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>