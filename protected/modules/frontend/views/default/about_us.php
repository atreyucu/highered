<?php
$data = Yii::app()->getController()->common_data;
  $header = $data['header'];

  $logo = $header['logo'];
  $logo_alt = $header['logo_alt']
?>
<a href="<?php echo $this->createUrl('/')?>" class="site-logo"><img class="responsive-image" src="<?php echo $logo?>" alt="<?php echo $logo_alt?>"></a>
<a href="" class="menu-trigger"><i class="fa fa-bars fa-2x fa-inverse"></i></a>

<?php Yii::app()->getController()->renderPartial('_menu', array());?>

<!-- Pages Header -->
<header class="page-header" data-parallax="scroll" data-image-src="<?php echo $banner_image;?>">
    <div class="dark-mask"></div>
</header>
<div class="page-title">
    <h2 class="text-center"><?php echo $title;?></h2>
</div>
<!-- /Pages Header -->
<div class="container">
    <div class="page-text">
        <?php echo $description;?>
    </div>
    <div class="contact-person">
        <div class="contact-photo text-center">
            <img class="image-responsive" src="<?php echo $biography_image?>" alt="<?php echo  $biography_image_alt?>">
            <div class="text-center">
                <a href="<?php echo Yii::app()->controller->createUrl('/contact-us')?>" class="site-btn"><?php echo $contact_me_btn_text?></a>
            </div>
        </div>
        <div class="about-info">
            <h2 class="name"><?php echo $biography_title?></h2>
            <p class="small-sub"><?php echo $biography_subtitle?></p>
            <div class="info-text">
                <?php echo $biography_description?>
            </div>
        </div>
    </div>
</div> <!-- /container -->
<div class="fancy-section">
    <div class="top">
        <div class="image-section left half" data-parallax="scroll" data-image-src="<?php echo $left_image?>"></div>
        <!--<div class="image-section left bg-image half">-->
        <!--<img class="image-to-bg" src="/static/img/about-img-01.jpg">-->
        <!--</div>-->
        <div class="content half ">
            <div class="content-item text-center">
                <h2 class="title"><?php echo $mission_title?></h2>
                <div class="description">
                    <?php echo $mission_text?>
                </div>
            </div>

            <div class="content-item text-center">
                <h2 class="title"><?php echo $services_title?></h2>
                <div class="description">
                    <?php echo $services_text?>
                </div>
            </div>

            <div class="content-item text-center">
                <h2 class="title"><?php echo $committed_title?></h2>
                <div class="description">
                    <?php echo $committed_text?>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="content half">
            <div class="content-item text-center">
                <h2 class="title"><?php echo $vision_title?></h2>
                <div class="description">
                   <?php echo $vision_text?>
                </div>
            </div>

            <div class="content-item text-center">
                <h2 class="title"><?php echo $clientele_title?></h2>
                <div class="description">
                    <?php echo $Clientele_text?>
                </div>
            </div>

            <div class="content-item text-center">
                <h2 class="title"><?php echo $committed_title2?></h2>
                <div class="description">
                   <?php echo $committed_text2?>
                </div>
            </div>
        </div>

        <div class="image-section right half" data-parallax="scroll" data-image-src="<?php echo $right_image?>"></div>
        <!--<div class="image-section right bg-image half">-->
        <!--<img class="image-to-bg" src="/static/img/about-img-02.jpg">-->
        <!--</div>-->
    </div>
</div> <!-- /.fancy-section -->
