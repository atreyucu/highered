<a href="index.html" class="site-logo mobile-small"><img class="responsive-image" src="<?php echo $logo?>" alt="<?php echo $logo_alt?>"></a>

<div class="error-page thank-page" style='<?php echo $background_image?>?1443697098') no-repeat scroll center center / cover rgba(0, 0, 0, 0);">
    <div class="dark-mask"></div>
    <div class="error-text text-center">
        <h1 class="jumbo-text"><?php echo $title?></h1>
        <h3 class="banner-caption"><?php echo $subtitle?></h3>
        <p class="message-text">
            <?php echo $description?>
        </p>
        <a href="<?php echo $this->createUrl('/')?>" class="site-btn btn-transparent"><?php echo $home_button_text?></a>
        <a href="<?php echo $this->createUrl('/frontend/default/services')?>" class="site-btn red-btn"><?php echo $services_button_text?></a>
    </div>
</div>