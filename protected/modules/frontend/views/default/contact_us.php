<style>
    .error {
        border-color: red;
    }
</style>
<?php
$cs = Yii::app()->clientScript;
$cs->registerCssFile($base_url . '/static/css/jquery.qtip.css');
$cs->registerScriptFile(Yii::app()->baseUrl . '/static/js/jquery.qtip.min.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->baseUrl . '/static/js/imagesloaded.pkg.min.js',CClientScript::POS_END);
?>

<?php
$data = Yii::app()->getController()->common_data;
$header = $data['header'];

$logo = $header['logo'];
$logo_alt = $header['logo_alt']
?>
    <a href="<?php echo $this->createUrl('/') ?>" class="site-logo"><img class="responsive-image"
                                                                         src="<?php echo $logo ?>"
                                                                         alt="<?php echo $logo_alt ?>"></a>
    <a href="" class="menu-trigger"><i class="fa fa-bars fa-2x fa-inverse"></i></a>

<?php Yii::app()->getController()->renderPartial('_menu', array()); ?>

    <!-- Pages Header -->
    <header class="page-header" data-parallax="scroll" data-image-src="<?php echo $banner_image ?>">
        <div class="dark-mask"></div>
    </header>
    <div class="page-title">
        <h2 class="text-center"><?php echo $title ?></h2>
    </div>
    <!-- /Pages Header -->

    <div class="container">
        <div class="page-text">
            <?php echo $description ?>
        </div>
    </div> <!-- /.container -->

    <div class="contact-layout">
        <div class="basic-info" data-parallax="scroll" data-image-src="<?php echo $left_image ?>">
            <div class="fancy-info">
                <h2 class="title"><?php echo $company_info['name'] ?></h2>

                <p class="address"><?php echo $company_info['address'] ?></p>
                <a href="mailto:<?php echo $company_info['email'] ?>"
                   class="hvr-underline-from-left-beige"><?php echo $company_info['email'] ?></a>

                <p class="phone"><?php echo $company_info['phone1'] ?></p>
                <ul class="menu-socials white">
                    <li><a href="<?php echo $social['facebook'] ?>"><i class="fa fa-facebook fa-inverse"></i></a></li>
                    <li><a href="<?php echo $social['youtube'] ?>"><i class="fa fa-youtube fa-inverse"></i></a></li>
                    <li><a href="<?php echo $social['linkedin'] ?>"><i class="fa fa-linkedin fa-inverse"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="form-wrapper">
            <!--<div class="flash" style="clear: both; display: block;">

            </div>-->
            <?php
            /** @var $model ContactForm */
            /** @var $this GxController */
            /** @var $form TbActiveForm */
          $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                'id' => 'contact_page_form',
                'enableAjaxValidation' => true,
                'htmlOptions' => array('class' => 'contact-form'),
            )); ?>
<!--            <form method="post" class="contact-form" action="/frontend/default/sendmail">-->
                <h3 class="form-title text-center"><?php echo $form_title ?></h3>
                <div class="control-wrapper">
                    <?php echo $form->textField($model, 'name', array('data-title' => CHtml::encode($model->getError('name')),'placeholder'=>$model->getAttributeLabel('name'), 'class'=>'form-control')); ?>
<!--                    <input type="text" class="form-control" placeholder="Your name" name="name">-->
                </div>
                <div class="control-wrapper">
                    <?php echo $form->textField($model, 'email', array('data-title' => CHtml::encode($model->getError('email')),'placeholder'=>$model->getAttributeLabel('email'), 'class'=>'form-control')); ?>
<!--                    <input type="email" class="form-control" placeholder="Your Email" name="email">-->
                </div>
                <div class="control-wrapper">
                    <?php echo $form->textArea($model, 'body', array('data-title' => CHtml::encode($model->getError('body')),'placeholder'=>$model->getAttributeLabel('body'), 'class'=>'form-control','rows'=>6)); ?>
<!--                    <textarea class="form-control" rows="6" placeholder="Message..." name="message"></textarea>-->
                </div>
                <div class="control-wrapper">
                    <button type="submit" class="site-btn"><?php echo $from_button_text ?></button>
                </div>
<!--            </form>-->
            <?php $this->endWidget(); ?>
        </div>
    </div> <!-- /.contact-layout -->

    <div class="full-width-link">
        <a class="jumbo-link"
           href="<?php echo $this->createUrl('/frontend/default/services'); ?>"><?php echo $title2 ?></a>
    </div>

    <!-- Map Wrapper, was set as background image, please note that full width map is not advised for mobile ready sites
         since they modify the normal scroll behaviour, for example, when over a map and try to slide up, the map is the
         one that catch the scroll event and not de site itself -->
<?php if (!$use_map): ?>
    <div class="map-wrapper"
         style="background: url('<?php echo $map_image ?>?1441537918') no-repeat scroll center center / cover rgba(0, 0, 0, 0);">
    </div>
<?php else: ?>
    <input type="hidden" id="latitude" value="<?php echo $latitude ?>">
    <input type="hidden" id="longitude" value="<?php echo $longitude ?>">
    <input type="hidden" id="zoom" value="13">

    <div id="map_contact" class="map-wrapper"></div>
<?php endif ?>


<?php

$js = <<<JS

     $('[data-title!=""]').qtip({ // Grab all elements with a non-blank data-tooltip attr.
    content: {
        attr: 'data-title' // Tell qTip2 to look inside this attr for its content
    },
     position: {
        my: 'top left',
        at: 'bottom center'
    },
    style: {
        classes: 'qtip-dark',

    }
})
JS;

cs()->registerScript('contact', $js);


?>