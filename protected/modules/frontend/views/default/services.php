<?php
$data = Yii::app()->getController()->common_data;
$header = $data['header'];

$logo = $header['logo'];
$logo_alt = $header['logo_alt']
?>
<a href="<?php echo $this->createUrl('/')?>" class="site-logo"><img class="responsive-image" src="<?php echo $logo?>" alt="<?php echo $logo_alt?>"></a>
<a href="" class="menu-trigger"><i class="fa fa-bars fa-2x fa-inverse"></i></a>

<?php Yii::app()->getController()->renderPartial('_menu', array());?>

<!-- Pages Header -->
<header class="page-header" data-parallax="scroll" data-image-src="<?php echo $banner_image?>">
    <div class="dark-mask"></div>
</header>
<div class="page-title">
    <h2 class="text-center"><?php echo $title?></h2>
</div>
<!-- /Pages Header -->

<div class="container">
    <div class="page-text">
        <?php echo $description?>
    </div>
</div> <!-- /.container -->

<div class="parallax-schedule" data-parallax="scroll" data-image-src="<?php echo $background_image?>">
    <div class="schedule">
        <h2 class="title"><?php echo $title2?></h2>
        <p class="description">
            <?php echo $description2?>
        </p>
        <ul class="days">
            <li>
                <h4><?php echo $nights_title?></h4>
                <p><?php echo $nights_hours?></p>
            </li>
            <li>
                <h4><?php echo $weekend_title?></h4>
                <p><?php echo $weekend_hours?></p>
            </li>
        </ul>
    </div>
    <a href="<?php echo $this->createUrl('/frontend/default/contact_us')?>" class="site-btn"><?php echo $contact_button_text?></a>
</div> <!-- /.parallax-schedule -->

<div class="session-board">
    <div class="list-wrapper">
        <i class="fa fa-angle-right fa-inverse nav-trigger"></i>
        <div class="boards-list">
            <?php foreach($services as $service):?>
                <div class="board-nav">
                    <div class="entry-name"><?php echo $service['name']?></div>
                    <img class="right-pointer" src="/static/img/arrow-right.png" alt="">
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content-carousel">
            <?php foreach($services as $service):?>
                <dl class="boards-content">
                    <?php foreach($service['topics'] as $topic):?>
                        <dt class="item-title"><?php echo $topic['title']?></dt>
                        <dt class="hours"><?php echo $topic['time']?></dt>
                        <dd class="item-content">
                            <?php echo $topic['description']?>
                        </dd>
                    <?php endforeach?>
                </dl>
            <?php endforeach?>
        </div> <!-- /.content-carousel -->
    </div>
</div> <!-- /.session-board -->