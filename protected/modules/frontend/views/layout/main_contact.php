<?php $base_url = Yii::app()->request->baseUrl; ?>
<?php $cs = Yii::app()->clientScript; ?>
<?php $action_id = Yii::app()->controller->action->id;?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>
    <?php echo $this->common_data['general_tracking'] ?>


    <?php
    Yii::app()->clientScript->scriptMap = array(
        'bootstrap.min.css' => false,
        'font-awesome.min.css' => false,
        'bootstrap-yii.css' => false,
        'jquery-ui-bootstrap.css' => false,

        'jquery.min.js' => false,
        'jquery.js' => false,
        'bootstrap-noconflict.js' => false,
        'bootbox.min.js' => false,
        'notify.min.js' => false,
        'bootstrap.min.js' => false,
    );  // OJO

    $base_url = Yii::app()->request->baseUrl;
    $cs = Yii::app()->clientScript;
    ?>

    <?php $cs->registerCssFile($base_url . '/static/css/reset.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/slick/slick.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/slick/slick-theme.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/css/font-awesome.min1.css'); ?>

    <?php $cs->registerCssFile($base_url . '/static/css/style.css'); ?>

    <script src="/static/js/modernizr.js"></script>


</head>
<body class="pages">
<?php
$action_id = Yii::app()->controller->action->id;
?>



<?php
echo $content;
?>

<?php $common_data = Yii::app()->getController()->common_data;?>

<div class="bottom-info floated">
    <p><?php echo date('Y')?> &copy; <?php echo $common_data['footer']['copy_right']?></p>
    <p><?php echo $common_data['footer']['developer_by_text']?> <a href="<?php echo $common_data['footer']['developer_site_url']?>"><?php echo $common_data['social']['developer_by_link_text']?></a></p>
</div>


<script src="/static/js/jquery-2.1.4.js"></script>
<script src="/static/js/parallax.min.js"></script>
<script src="/static/slick/slick.min.js"></script>
<script src="/static/js/site-scripts.js"></script>




<?php if(Yii::app()->getController()->there_is_map()):?>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="/static/js/contact.js"></script>
<?php endif?>


</body>
</html>