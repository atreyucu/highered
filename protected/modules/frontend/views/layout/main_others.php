<?php $base_url = Yii::app()->request->baseUrl; ?>
<?php $cs = Yii::app()->clientScript; ?>
<?php $action_id = Yii::app()->controller->action->id;?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>
    <?php echo $this->common_data['general_tracking'] ?>


    <?php
    Yii::app()->clientScript->scriptMap = array(
        'bootstrap.min.css' => false,
        'font-awesome.min.css' => false,
        'bootstrap-yii.css' => false,
        'jquery-ui-bootstrap.css' => false,

        'jquery.min.js' => false,
        'jquery.js' => false,
        'bootstrap-noconflict.js' => false,
        'bootbox.min.js' => false,
        'notify.min.js' => false,
        'bootstrap.min.js' => false,
    );  // OJO

    $base_url = Yii::app()->request->baseUrl;
    $cs = Yii::app()->clientScript;
    ?>

    <?php $cs->registerCssFile($base_url . '/static/css/reset.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/slick/slick.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/slick/slick-theme.css'); ?>
    <?php $cs->registerCssFile($base_url . '/static/css/font-awesome.min1.css'); ?>

    <?php $cs->registerCssFile($base_url . '/static/css/style.css'); ?>

    <script src="/static/js/modernizr.js"></script>


</head>
<body class="pages">
<?php
$action_id = Yii::app()->controller->action->id;
?>



<?php
echo $content;
?>

<?php $company_info = Yii::app()->getController()->get_company_info(); ?>
<?php $common_data = Yii::app()->getController()->common_data; ?>


<div class="page-footer">
    <div class="contact-info">

        <div class="info text-center">
            <div class="icon"><i class="fa fa-home"></i></div>
            <p class="label">Address</p>
            <div class="info-content">
                <?php echo $company_info['address']?>
            </div>
        </div>

        <div class="info text-center">
            <div class="icon"><i class="fa fa-phone"></i></div>
            <p class="label">Phone</p>
            <div class="info-content">
                <?php echo $company_info['phone1']?>
            </div>
        </div>

        <div class="info text-center">
            <div class="icon"><i class="fa fa-envelope-o"></i></div>
            <p class="label">E-Mail</p>
            <div class="info-content">
                <a href="mailto:<?php echo $company_info['email']?>" class="hvr-underline-from-left"><?php echo $company_info['email']?></a>
            </div>
        </div>

    </div>

    <div class="footer">
        <div class="wrapper">
            <div class="left-corner"></div>
            <div class="right-corner"></div>
            <div class="jumbo-text text-center">Join us today</div>
            <div class="fancy-line"></div>
            <ul class="footer-socials text-center">
                <?php if($common_data['social']['facebook']):?><li><a href="<?php echo $common_data['social']['facebook']?>"><i class="fa fa-facebook fa-inverse"></i></a></li><?php endif;?>
                <?php if($common_data['social']['youtube']):?><li><a href="<?php echo $common_data['social']['youtube']?>"><i class="fa fa-youtube fa-inverse"></i></a></li><?php endif;?>
                <?php if($common_data['social']['linkedin']):?><li><a href="<?php echo $common_data['social']['linkedin']?>"><i class="fa fa-linkedin fa-inverse"></i></a></li><?php endif;?>
            </ul>
        </div>
        <div class="bottom-info">
            <p><?php echo date('Y')?> &copy; <?php echo $common_data['footer']['copy_right']?></p>
            <p><?php echo $common_data['footer']['developer_by_text']?> <a href="<?php echo $common_data['footer']['developer_site_url']?>"><?php echo $common_data['footer']['developer_by_link_text']?></a></p>
        </div>
    </div>

</div>
</div> <!-- /.page-footer -->


<script src="/static/js/jquery-2.1.4.js"></script>
<script src="/static/js/parallax.min.js"></script>
<script src="/static/slick/slick.min.js"></script>
<script src="/static/js/site-scripts.js"></script>


<?php if($action_id == 'services'):?>
    <script src="/static/js/services.js"></script>
<?php endif?>
</body>
</html>