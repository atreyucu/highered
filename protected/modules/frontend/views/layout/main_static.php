<?php $base_url = Yii::app()->request->baseUrl; ?>
<?php $cs = Yii::app()->clientScript; ?>
<?php $action_id = Yii::app()->controller->action->id;?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>
    <?php echo $this->common_data['general_tracking'] ?>


    <?php
    Yii::app()->clientScript->scriptMap = array(
        'bootstrap.min.css' => false,
        'font-awesome.min.css' => false,
        'bootstrap-yii.css' => false,
        'jquery-ui-bootstrap.css' => false,

        'jquery.min.js' => false,
        'jquery.js' => false,
        'bootstrap-noconflict.js' => false,
        'bootbox.min.js' => false,
        'notify.min.js' => false,
        'bootstrap.min.js' => false,
    );  // OJO

    $base_url = Yii::app()->request->baseUrl;
    $cs = Yii::app()->clientScript;
    ?>

    <?php $cs->registerCssFile($base_url . '/static/css/reset.css'); ?>

    <?php $cs->registerCssFile($base_url . '/static/css/font-awesome.min1.css'); ?>

    <?php $cs->registerCssFile($base_url . '/static/css/style.css'); ?>

    <script src="/static/js/modernizr.js"></script>


</head>
<body class="pages">
<?php
$action_id = Yii::app()->controller->action->id;
?>



<?php
echo $content;
?>


</body>
</html>