<?php

class DefaultController extends Controller
{
    public $common_data;

    public function init()
    {
        $this->common_data = $this->getVariablesArray();

    }
    private function getVariablesArray()
    {
        $menu = MainMenu::model()->find();
        $social_media = SocialLinks::model()->find();
        $comp_info = CompanyInfo::model()->find();

        if(isset($menu))
        {
            $navigation_menu = array();
            $navigation_menu['portrait_image'] = $menu->_portrait_image->getFileUrl('normal');
            $navigation_menu['portrait_alt'] = $menu->portrait_image;

            $navigation_menu['home'] = $menu->home;
            $navigation_menu['about'] = $menu->about;
            $navigation_menu['service'] = $menu->service;
            $navigation_menu['contact'] = $menu->contact;

            $navigation_menu['facebook'] = $social_media->facebook;
            $navigation_menu['youtube'] = $social_media->youtube;
            $navigation_menu['linkedin'] = $social_media->linkedin;

        }
        else{
            $navigation_menu = array();
            $navigation_menu['portrait_image'] = '/static/img/contact-portrait.png';
            $navigation_menu['portrait_alt'] = 'Portrait';

            $navigation_menu['home'] = 'home';
            $navigation_menu['about'] = 'about';
            $navigation_menu['service'] = 'services';
            $navigation_menu['contact'] = 'contact';

            $navigation_menu['facebook'] = 'facebook';
            $navigation_menu['youtube'] = 'youtube';
            $navigation_menu['linkedin'] = 'linkedin';
        }

        if($comp_info){
            $header_arr = array();
            $header_arr['logo'] = $comp_info->_main_logo->getFileUrl('normal');
            $header_arr['logo_alt'] = $comp_info->main_logo;
            $header_arr['logo2'] = $comp_info->_secondary_logo->getFileUrl('normal');
            $header_arr['logo2_alt'] = $comp_info->secondary_logo;

            $navigation_menu['logo'] = $comp_info->_secondary_logo->getFileUrl('normal');
            $navigation_menu['logo_alt'] = $comp_info->secondary_logo;
        }
        else{
            $header_arr = array();
            $header_arr['logo'] = '/static/img/logo-red.png';
            $header_arr['logo_alt'] = 'Site logo';
            $header_arr['logo2'] = '/static/img/menu-logo.png';
            $header_arr['logo2_alt'] = 'Site logo';

            $navigation_menu['logo'] = '/static/img/menu-logo.png';
            $navigation_menu['logo_alt'] = 'Menu logo';
        }

        $footer = Footer::model()->find();
        if(isset($footer))
        {

            $footer_data = array();

            $footer_data['copy_right'] = $footer->copy_right;
            $footer_data['developer_by_text'] = $footer->developer_by_text;

            $footer_data['developer_by_link_text'] = $footer->developer_by_link_text;

            $footer_data['developer_site_url'] = $footer->developer_site_url;

        }

        $tracking_code = GeneralTrackingCodes::model()->find();
        if($tracking_code){
            $code = $tracking_code['source_code'];
        }

        $social = array();

        if($social_media){
            $social['facebook'] = $social_media->facebook;
            $social['youtube'] = $social_media->youtube;
            $social['linkedin'] = $social_media->linkedin;
        }
        else{
            $social['facebook'] = 'http://facebook.com';
            $social['youtube'] = 'http://youtube.com';
            $social['linkedin'] = 'http://linkedin.com';
        }



        return array(
            'general_tracking' => $code,
            'menu' => $navigation_menu,
            'footer' => $footer_data,
            'social' => $social,
            'header' => $header_arr,
        );
    }

    public function get_company_info()
    {
        $company_info = CompanyInfo::model()->find();
        $company_info_data = array();
        $company_info_data['email'] = 'contact@highered.us';
        $company_info_data['addr_line1'] = 'Miami, FL 33133/Coral Gables, FL 33145';
        $company_info_data['phone1'] = '1 305-888-9009';

        if(isset($company_info))
        {
            $company_info_data['name'] = $company_info->name;
            $company_info_data['email'] = $company_info->email;
            $company_info_data['phone1'] = $company_info->phone1;
            $company_info_data['phone2'] = $company_info->phone2;
            $company_info_data['address'] = $company_info->address;
        }

        return $company_info_data;
    }

    public function there_are_video(){
        $criteria = new CDbCriteria();
        $criteria->compare('banner_type',2);
        $count = HomeBannerSection::model()->count($criteria);

        return $count > 0;
    }

    public function there_is_map(){
        $section3 = ContactusPageSection3::model()->find();

        return $section3->use_map;
    }


    public function actions()
    {
        $this->layout = '/layout/main';
        $actions_path = 'application.modules.frontend.controllers.default';
        return array(
            'index' => $actions_path.'.IndexAction',
            'about_us' => $actions_path.'.AboutUsAction',
            'contact_us' => $actions_path.'.ContactUsAction',
            'services' => $actions_path.'.ServicesAction',
            'thanks' => $actions_path.'.ThanksAction',
//            'faqs' => $actions_path.'.FaqsAction',

            'changelang' => 'application.modules.seo.components.LanguageSwitcherAction',
        );
    }

    public function actionError()
    {
        $this->layout = '/layout/main_static';
        
        $error_page = ErrorPage::model()->find();
        
        if($error_page){
            $error_arr = array();
            
            $error_arr['background_image'] = $error_page->_background_image->getFileUrl('normal');
            $error_arr['title'] = $error_page->title;
            $error_arr['message'] = $error_page->message;
            $error_arr['button_text'] = $error_page->button_text;
        }
        else{
            $error_arr = array();

            $error_arr['background_image'] = '/static/img/error-img.jpg';
            $error_arr['title'] = 'Error page';
            $error_arr['message'] = 'We can’t seem to find the page you are looking for';
            $error_arr['button_text'] = 'GO HOMEPAGE';
        }


        $company_info = CompanyInfo::model()->find();

        if($company_info){
            $error_arr['logo'] = $company_info->_main_logo->getFileUrl('normal');
            $error_arr['logo_alt'] = $company_info->main_logo;
        }
        else{
            $error_arr['logo'] = 'static/img/logo-red.png';
            $error_arr['logo_alt'] = 'Site Logo';
        }




        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
            {
                $this->render('error', $error_arr);
            }
        }
    }

    public function actionSendmail()
    {
        $company_info = CompanyInfo::model()->find();
        $to_email = 'ncamps@ireevo.com';
	    $to_name = 'Prueba';
        if(isset($company_info)){
            $to_email = $company_info->email;
            $to_name = $company_info->name;
        }


        if(Yii::app()->request->isPostRequest){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = '<h2> Your message was received </h2><br>';
			$message= $message.'Thanks for contacting us we will reply to you shortly. <br><br>';
            $subject = $to_name;
            if (!$this->sendMail($to_name, $to_email, $email, $subject, $message, $description = 'Email notification')){
                $url = $this->createUrl('/frontend/default/contact_us');
                return $this->redirect($url.'#form-wrapper');
            }

            $subject = $_POST['subject'];
            $message = $_POST['message'];

            $message1 = t("Message from: ").'<br><br>'.
                        t("Name: ").$to_name.'<br>'.
                        t("Email: ").$to_email.'<br>'.
                        t("Message: "). $message;

            if(!$this->sendMail($name, $email, $to_email, $subject, $message1)){
                $url = $this->createUrl('/frontend/default/contact_us');
                return $this->redirect($url.'#form-wrapper');
            }

            $url = $this->createUrl('/frontend/default/thanks');
            return $this->redirect($url);
        }
    }

    public function sendMail($from_name,$from_email, $to_email, $subject,$message, $description = 'Contact') {

        $mail = new YiiMailer('contact', array('message' => nl2br($message), 'name' => $from_email, 'description' => $description));


        //set properties
        $mail->setFrom($from_email, $from_name);
        $mail->setSubject($subject);
        $mail->setTo($to_email);
		
        if ($mail->send()) {
            Yii::app()->user->setFlash('success',t('Thanks for contact us. We answers you soon as possible.'));
            return true;
        } else {
            Yii::app()->user->setFlash('error',t('Error sending email: ').$mail->getError());
            return false;
        }

    }


}



