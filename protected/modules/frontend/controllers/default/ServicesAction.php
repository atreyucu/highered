<?php

class ServicesAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = "/layout/main_others";
        
        $service_banner = ServiceBannerSection::model()->find();
        
        if($service_banner){
            $banner_image = $service_banner->_banner_image->getFileUrl('normal');
            $title = $service_banner->title;
            $description = $service_banner->description;
        }
        else{
            $banner_image = '/static/img/services-header.jpg';
            $title = 'Services';
            $description = "<p> We offer educational planning and consulting services to students and their families. While it is preferred to meet with our clients in person, we understand that you may only be able to meet through internet-based video technologies such as Skype, ooVoo, Hangouts, etc. We also provide some educational planning consultations through telephone, although it is less desirable. So join our Janus Scholars and fulfill your educational dreams. </p>
<p> If time and money are impediments to joining our one-on-one consultations as a Janus Scholar, you may want to become a Summer Academy Scholar and receive the guidance, information, and feedback that will give you the confidence needed in your college search. </p>
<p> Consistent with the HECA Professional Ethical Standards, we never receive money from colleges or other organizations for referrals and placements, and we never guarantee students (and their families) admissions to any college, university, or postsecondary institution. </p>
<p> Consistent with the HECA Professional Ethical Standards, we never receive money from colleges or other organizations for referrals and placements, and we never guarantee students (and their families) admissions to any college, university, or postsecondary institution. </p>";
        }
        
        $service_section2 = ServicePageSection2::model()->find();
        
        if($service_section2){
            $background_image = $service_section2->_background_image->getFileUrl('normal');
            $title2 = $service_section2->title;
            $description2 = $service_section2->description;
            $nights_title = $service_section2->nights_title;
            $nights_hours = $service_section2->nights_hours;
            $weekend_title = $service_section2->weekend_title;
            $weekend_hours = $service_section2->weekend_hours;
            $contact_button_text = $service_section2->contact_button_text;
        }
        else{
            $background_image = '/static/img/services-parallax.jpg';
            $title2 = 'Hours of Availability';
            $description2 = 'Understanding that most high school students attend school and their parents work during traditional business hours, we offer our educational planning and consulting services during most weeknights and weekends.';
            $nights_title = 'Most Weeknights';
            $nights_hours = '5:30pm - 9:00pm';
            $weekend_title = 'Most Weekends';
            $weekend_hours = '5:30pm - 9:00pm';
            $contact_button_text = 'Conatact us';
        }

        $services_arr = array();

        $plan_criteria = new CDbCriteria();

        $plan_criteria->order = 'created DESC';

        $plans = Plan::model()->findAll();

        foreach($plans as $plan){

            $temp_criteria = new CDbCriteria();

            $temp_criteria->compare('plan',$plan->id);

            $temp_criteria->order = 'orderr ASC';

            $topics = Topic::model()->findAll($temp_criteria);

            $temp_arr = array();

            foreach($topics as $topic){
                $temp_arr[$topic->id] = array('title'=>$topic->title,'time'=>$topic->time,'description'=>$topic->description);
            }

            $services_arr[$plan->id] = array('name' => $plan->title, 'topics'=>$temp_arr);
        }



        return $controller->render('services',
            array(
                'banner_image' => $banner_image,
                'title' => $title,
                'description' => $description,
                'background_image'=> $background_image,
                'title2'=> $title2,
                'description2'=> $description2,
                'nights_title'=> $nights_title,
                'nights_hours'=> $nights_hours ,
                'weekend_title'=> $weekend_title,
                'weekend_hours'=> $weekend_hours ,
                'contact_button_text'=> $contact_button_text,
                'services' => $services_arr
            )
        );

    }
}