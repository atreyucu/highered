<?php

class ThanksAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = '/layout/main_static';
        
        $thank_page = ThankPage::model()->find();
        
        if($thank_page){
            $thank_arr = array();
            $thank_arr['background_image'] = $thank_page->_background_image->getFileUrl('normal');
            $thank_arr['title'] = $thank_page->title;
            $thank_arr['subtitle'] = $thank_page->subtitle;
            $thank_arr['description'] = $thank_page->description;
            $thank_arr['home_button_text'] = $thank_page->home_button_text;
            $thank_arr['services_button_text'] = $thank_page->services_button_text;
        }
        else{
            $thank_arr = array();
            $thank_arr['background_image'] = '/static/img/error-img.jpg';
            $thank_arr['title'] = 'Thank you';
            $thank_arr['subtitle'] = 'for trust in our expertise';
            $thank_arr['description'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
            ullamcorper suscipit lobortis nisl ut aliquip ex ea commo';
            $thank_arr['home_button_text'] = 'Go Homepage';
            $thank_arr['services_button_text'] = 'Our Services';

        }

        $company_info = CompanyInfo::model()->find();

        if($company_info){
            $thank_arr['logo'] = $company_info->_main_logo->getFileUrl('normal');
            $thank_arr['logo_alt'] = $company_info->main_logo;
        }
        else{
            $thank_arr['logo'] = '/static/img/logo-red.png';
            $thank_arr['logo_alt'] = 'Site logo';
        }

        return $controller->render('thanks',
            $thank_arr
        );

    }
}