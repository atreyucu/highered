<?php

class AboutUsAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = "/layout/main_others";



        $about_page = AboutusBannerSection::model()->find();

        $about_section2 = AboutusPageSection2::model()->find();

        $about_section3 = AboutusPageSection3::model()->find();

        if($about_page){
            $banner_image = $about_page->_banner_image->getFileUrl('normal');
            $title = $about_page->title;
            $description = $about_page->description;
            $biography_image = $about_page->_biography_image->getFileUrl('normal');
            $biography_image_alt = $about_page->biography_image;
            $biography_title = $about_page->biography_title;
            $biography_subtitle = $about_page->biography_subtitle;
            $biography_description = $about_page->biography_description;
            $contact_me_btn_text = $about_page->contact_me_btn_text;
        }
        else{
            $banner_image = '/static/img/about-header.jpg';
            $title = 'About Us';
            $description = '<p>
            Higher Education Consultants of America, LLC is an education planning consultancy agency dedicated to
            college admissions consulting, graduate and professional school admissions consultancy, international
            college student applications.
        </p>
        <p>
            Academic, social, financial, occupational, and other dimensions dominate the questions and concerns
            families and students have with the college search process. HECA, LLC understands the many questions
            you have about college. We provide our clients and scholars with important information regarding colleges
            and universities, both their stated and unstated admissions requirements, campus culture, and  student
            interests, etc.
        </p>
        <p>
            Trying to find a best fit does not necessarily mean the most expensive or highest ranking institution.
            For some people, finding the best fit may mean focusing on college majors and available financial aid
            provided and/or private scholarships. After all, what good is it to be admitted to an Ivy League institution,
            if the degree you want to earn is not offered? Not much if you are interested in a specific career track.
        </p>
        <p>
            At Higher Education Consultants of America, we pride ourselves in ensuring that you understand the fit
            of the institution is the most important part of the decision to accept admissions to an institution.
            We put everything into perspective and collectively weigh your values and interests in the decision-making
            process.
        </p>';
            $biography_image = '/static/img/contact-photo.jpg';
            $biography_title = 'Pedro (Pete) Villarreal III, Ph.D.';
            $biography_subtitle = 'CEO and Chief Education Consultant';
            $biography_description = ' <p>
                    Pete has served in various roles at a variety of institutions including public and private,
                    religious and non-denomination, regional and major land grant universities, state-owned and
                    state-related, nationally ranked and unranked institutions such as George Washington University,
                    University of Florida, University of Miami, Pennsylvania State University, Baylor University, and
                    the University of Texas-Pan American.
                </p>
                <p>
                    His experiences as a professor teaching future administrators of colleges and universities gives
                    him the insight and practical knowledge of the ins and outs on how higher education institutions
                    organize themselves. This information can be quite useful to students and parents in the educational
                    planning and consulting process.
                </p>
                <p>
                    Pete has served in various roles at a variety of institutions including public and private,
                    religious and non-denomination, regional and major land grant universities, state-owned and
                    state-related, nationally ranked and unranked institutions such as George Washington University,
                    University of Florida, University of Miami, Pennsylvania State University, Baylor University, and
                    the University of Texas-Pan American.
                </p>';
            $contact_me_btn_text = 'Contact Me';
        }
        
        if($about_section2){
            $left_image = $about_section2->_left_image->getFileUrl('normal');
            $mission_title = $about_section2->mission_title;
            $mission_text = $about_section2->mission_text;
            $services_title = $about_section2->services_title;
            $services_text = $about_section2->services_text;
            $committed_title = $about_section2->committed_title;
            $committed_text = $about_section2->committed_text;
        }
        else{
            $left_image = '/static/img/about-img-01.jpg';
            $mission_title = 'Mission';
            $mission_text = ' We serve students and their families through the college search process including college matching,
                    college admissions and financial aid information dissemination, career exploration and major selection,
                    and academic preparation and curricular advice.';
            $services_title = 'Services';
            $services_text = ' We provide individual and group educational services to prospective college students. Specific
                    services include assistance with the college search process, college admissions information,
                    college matching; financial aid information; college applications; resume editing, and college
                    essay/personal statements editing. We also provide our clients with access to college and
                    university resources and guides.';
            $committed_title = 'Committed to Equity';
            $committed_text = 'We commit to serve a very limited number of academically talented students who come from extreme
                    poverty on a pro-bono basis.';
        }
        
        if($about_section3){
            $right_image = $about_section3->_right_image->getFileUrl('normal');
            $vision_title = $about_section3->vision_title;
            $vision_text = $about_section3->vision_text;
            $clientele_title = $about_section3->clientele_title;
            $Clientele_text = $about_section3->Clientele_text;
            $committed_title2 = $about_section3->committed_title;
            $committed_text2 = $about_section3->committed_text;
        }
        else{
            $right_image = '/static/img/about-img-02.jpg';
            $vision_title = 'Vision';
            $vision_text = ' We will be a leader in the area of independent educational consulting dedicated to prospective
                    college students and their families.';
            $clientele_title = 'Clientele';
            $Clientele_text = 'We serve three types of clients: 1) traditional students seeking assistance with undergraduate
                    admissions; 2) students seeking assistance with graduate and professional school admissions;
                    and 3) international students (largely from Latin America) seeking assistance with the international
                    student admissions process at American higher education institutions.';
            $committed_title2 = $about_section3->committed_title;
            $committed_text2 = $about_section3->committed_text;
            
        }

        return $controller->render('about_us',
            array(
                'banner_image' => $banner_image,
                'title' => $title,
                'description' => $description,
                'biography_image' => $biography_image,
                'biography_image_alt' => $biography_image_alt,
                'biography_title' => $biography_title,
                'biography_subtitle' => $biography_subtitle,
                'biography_description' => $biography_description,
                'contact_me_btn_text' => $contact_me_btn_text,
                'left_image' =>  $left_image,
                'mission_title' =>  $mission_title,
                'mission_text' =>  $mission_text,
                'services_title' =>  $services_title,
                'services_text' =>  $services_text,
                'committed_title' =>  $committed_title,
                'committed_text' =>  $committed_text,
                'right_image' => $right_image,
                'vision_title' => $vision_title,
                'vision_text' => $vision_text,
                'clientele_title' => $clientele_title,
                'Clientele_text' => $Clientele_text,
                'committed_title2' => $committed_title2,
                'committed_text2' => $committed_text2,
            )
        );

    }
}