<?php

class ContactUsAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = "/layout/main_contact";
        $model = new ContactForm;
        $contact_banner = ContactusBannerSection::model()->find();
        
        if($contact_banner){
             $banner_image = $contact_banner->_banner_image->getFileUrl('normal');
             $title = $contact_banner->title;
             $description = $contact_banner->description;
        }
        else{
            $banner_image = '/static/img/about-header.jpg';
            $title = 'Contact Us';
            $description = '<p> Higher Education Consultants of America, LLC is an education planning consultancy agency dedicated to college admissions consulting, graduate and professional school admissions consultancy, international college student applications. </p>
<p> Academic, social, financial, occupational, and other dimensions dominate the questions and concerns families and students have with the college search process. HECA, LLC understands the many questions you have about college. We provide our clients and scholars with important information regarding colleges and universities, both their stated and unstated admissions requirements, campus culture, and student interests, etc. </p>';
        }

        $contact_section1 = ContactusPageSection2::model()->find();

        if($contact_section1){
             $left_image = $contact_section1->_left_image->getFileUrl('normal');
             $form_title = $contact_section1->form_title;
             $from_button_text = $contact_section1->from_button_text;
        }
        else{
            $left_image = '/static/img/contact-img-01.jpg';
            $form_title = 'Send us and email';
            $from_button_text = 'SEND IT';
        }

        $company_info = $controller->get_company_info();

        $social_links = $controller->common_data['social'];

        $contact_section3 = ContactusPageSection3::model()->find();

        if($contact_section3){
            $use_map = $contact_section3->use_map;
            $title2 = $contact_section3->title;
            if(!$use_map){
                $map_image = $contact_section3->_map_image->getFileUrl('normal');
                $longitude = 0;
                $latitude = 0;
            }
            else{
                $longitude = $contact_section3->longitude;
                $latitude = $contact_section3->latitude;
                $map_image = '';
            }
        }
        else{
            $use_map = 0;
            $title2 = 'Check our services';
            $map_image = '/static/img/site-map.jpg';
            $longitude = 0;
            $latitude = 0;

        }

        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                if ($model->process()) {
                    $this->redirect(array('/frontend/default/thanks'));
                }
            }
        }

        return $controller->render('contact_us',
            array(
                'banner_image' => $banner_image,
                'title' => $title,
                'description' => $description,
                'left_image' => $left_image,
                'form_title' => $form_title,
                'from_button_text' => $from_button_text,
                'company_info' => $company_info,
                'social' =>$social_links,
                'use_map' => $use_map,
                'title2' => $title2,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'map_image' => $map_image,
                'model' => $model,
            )
        );

    }
}