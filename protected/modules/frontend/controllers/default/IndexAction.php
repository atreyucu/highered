<?php

class IndexAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = "/layout/main";

        $home_banner_criteria = new CDbCriteria();

        $home_banner_criteria->order = 'created DESC';

        $home_banners = HomeBannerSection::model()->findAll($home_banner_criteria);

        $banner_arr = array();

        $video_appear = false;

        foreach($home_banners as $banner){
            if($banner->banner_type == 1){
                $banner_arr[$banner->id] = array('type'=>$banner->banner_type,
                    'title'=> $banner->banner_title,
                    'subtitle'=> $banner->banner_subtitle,
                    'image' => $banner->_banner_image->getFileUrl('normal'),
                    'image_alt' => $banner->banner_image,
                    'start_text' => $banner->banner_started_text
                );
            }
            else{
                $video_appear = true;
                $temp_video_format1 = 'video/mp4';
                $real_format1 = 'mp4';
                if($banner->video_format == 1){
                    $temp_video_format1 = 'video/webm';
                    $real_format1 = 'webm';
                }
                elseif($banner->video_format == 3){
                    $temp_video_format1 = 'video/ogg';
                    $real_format1 = 'ogv';
                }


                $other_videos = array();

                if($banner->add_other_video_format == 1){
                    $video_criteria = new CDbCriteria();
                    $video_criteria->compare('home_banner',$banner->id);

                    $other_videos_obj = HomeBannerVideo::model()->findAll($video_criteria);

                    foreach($other_videos_obj as $vid){
                        $tem_upload_dir = '/images/HomeBannerVideo/';
                        $temp_video_format = 'video/mp4';
                        $real_format = 'mp4';
                        if($vid->video_format == 1){
                            $temp_video_format = 'video/webm';
                            $real_format = 'webm';
                        }
                        elseif($vid->video_format == 3){
                            $temp_video_format = 'video/ogg';
                            $real_format = 'ogv';
                        }
                        $other_videos[$vid->id] = array('video'=>$tem_upload_dir.'video_'.$vid->id.'_normal.'.$real_format,'format'=>$temp_video_format);
                    }
                }

                $banner_upload_dir = '/images/HomeBannerSection/';

                $banner_arr[$banner->id] = array('type'=>$banner->banner_type,
                    'title'=> $banner->banner_title,
                    'subtitle'=> $banner->banner_subtitle,
                    'video' => $banner_upload_dir.'video_'.$banner->id.'_normal.'.$real_format1,
                    'video_img' => $banner->_video_image->getFileUrl('normal'),
                    'start_text' => $banner->banner_started_text,
                    'video_format' => $temp_video_format1,
                    'other_vid' => $other_videos
                );
            }
        }
        
        $home_section2 = HomePageSection2::model()->find();
        
        if($home_section2){
             $image = $home_section2->_image->getFileUrl('normal');
             $image_alt = $home_section2->image;
             $title = $home_section2->title;
             $subtitle_p1 = $home_section2->subtitle_p1;
             $subtitle_p2 = $home_section2->subtitle_p2;
             $search_button_text = $home_section2->search_button_text;
             $description = $home_section2->description;
        }
        else{
            $image = '/static/img/section-2-image.jpg';
            $image_alt = 'Promo image';
            $title = 'College Search';
            $subtitle_p1 = 'Harvard receives over 37,000 applications. It admits approximately 2,100';
            $subtitle_p2 = 'Penn State receives over 66,500 applications. It admits over 8,100 to its main campus';
            $search_button_text = 'Search with us';
            $description = '<p>Trying to find a best fit does not necessarily mean the most expensive or highest ranking institution. For  some people, finding the best fit may mean focusing on college majors and available financial aid provided and/or private scholarships. After all, what good is                         it to be admitted to an Ivy League institution, if the degree you want to earn is not offered? Not much if you are interested in a specific career track.</p>';
        }
        
        $home_section3 = HomePageSection3::model()->find();
        
        if($home_section3){
             $image2 = $home_section3->_image->getFileUrl('normal');
             $image2_alt = $home_section3->image;
             $title2 = $home_section3->title;
             $description2 = $home_section3->description;
        }
        else{
            $image2 = '/static/img/section-3-image.jpg';
            $image2_alt = 'Promo Image';
            $title2 = 'Financial Aid and Scholarship';
            $description2 = 'One of the most important and often determining aspects of the college search and match process is related to the financing of higher education. The affordability question is an important part of the process and requires your';
        }

        $home_section4 = HomePageSection4::model()->find();

        if($home_section4){
            $image3 = $home_section4->_image->getFileUrl('normal');
            $title3 = $home_section4->title;
            $description3 = $home_section4->description;
        }
        else{
            $image3 = '/static/img/img-4.jpg';
            $title3 = 'International Students';
            $description3 = 'If you or your family is located out of the country, we provide the services on a limited-term basis through the use of Skype, ooVoo, Hangouts, or other video over internet service providers. Please contact us to inquire about specialized pricing for you and your situation.';
        }

        $home_section5 = HomePageSection5::model()->find();

        if($home_section5){
             $left_image =  $home_section5->_left_image->getFileUrl('normal');
             $left_image_alt =  $home_section5->left_image;
             $right_image = $home_section5->_right_image->getFileUrl('normal');
             $right_image_alt = $home_section5->right_image;
             $title4 = $home_section5->title;
             $subtitle_p12 = $home_section5->subtitle_p1;
             $subtitle_p22 = $home_section5->subtitle_p2;
             $get_started_button_text = $home_section5->get_started_button_text;
        }
        else{
            $left_image =  '/static/img/section-6-image1.jpg';
            $right_image = '/static/img/section-6-image2.jpg';
            $title4 = 'Graduate and Professional Search';
            $subtitle_p12 = 'We all know that graduate and professional education can be a difficult and enduring task. ';
            $subtitle_p22 = 'I am available to provide individual based sessions on a limited-term basis to assist with the graduate school and professional school admissions process. ';
            $get_started_button_text = 'GET STARTED';
        }
        
        $home_section6 = HomePageSection6::model()->find();
        
        if($home_section6){
             $title5 = $home_section6->title;
             $description5 = $home_section6->description;
             $left_image5 = $home_section6->_left_image->getFileUrl('normal');
             $left_image5_alt = $home_section6->left_image;
             $left_title5 = $home_section6->left_title;
             $left_btn_text5 = $home_section6->left_btn_text;
             $middle_image5 = $home_section6->_middle_image->getFileUrl('normal');
             $middle_image5_alt = $home_section6->middle_image;
             $middle_title5 = $home_section6->middle_title;
             $middle_btn_text5 = $home_section6->middle_btn_text;
             $right_image5 = $home_section6->_right_image->getFileUrl('normal');
             $right_image5_alt = $home_section6->right_image;
             $right_title5 = $home_section6->right_title;
             $right_btn_text5 = $home_section6->right_btn_text;
        }
        else{
            $title5 = 'Success Stories';
            $description5 = 'I have a limited clientele, and every student I work with receives the individual attention that makes her or him highly prepared for college search and application process. I provide… ';
            $left_image5 = '/static/img/service-1.jpg';
            $left_image5_alt = 'Promo image';
            $left_title5 = 'In ferox quadrata';
            $left_btn_text5 = 'Get Started';
            $middle_image5 = '/static/img/service-2.jpg';
            $middle_image5_alt = 'Promo image';
            $middle_title5 = 'Nunquam visum idoleum';
            $middle_btn_text5 = 'Get Started';
            $right_image5 = '/static/img/service-3.jpg';
            $right_image5_alt = 'Promo image';
            $right_title5 = 'Demittos fermium est';
            $right_btn_text5 = 'Get Started';
            
        }

        return $controller->render('index',
            array(
                'banners' => $banner_arr,
                'image'  => $image,
                'image_alt'  => $image_alt,
                'title'  => $title,
                'subtitle_p1'  => $subtitle_p1,
                'subtitle_p2'  => $subtitle_p2,
                'search_button_text'  => $search_button_text,
                'description'  => $description,
                'image2' => $image2,
                'image2_alt' => $image2_alt,
                'title2' => $title2,
                'description2' => $description2,
                'image3' => $image3,
                'title3' => $title3,
                'description3' => $description3,
                'left_image'  =>  $left_image,
                'left_image_alt'  =>  $left_image_alt,
                'right_image'  => $right_image,
                'right_image_alt'  => $right_image_alt,
                'title4'  => $title4,
                'subtitle_p12'  => $subtitle_p12,
                'subtitle_p22'  => $subtitle_p22,
                'get_started_button_text'  => $get_started_button_text,
                'title5' => $title5,
                'description5' => $description5,
                'left_image5' => $left_image5,
                'left_image5_alt' => $left_image5_alt,
                'left_title5' => $left_title5,
                'left_btn_text5' => $left_btn_text5,
                'middle_image5' => $middle_image5,
                'middle_image5_alt' => $middle_image5_alt,
                'middle_title5' => $middle_title5,
                'middle_btn_text5' => $middle_btn_text5,
                'right_image5' => $right_image5,
                'right_image5_alt' => $right_image5_alt,
                'right_title5' => $right_title5,
                'right_btn_text5' => $right_btn_text5,
                'video_appear' => $video_appear,
             )
        );

    }
}