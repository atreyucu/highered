<?php

Yii::import('entity.models._base.BaseSystemCompany');

class SystemCompany extends BaseSystemCompany
{
    /**
     * @param string $className
     * @return SystemCompany
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function choices()
    {
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr)
    {
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach ($all as $p) {
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
     * Es llamado para saber si se muestra o no la administracion de este modelo
     * usar en conjunto con los permisos
     * @return boolean
     */
    public function hasAdmin()
    {
        return false;
    }

    public function init()
    {
        $this->id = rand_uniqid();
    }

    /*
    public function afterSave(){


        $x = $this->cropX;
        $y = $this->cropY;
        $w = $this->cropW;
        $h = $this->cropH;
        $this->imageAR->formats= array(
            // create a thumbnail for used in the view datails
            'logo' => array(
                'suffix' => '_logo',
                'process' => array('resize' => array(215, 31)),
            ),
            'crop' => array(
                'suffix' => '_crop',
                'process' => array('resize' => array($x, $y,$w,$h)),
            ),
            'normal' => array(
                'suffix' => '_normal',
                'process' => array('resize' => array($x, $y,$w,$h)),
            ),
            // and override the default :
        );
        parent::afterSave();
        }
    */


    /**
     * Admin variables (ycm module)
     */
    public $adminNames = array('SystemCompanies', 'systemcompany', 'systemcompanies', 'Datos de la Organización'); // admin interface, singular, plural
    public $downloadExcel = false; // Download Excel
    public $downloadMsCsv = false; // Download MS CSV
    public $downloadCsv = false; // Download CSV


    /**
     * Config for attribute widgets (ycm module)
     *
     * @return array
     */
    public function attributeWidgets()
    {
        return array(
            array('id', 'textField'),
            array('name', 'textField'),
            array('description', 'ckeditor'),
            array('mision', 'wysiwyg'),
            array('vision', 'wysiwyg'),
            array('phone', 'textField'),
            array('fax', 'textField'),
            array('url', 'textField'),
            array('email', 'textField'),
            array('full_adress', 'wysiwyg'),
            array('longitude', 'textField'),
            array('latitude', 'textField'),
            array('terms_conditions', 'ckeditor'),
            array('shared_facebook', 'textField'),
            array('shared_google', 'textField'),
            array('shared_twitter', 'textField'),
            array('shared_youtube', 'textField'),
        );
    }


    /**
     * Config for TbGridView class (ycm module)
     *
     * @return array
     */
    public function adminSearch()
    {
        return array(
            'columns' => array(
                'name',
                'description',
                'mision',
                'vision',
                'phone',
                'fax',
                /*
                'url',
                'email',
                'full_adress',
                'longitude',
                'latitude',
                'terms_conditions',
                'shared_facebook',
                'shared_google',
                'shared_twitter',
                'shared_youtube',
            */
            ),
        );
    }


    /**
     * Config for TbDetailView class (ycm module)
     *
     * @return array
     */
    public function details()
    {
        return array(
            'attributes' => array(
                'name',
                'description',
                'mision',
                'vision',
                'phone',
                'fax',
                /*
                'url',
                'email',
                'full_adress',
                'longitude',
                'latitude',
                'terms_conditions',
                'shared_facebook',
                'shared_google',
                'shared_twitter',
                'shared_youtube',
            */
            ),
        );
    }


}