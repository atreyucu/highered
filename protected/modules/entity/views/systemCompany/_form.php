<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'system-company-form',
    'enableClientValidation' => true,

));
?>
<?php echo $form->textFieldGroup($model, 'name', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity name'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->fileFieldGroup($model, 'recipeImg', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    'hint' => Yii::t('Hint',  'Please, select the picture for the logo, the dimension for this image must be 120x35px')
));
echo $form->textFieldGroup($model, 'logo_image_alt',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert logo image_alt'),
        'append' => 'Text',
    )
); ?>

<?php echo $form->redactorGroup($model, 'description',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'mision',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'vision',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>

<?php echo $form->maskedTextFieldGroup($model, 'phone',
    array("widgetOptions" => array(
        'mask' => '999-999-9999',
        'htmlOptions' => array(
            'maxlength' => 30,
            'class' => 'col-sm-5',
            'placeholder' => Yii::t('Placeholder', 'Phone Number'),
            'append' => '#'

        ),

    ),
    )
); ?>

<?php echo $form->maskedTextFieldGroup($model, 'fax',
    array("widgetOptions" => array(
        'mask' => '999-999-9999',
        'htmlOptions' => array(
            'maxlength' => 30,
            'class' => 'col-sm-5',
            'placeholder' => Yii::t('Placeholder', 'Fax Number'),
            'append' => '#'

        ),

    ),
    )
); ?>

<?php echo $form->textFieldGroup($model, 'url', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity url'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'email', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity email'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->html5EditorGroup($model, 'full_adress',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                //'height' => '200',
                'options' => array('color' => true, 'lang' => app()->getLanguage())
            ),
        )
    )
); ?>
<?php echo $form->textFieldGroup($model, 'longitude',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity longitude'),
        'append' => '.00'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'latitude',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity latitude'),
        'append' => '.00'
    )
); ?>

<?php echo $form->redactorGroup($model, 'terms_conditions',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_facebook', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity shared_facebook'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_google', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity shared_google'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_twitter', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity shared_twitter'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_youtube', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert entity shared_youtube'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_newUser',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_contactUs',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_newsletter',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<div class='form-actions'><a href='<?php echo app()->request->urlReferrer; ?>' class='btn btn-default'><span
            class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('Button','Back'); ?></a>   <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon' => 'glyphicon glyphicon-saved',
            'label' => Yii::t('Label', 'Save item')
        )
    ); ?>
    <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'reset',
            'context' => 'warning',
            'icon' => 'glyphicon glyphicon-remove',
            'label' => Yii::t('Label', 'Reset form')
        )
    ); ?>
    <?php $this->endWidget(); ?>
    <?php if (isset($model->id)) {
        echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-remove') . 'Remove item'), array('delete', 'id' => $model->id), array('class' => 'btn btn-danger'));
    }?></div>