<?php

class EntityModule extends CWebModule
{
    public function getName()
    {
        return 'Entity';
    }

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->attachBehavior('module', array(
            'class' => 'application.components.ModuleBehavior'
        ));

        // import the module-level models and components
        $this->setImport(array(
            'entity.models.*',
            'entity.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }
}
