<?php

class m150304_143251_2015_03_04_create_tables extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable(
            'company_info', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Company info")))."'",
                'main_logo' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Main logo", "dimensions" => "260,70"))."'",
                'secondary_logo' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Secondary logo", "dimensions" => "219,59"))."'",
                'name' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("label"=>"name"))."'",
                'email' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("label"=>"email", "type" => "@email"))."'",
                'phone1' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("label"=>"phone1", "type" => "@phone"))."'",
                'phone2' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("label"=>"phone2", "type" => "@phone"))."'",

                'address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Address line1"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'footer', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Footer")))."'",
                'copy_right' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Copy right information"))."'",
                'developer_by_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Developer by text"))."'",
                'developer_by_link_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Developer by link text"))."'",
                'developer_site_url' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Developer site url\"}'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'main_menu', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Main menu")))."'",
                'portrait_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Portrait image", "dimensions" => "133,134"))."'",
                'home' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Home option text"))."'",
                'about' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"About option text"))."'",
                'service' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Service option text"))."'",
                'contact' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact option text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'social_links', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Social Media")))."'",

                'facebook' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Facebook url\"}'",
                'youtube' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Youtube url\"}'",
                'linkedin' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Linkedin url\"}'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'home_banner_section', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Banner section")))."'",
                'banner_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Image", "dimensions" => "1600,810"))."'",
                'video' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Video", "dimensions" => "1600,810"))."'",
                'video_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Alternative video image", "dimensions" => "1920,1080"))."'",
                'banner_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Banner title"))."'",
                'banner_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Banner subtitle"))."'",
                'banner_started_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Started button text"))."'",

                'banner_type' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Banner type", "data"=> array("1" => "Image", "2" => "Video")))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'home_page_section2', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Section2")))."'",
                'image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"College image", "dimensions" => "800,810"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'subtitle_p1' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtitle paragraph1"))."'",
                'subtitle_p2' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtitle paragraph2"))."'",

                'search_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"search button text"))."'",

                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description", "type"=> "@redactor"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'home_page_section3', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Section3")))."'",
                'image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Image", "dimensions" => "1600,880"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'home_page_section4', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Section4")))."'",
                'image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Image", "dimensions" => "1600,810"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'home_page_section5', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Section5")))."'",
                'left_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Left Image", "dimensions" => "857,900"))."'",
                'right_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Right Image", "dimensions" => "879,810"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'subtitle_p1' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtitle paragraph1"))."'",
                'subtitle_p2' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtitle paragraph2"))."'",

                'get_started_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Get started button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'home_page_section6', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Home Page","submenu"=>"Section6")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",
                'left_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Left Image", "dimensions" => "500,347"))."'",
                'left_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Left image title"))."'",
                'left_btn_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Left image button text"))."'",
                'middle_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Middle Image", "dimensions" => "500,347"))."'",
                'middle_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Left image title"))."'",
                'middle_btn_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Middle image button text"))."'",
                'right_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg3", "label"=>"Right Image", "dimensions" => "500,347"))."'",
                'right_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Left image title"))."'",
                'right_btn_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Left image button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'aboutus_banner_section', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"About us","submenu"=>"Banner section")))."'",
                'banner_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Banner Image", "dimensions" => "1600,484"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description", "type"=> "@redactor"))."'",
                'biography_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Biography Image", "dimensions" => "540,410"))."'",
                'biography_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Biography title"))."'",
                'biography_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Biography subtitle"))."'",
                'biography_description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Biography", "type"=> "@redactor"))."'",
                'contact_me_btn_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact me button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');





        $this->createTable(
            'aboutus_page_section2', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"About us","submenu"=>"Section 2")))."'",
                'left_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Left image", "dimensions" => "800,960"))."'",

                'mission_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Mission Title"))."'",
                'mission_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Mission text"))."'",
                'services_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Services Title"))."'",
                'services_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Services text"))."'",
                'committed_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Committed Title"))."'",
                'committed_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Committed text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'aboutus_page_section3', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"About us","submenu"=>"Section 3")))."'",
                'right_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Right image", "dimensions" => "799,960"))."'",


                'vision_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Vision Title"))."'",
                'vision_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Vision text"))."'",
                'clientele_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Clientele Title"))."'",
                'Clientele_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Services text"))."'",
                'committed_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Committed Title"))."'",
                'committed_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Committed text"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'contactus_banner_section', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Contact us","submenu"=>"Banner section")))."'",
                'banner_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Banner Image", "dimensions" => "1600,484"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description", "type"=> "@redactor"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');





        $this->createTable(
            'contactus_page_section2', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Contact us","submenu"=>"Section 2")))."'",
                'left_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Left image", "dimensions" => "800,900"))."'",


                'form_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form Title"))."'",
                'from_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"From button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'contactus_page_section3', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Contact us","submenu"=>"Section 3")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",

                'use_map' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Use map view"))."'",
                'longitude' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Longitude"))."'",
                'latitude' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Latitude"))."'",
                'map_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg3", "label"=>"Map image", "dimensions" => "1600,440"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'service_banner_section', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Service page","submenu"=>"Banner section")))."'",
                'banner_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Banner Image", "dimensions" => "1600,484"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description", "type"=> "@redactor"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');





        $this->createTable(
            'service_page_section2', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Service page","submenu"=>"Section 2")))."'",
                'background_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Background image", "dimensions" => "1600,680"))."'",


                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",

                'nights_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Weeknights title"))."'",
                'nights_hours' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Weeknights time text"))."'",


                'weekend_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Weekends title"))."'",
                'weekend_hours' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Weekends time text"))."'",


                'contact_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact button text"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'plan', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Services","submenu"=>"Plan")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'topic', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Services","submenu"=>"Topic")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'time' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Time planned"))."'",
                'description' =>  'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",

                'plan' => 'varchar(50) NOT NULL',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');




        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('topics_plan_id_fk1', 'topic', 'plan', 'plan', 'id', 'CASCADE', 'CASCADE');

	}

	public function safeDown()
	{
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->dropForeignKey('topics_plan_id_fk1', 'topic');


        $this->dropTable('company_info');
        $this->dropTable('main_menu');
        $this->dropTable('footer');
        $this->dropTable('social_links');

        $this->dropTable('home_banner_section');
        $this->dropTable('home_page_section2');
        $this->dropTable('home_page_section3');
        $this->dropTable('home_page_section4');
        $this->dropTable('home_page_section5');
        $this->dropTable('home_page_section6');


        $this->dropTable('aboutus_banner_section');
        $this->dropTable('aboutus_page_section2');
        $this->dropTable('aboutus_page_section3');


        $this->dropTable('contactus_banner_section');
        $this->dropTable('contactus_page_section2');
        $this->dropTable('contactus_page_section3');

        $this->dropTable('service_banner_section');
        $this->dropTable('service_page_section2');
        $this->dropTable('plan');
        $this->dropTable('topic');

        return true;
	}
}