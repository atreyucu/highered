<?php

class m150929_174539_2015_09_29_topic_order extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->execute("ALTER TABLE topic ADD orderr INT(11) DEFAULT 1 NULL COMMENT '{\"label\":\"Topic Order\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('topic','orderr');
	}

}