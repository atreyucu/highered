<?php

class m151001_112609_adding_error_tanks_pages_2015_10_01 extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(
            'error_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Error Page")))."'",
                'background_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Background image", "dimensions" => "1600,810"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'message' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Message text"))."'",
                'button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'thank_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Thanks page")))."'",
                'background_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Background image", "dimensions" => "1600,810"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtitle"))."'",
                'description' => 'TEXT NULL COMMENT '."'".migratetionComment(array("label"=>"Description"))."'",

                'home_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Home button text"))."'",

                'services_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Services button text"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');
	}

	public function safeDown()
	{
        $this->dropTable('error_page');
        $this->dropTable('thank_page');
	}

}