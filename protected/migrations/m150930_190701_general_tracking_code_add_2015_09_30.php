<?php

class m150930_190701_general_tracking_code_add_2015_09_30 extends CDbMigration
{

	public function safeUp()
	{
        $this->execute('CREATE TABLE IF NOT EXISTS `general_tracking_codes` (
  `id` varchar(50) NOT NULL COMMENT \'{"type": "@uid", "rows":1, "breadcrumbs": {"tablename": "General tracking codes"}}\',
  `source_code` text NOT NULL COMMENT \'{"type": "@textarea", "label": "Source code"}\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;');

	}

	public function safeDown()
	{
        $this->dropTable('general_tracking_codes');
	}

}