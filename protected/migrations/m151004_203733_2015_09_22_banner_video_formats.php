<?php

class m151004_203733_2015_09_22_banner_video_formats extends CDbMigration
{

	public function safeUp()
	{
        $this->createTable(
            'home_banner_video', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Services","submenu"=>"Topic")))."'",
                'video' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Video", "dimensions" => "1600,810"))."'",

                'video_format' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"video format", "data"=> array("1" => "video/webm", "2" => "video/mp4", "3" => "video/ogg")))."'",

                'home_banner' => 'varchar(50) NOT NULL',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->execute("ALTER TABLE home_banner_section ADD video_format VARCHAR(255) NULL COMMENT '{\"label\":\"Video Format\"}' AFTER id;");
        $this->execute("ALTER TABLE home_banner_section ADD add_other_video_format tinyint(1) NULL default 0 COMMENT '{\"label\":\"I want add more video formats\"}' AFTER id;");


        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('video_home_banner_id_fk1', 'home_banner_video', 'home_banner', 'home_banner_section', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->dropForeignKey('video_home_banner_id_fk1','home_banner_video');
        $this->dropTable('home_banner_video');
        $this->dropColumn('home_banner_section','video_format');
	}

}