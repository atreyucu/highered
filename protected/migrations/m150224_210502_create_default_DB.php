<?php

class m150224_210502_create_default_DB extends CDbMigration
{
	public function safeUp()
	{
        echo "Creating default db to iReevo Template.\n";

        echo "Creating authassignment table.\n";
        $this->createTable('authassignment',array(
          "`itemname` varchar(64) NOT NULL,
            `userid` varchar(64) NOT NULL,
            `bizrule` text,
            `data` text,
            PRIMARY KEY (`itemname`,`userid`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating authitem table.\n";
        $this->createTable('authitem',array(
          "`name` varchar(64) NOT NULL,
          `type` int(11) NOT NULL,
          `description` text,
          `bizrule` text,
          `data` text,
          PRIMARY KEY (`name`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating authitemchild table.\n";
        $this->createTable('authitemchild',array(
          "`parent` varchar(64) NOT NULL,
           `child` varchar(64) NOT NULL,
            PRIMARY KEY (`parent`,`child`),
            KEY `child` (`child`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating cache table.\n";
        $this->createTable('cache',array(
          " `id` char(128) NOT NULL,
            `expire` int(11) DEFAULT NULL,
            `value` longblob,
            PRIMARY KEY (`id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        echo "Creating email table.\n";
        $this->createTable('email',array(
          " `id` int(11) NOT NULL AUTO_INCREMENT,
             `purpose` varchar(255) NOT NULL,
             `from` varchar(255) NOT NULL,
             `reply_to` varchar(255) DEFAULT NULL,
             `subject` varchar(255) NOT NULL,
             `content` text NOT NULL,
             `internal` tinyint(1) NOT NULL,
             PRIMARY KEY (`id`),
             UNIQUE KEY `purpose` (`purpose`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating i18n_category table.\n";
        $this->createTable('i18n_category',array(
          "  `category` varchar(255) NOT NULL,
              `description` text,
              PRIMARY KEY (`category`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating i18n_language table.\n";
        $this->createTable('i18n_language',array(
          "  `id` varchar(10) NOT NULL,
              `name` varchar(100) DEFAULT NULL,
              `enable` int(11) DEFAULT '1',
              `isDefault` int(11) DEFAULT '0',
              PRIMARY KEY (`id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');
        $this->insert('i18n_language',array(
            'id'=>'en',
            'name'=>'English',
            'enable'=>'1',
            'isDefault'=>'1'
        ));
        $this->insert('i18n_language',array(
            'id'=>'es',
            'name'=>'Español',
            'enable'=>'1',
            'isDefault'=>'0'
        ));

        echo "Creating i18n_message table.\n";
        $this->createTable('i18n_message',array(
                  "`id` int(11) NOT NULL,
                  `language` varchar(10) NOT NULL,
                  `translation` text,
                  PRIMARY KEY (`id`,`language`),
                  KEY `FKi18n_messa482734` (`language`),
                  KEY `FKi18n_messa868058` (`id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating i18n_source table.\n";
        $this->createTable('i18n_source',array(
            "`id` int(11) NOT NULL AUTO_INCREMENT,
              `category` varchar(255) DEFAULT NULL,
              `message` text,
              PRIMARY KEY (`id`),
              KEY `FKi18n_sourc696102` (`category`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating profiles table.\n";
        $this->createTable('profiles',array(
            "`user_id` int(11) NOT NULL,
             `lastname` varchar(50) NOT NULL DEFAULT '',
             `firstname` varchar(50) NOT NULL DEFAULT '',
              PRIMARY KEY (`user_id`),
              KEY `FKprofiles815537` (`user_id`),
              KEY `profiles_ibfk_1` (`user_id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');
        $this->insert('profiles',array(
            'user_id'=>'1',
            'lastname'=>'Admin',
            'firstname'=>'Administrator'
        ));


        echo "Creating profiles_fields table.\n";
        $this->createTable('profiles_fields',array(
             "`id` int(10) NOT NULL AUTO_INCREMENT,
              `varname` varchar(50) NOT NULL,
              `title` varchar(255) NOT NULL,
              `field_type` varchar(50) NOT NULL,
              `field_size` int(3) NOT NULL DEFAULT '0',
              `field_size_min` int(3) NOT NULL DEFAULT '0',
              `required` int(1) NOT NULL DEFAULT '0',
              `match` varchar(255) NOT NULL DEFAULT '',
              `range` varchar(255) NOT NULL DEFAULT '',
              `error_message` varchar(255) NOT NULL DEFAULT '',
              `other_validator` varchar(255) NOT NULL DEFAULT '',
              `default` varchar(255) NOT NULL DEFAULT '',
              `widget` varchar(255) NOT NULL DEFAULT '',
              `widgetparams` varchar(5000) NOT NULL DEFAULT '',
              `position` int(3) NOT NULL DEFAULT '0',
              `visible` int(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              KEY `varname` (`varname`,`widget`,`visible`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->execute("INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3)");

            echo "Creating system_company table.\n";
        $this->createTable('system_company',array(
            " `id` varchar(50) NOT NULL COMMENT '@uid',
              `name` varchar(255) NOT NULL,
              `logo_image_alt` varchar(100) NOT NULL COMMENT 'recipeImg',
              `description` text COMMENT '@ckeditor',
              `mision` text COMMENT '@redactor',
              `vision` text COMMENT '@redactor',
              `phone` varchar(125) DEFAULT NULL,
              `fax` varchar(125) DEFAULT NULL,
              `url` varchar(255) DEFAULT NULL COMMENT '@url',
              `email` varchar(255) NOT NULL COMMENT '@email',
              `full_adress` text COMMENT '@html',
              `longitude` float NOT NULL,
              `latitude` float NOT NULL,
              `customer_support` text NOT NULL COMMENT '@redactor',
              `terms_conditions` text COMMENT '@ckeditor',
              `shared_facebook` varchar(200) NOT NULL COMMENT '@url',
              `shared_google` varchar(200) NOT NULL COMMENT '@url',
              `shared_twitter` varchar(200) NOT NULL COMMENT '@url',
              `shared_youtube` varchar(200) NOT NULL COMMENT '@url',
              `msg_newUser` text NOT NULL COMMENT '@redactor',
              `msg_contactUs` text NOT NULL COMMENT '@redactor',
              `msg_newsletter` text NOT NULL COMMENT '@redactor',
              PRIMARY KEY (`id`),
              UNIQUE KEY `name` (`name`),
              UNIQUE KEY `email` (`email`),
              UNIQUE KEY `phone` (`phone`),
              UNIQUE KEY `fax` (`fax`),
              UNIQUE KEY `url` (`url`)"
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->execute(<<<SQL
INSERT INTO `system_company` (`id`, `name`, `logo_image_alt`, `description`, `mision`, `vision`, `phone`, `fax`, `url`, `email`, `full_adress`, `longitude`, `latitude`, `customer_support`, `terms_conditions`, `shared_facebook`, `shared_google`, `shared_twitter`, `shared_youtube`, `msg_newUser`, `msg_contactUs`, `msg_newsletter`) VALUES
('ccc606ed-a16c-a854-c117-9ac834b90a4d', 'iReevo', 'iReevo', '<p><span  style="background-color: initial;"></span></p>', NULL, NULL, '305-447-2300', '786-522-0627', 'http://www.iReevo.com', 'info@iReevo.com', '<div>4138 SW 16th, Terrace</div><div>Miami, FL 33134</div>', -80.2627, 25.7564, '', NULL, 'https://www.facebook.com/pages/Havana-Air/604401099671245?sk=timeline', 'http://www.google.com', 'http://www.twiter.com', 'http://www.youtube.com', '<p>welcome</p>', '<p>welcome</p>', '<p>welcome</p>')
SQL
);

        echo "Creating text table.\n";
        $this->createTable('text',array(
            "  `id` varchar(50) NOT NULL COMMENT '@uid',
              `title` varchar(255) NOT NULL,
              `title_en` varchar(255) NOT NULL,
              `title_es` varchar(255) NOT NULL,
              `description` text NOT NULL COMMENT '@redactor',
              `description_en` text NOT NULL,
              `description_es` text NOT NULL,
              `created` date DEFAULT NULL,
              `updated` date DEFAULT NULL,
              `owner` varchar(255) DEFAULT NULL,
              `text_category_id` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `FKtext193647` (`text_category_id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        echo "Creating text_category table.\n";
        $this->createTable('text_category',array(
            "`id` varchar(50) NOT NULL COMMENT '@uid',
              `category` varchar(255) NOT NULL,
              `description` text COMMENT '@redactor',
              `created` date DEFAULT NULL,
              `updated` date DEFAULT NULL,
              `owner` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        echo "Creating users table.\n";
        $this->createTable('users',array(
            " `id` int(11) NOT NULL AUTO_INCREMENT,
              `username` varchar(20) NOT NULL,
              `password` varchar(128) NOT NULL,
              `email` varchar(128) NOT NULL,
              `activkey` varchar(128) NOT NULL DEFAULT '',
              `createtime` int(10) NOT NULL DEFAULT '0',
              `lastvisit` int(10) NOT NULL DEFAULT '0',
              `superuser` int(1) NOT NULL DEFAULT '0',
              `status` int(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              UNIQUE KEY `username` (`username`),
              UNIQUE KEY `email` (`email`),
              KEY `status` (`status`),
              KEY `superuser` (`superuser`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

 $this->execute(<<<SQL
INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `createtime`, `lastvisit`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@ireevo.com', '309d34ef076838ffac800b82ddcdb336', 1261146094, 1380542420, 1, 1)
SQL
);

        echo "Creating foreing keys.\n";
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('authassignment_ibfk_1', 'authassignment', 'itemname', 'authitem', 'name', 'CASCADE', 'CASCADE');

        $this->addForeignKey('authitemchild_ibfk_1', 'authitemchild', 'parent', 'authitem', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('authitemchild_ibfk_2', 'authitemchild', 'child', 'authitem', 'name', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FKi18n_messa482734', 'i18n_message', 'language', 'i18n_language', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FKi18n_messa868058', 'i18n_message', 'id', 'i18n_source', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FKi18n_sourc696102', 'i18n_source', 'category', 'i18n_category', 'category', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FKprofiles815537', 'profiles', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('profiles_ibfk_1', 'profiles', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('text_ibfk_1', 'text', 'text_category_id', 'text_category', 'id', 'CASCADE', 'CASCADE');

        $this->execute('SET FOREIGN_KEY_CHECKS=1;');




    }

	public function safeDown()
	{

        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->dropForeignKey('authassignment_ibfk_1', 'authassignment');

        $this->dropForeignKey('authitemchild_ibfk_1', 'authitemchild');
        $this->dropForeignKey('authitemchild_ibfk_2', 'authitemchild');

        $this->dropForeignKey('FKi18n_messa482734', 'i18n_message');
        $this->dropForeignKey('FKi18n_messa868058', 'i18n_message');

        $this->dropForeignKey('FKi18n_sourc696102', 'i18n_source');

        $this->dropForeignKey('FKprofiles815537', 'profiles');
        $this->dropForeignKey('profiles_ibfk_1', 'profiles');

        $this->dropForeignKey('text_ibfk_1', 'text');


        $this->execute('SET FOREIGN_KEY_CHECKS=1;');

        $this->dropTable('authassignment');
        $this->dropTable('authitem');
        $this->dropTable('authitemchild');
        $this->dropTable('cache');
        $this->dropTable('email');
        $this->dropTable('i18n_category');
        $this->dropTable('i18n_language');
        $this->dropTable('i18n_message');
        $this->dropTable('i18n_source');
        $this->dropTable('profiles');
        $this->dropTable('profiles_fields');
        $this->dropTable('system_company');
        $this->dropTable('text');
        $this->dropTable('text_category');
        $this->dropTable('users');
	}

}