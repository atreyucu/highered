/**
 * Created by R4im3L on 25/08/2015.
 */
$(function() {
    //set imgs with class="image-to-bg" to parent background image
    $.each($(".bg-image"), function (){
        $(this).css({
            "background-image" : "url(" + $(this).find(".image-to-bg").attr("src") + ")"
        });
    });

    //menu-trigger
    var menu = $(".site-menu");
    var trigger = $(".menu-trigger");

    if ($(document).scrollTop() > 60) {
        trigger.css("top", "10px");
    } else {
        trigger.css("top", "36px");
    }
    $(document).on("scroll", function () {
        if ($(document).scrollTop() > 60) {
            trigger.css("top", "10px");
        } else {
            trigger.css("top", "36px");
        }
    });
    var imageSection = $(".image-section");
    $.each(imageSection, function() {
        var t = $(this);
        if (t.hasClass("left")) {
            t.height(t.next(".content").outerHeight());
        } else if (t.hasClass("right")) {
            t.height(t.prev(".content").outerHeight());
        }
    });

    $(".basic-info").height($(".form-wrapper").outerHeight());

    trigger.on("click", function (e) {
        e.preventDefault();
        menu.toggleClass("opened");
    });
    $(".close-btn").on("click", function(e) {
        e.preventDefault();
        menu.toggleClass("opened");
    });

    //footer bg
    var leftSide = $(".left-corner");
    var rightSide = $(".right-corner");
    var wrapper = $(".wrapper");
    leftSide.css({
        "border-top" : wrapper.outerHeight()+"px" + " solid #fff",
        "border-right" : wrapper.width()*0.25+"px" + " solid transparent"
    });
    rightSide.css({
        "border-top" : wrapper.outerHeight()+"px" + " solid #fff",
        "border-left" : wrapper.width()*0.25+"px" + " solid transparent"
    });

    //slick carousel init
    $(".main-carousel").slick({
        arrows: true,
        prevArrow: '<div class="arrows nav-left"><img src="/static/img/arrow-left.png" alt="left"></div>',
        nextArrow: '<div class="arrows nav-right"><img src="/static/img/arrow-right.png" alt="left"></div>',
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        slide: '.slide-item'
    });

    $(".content-carousel").slick({
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        slide: 'dl',
        adaptiveHeight: true,
        fade: true,
        asNavFor: '.boards-list'
    });

    $(".boards-list").slick({
        arrows: false,
        dots: false,
        vertical: true,
        slidesToShow: 5,
        focusOnSelect: true,
        slidesToScroll: 1,
        asNavFor: '.content-carousel'
    });
});