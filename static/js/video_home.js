$(document).ready(function(){
    var video = $('.main-carousel .slick-active').find('video').get(0).play();

    $('#main-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.main-carousel .slick-slide').find('video').get(0).pause();
        var video = $('.main-carousel .slick-active').find('video').get(0).play();
    });
});
