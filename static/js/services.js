/**
 * Created by ridosbeym on 30/09/15.
 */
$(function() {
    var navTrigger = $(".nav-trigger");
    var listWrapper = $(".list-wrapper");
    var footer = $(".page-footer");
    var sessionsWrapper = $(".session-board");
    navTrigger.on("click", function () {
        var t = $(this);
        listWrapper.toggleClass("opened");
        if (t.hasClass("fa-angle-left")) {
            t.removeClass("fa-angle-left").addClass("fa-angle-right");
        } else {
            t.removeClass("fa-angle-right").addClass("fa-angle-left");
        }
    });
    if ($(document).scrollTop() > sessionsWrapper.offset().top && $(document).scrollTop() < footer.offset().top) {
        listWrapper.addClass("fixed")
    } else if ($(document).scrollTop() < sessionsWrapper.offset().top || $(document).scrollTop() > footer.offset().top) {
        listWrapper.removeClass("fixed")
    }
    $(document).on("scroll", function () {
        if ($(document).scrollTop() > sessionsWrapper.offset().top && $(document).scrollTop() < footer.offset().top) {
            listWrapper.addClass("fixed")
        } else if ($(document).scrollTop() < sessionsWrapper.offset().top || $(document).scrollTop() > footer.offset().top - 80) {
            listWrapper.removeClass("fixed")
        }
    });
});

