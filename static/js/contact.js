function initialize() {
    var latitude =  parseFloat($('#latitude').attr('value'));
    var longitude = parseFloat($('#longitude').attr('value'));
    var zoom = parseInt($('#zoom').val());
    var map_canvas = document.getElementById('map_contact');
    var location = new google.maps.LatLng(latitude, longitude);
    var map_options = {
        center: location,
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options);
    new google.maps.Marker({
        position: location,
        map: map
    });
}

$(document).ready(function(){
    google.maps.event.addDomListener(window, 'load', initialize);
})
