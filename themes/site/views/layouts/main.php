<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link rel="stylesheet" href="<?php echo thu('css/normalize.css');?>">
    <!-- Use the .htaccess and remove these lines to avoid edge case issues. More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>

    <?php
    cs()->registerCssFile(thu('css/docs.css'));
    cs()->registerCssFile(thu('js/google-code-prettify/prettify.css'));
    cs()->registerCssFile(thu('css/jquery.gritter.css'));


    ?>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo thu('js/html5shiv.js')?>"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo thu('ico/apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo thu('ico/apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo thu('ico/apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo thu('ico/apple-touch-icon-57-precomposed.png')?>">
    <link rel="shortcut icon" href="<?php echo thu('ico/favicon.png')?>">

</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar">

<!-- Navbar
================================================== -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo CHtml::link(app()->name, app()->baseUrl, array('class'=>'brand'))?>

            <div class="nav-collapse collapse">
                <?php $this->widget('zii.widgets.CMenu', array(
                    'items' => Capitulo::menu(),
                    'htmlOptions' => array(
                        'class' => 'nav',
                    ),
                ))?>
            </div>
        </div>
    </div>
</div>

<!-- Subhead
================================================== -->
<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1><?php echo $this->title?></h1>
        <p class="lead"><?php echo $this->description?></p>
    </div>
</header>

<!--div class="bs-docs-social">
    <div class="container">
        <ul class="bs-docs-social-buttons">
            <li>
                <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twitter&repo=bootstrap&type=watch&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="100px" height="20px"></iframe>
            </li>
            <li>
                <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twitter&repo=bootstrap&type=fork&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="102px" height="20px"></iframe>
            </li>
            <li class="follow-btn">
                <a href="https://twitter.com/twbootstrap" class="twitter-follow-button" data-link-color="#0069D6" data-show-count="true">Follow @twbootstrap</a>
            </li>
            <li class="tweet-btn">
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://twitter.github.com/bootstrap/" data-count="horizontal" data-via="twbootstrap" data-related="mdo:Creator of Twitter Bootstrap">Tweet</a>
            </li>
        </ul>
    </div>
</div-->

<div class="container">
    <div class="row">
        <?php echo $content; ?>
    </div>


</div>



<!-- Footer
================================================== -->
<footer class="footer">
    <div class="container">
        <p>Powered by <?php echo el('iReevo', "http://www.ireevo.com/")?>.</p>
    </div>
</footer>



<?php
cs()->registerScriptFile(thu('js/jquery.gritter.js'), CClientScript::POS_END);
cs()->registerScriptFile(thu('js/holder/holder.js'), CClientScript::POS_END);
cs()->registerScriptFile(thu('js/application.js'), CClientScript::POS_END);
cs()->registerScriptFile(thu('js/google-code-prettify/prettify.js'), CClientScript::POS_END);

?>
<?php
$flashes = user()->flashes;
foreach($flashes as $key => $message){
    $icons = param('flash.icons');
    $icon = $icons[$key];
    cs()->registerScript('gritter_flash', <<<JS
    $.gritter.add({
        text:	'<i class="icon-$icon"></i> $message',
        sticky: false
    });
JS
        , CClientScript::POS_READY);

}
?>
</body>
</html>
