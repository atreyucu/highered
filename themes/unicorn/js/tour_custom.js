function swap_contents(tr1,tr2){
    if(tr1.length != 0){
        var temp_value = tr1.children('td').last().prev().children('input[type=hidden]').val();
        tr1.children('td').last().prev().children('input[type=hidden]').val(tr2.children('td').last().prev().children('input[type=hidden]').val());
        tr2.children('td').last().prev().children('input[type=hidden]').val(temp_value);

        var temp_html = tr1.html();
        tr1.html(tr2.html());
        tr2.html(temp_html);
    }
}

function add_gall(){
    if($("#tourGallTable tbody").children("tr").length > 1){
        $("#tourGallTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(parseInt($("#tourGallTable tbody").children("tr").last().prev().children().last().prev().children("input[type=hidden]").val()) + 1);
    }
    else{
        $("#tourGallTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(1);
    }
}

function add_day(){
    if($("#tourDayTable tbody").children("tr").length > 1){
        $("#tourDayTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(parseInt($("#tourDayTable tbody").children("tr").last().prev().children().last().prev().children("input[type=hidden]").val()) + 1)
    }
    else{
        $("#tourDayTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val("1");
    }
}


function swap_day_contents(tr1,tr2){
    if(tr1.length != 0 && tr1.attr('id') != 'hid_day_numb'){
        var temp_value = tr1.children('td').last().prev().children('input[type=hidden]').val();
        tr1.children('td').last().prev().children('input[type=hidden]').val(tr2.children('td').last().prev().children('input[type=hidden]').val());
        tr2.children('td').last().prev().children('input[type=hidden]').val(temp_value)

        //children('td').last().prev().children('h4').children('#day_numb').html()

        var temp_value = tr1.children('td').last().prev().children('h4').children('#day_numb').html();
        tr1.children('td').last().prev().children('h4').children('#day_numb').html(tr2.children('td').last().prev().children('h4').children('#day_numb').html());
        tr2.children('td').last().prev().children('h4').children('#day_numb').html(temp_value);



        var temp_html = tr1.html();
        tr1.html(tr2.html());
        tr2.html(temp_html);
    }
}

function re_numb_days_from(tr){
    temp_tr = tr;
    //temp_tr.children('td').last().prev().children('h4').children('#day_numb').html(parseInt(temp_tr.children('td').last().prev().children('h4').children('#day_numb').html()) - 1);
    while(temp_tr.children('td').length != 0){
        temp_tr.children('td').last().prev().children('h4').children('#day_numb').html(parseInt(temp_tr.children('td').last().prev().children('h4').children('#day_numb').html()) - 1);
        temp_tr = temp_tr.next();
    }
}



function add_topic(){
    if($("#topicTable tbody").children("tr").length > 1){
        $("#topicTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(parseInt($("#topicTable tbody").children("tr").last().prev().children().last().prev().children("input[type=hidden]").val()) + 1);
    }
    else{
        $("#topicTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val("1");
    }
}



function swap_topic_contents(tr1,tr2){
    if(tr1.length != 0 && tr1.attr('id') != 'hid_day_numb'){
        var temp_value = tr1.children('td').last().prev().children('input[type=hidden]').val();
        tr1.children('td').last().prev().children('input[type=hidden]').val(tr2.children('td').last().prev().children('input[type=hidden]').val());
        tr2.children('td').last().prev().children('input[type=hidden]').val(temp_value)

        var temp_html = tr1.html();
        tr1.html(tr2.html());
        tr2.html(temp_html);
    }
}