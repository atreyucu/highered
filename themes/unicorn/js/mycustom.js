//soft scroll
$(document).ready(function() {
    //$(".redactor").redactor();
    $( ".tabs" ).tabs();
    $( ".accordion" ).accordion();

    $(".currency").maskMoney({
        'symbol': ' $',
        'showSymbol': false,
        'symbolStay': false,
        'thousands': ', ',
        'decimal': '.',
        'precision': 2,
        'defaultZero': true,
        'allowZero': true,
        'allowNegative': false
    });

    $(".various").fancybox({
        maxWidth	: 900,
        maxHeight	: 900,
        fitToView	: false,
        width		: '80%',
        height		: '80%',
        autoSize	: false,
        closeClick	: false,
        openEffect : 'elastic',
        openSpeed  : 150,
        closeEffect	: 'elastic'
    });

    $(".fancybox").fancybox({
        openEffect : 'elastic',
        openSpeed  : 150,

        closeEffect : 'elastic',
        closeSpeed  : 150,
        helpers : {
            title : {
                type : 'inside'
            },
            buttons	: {}
        }


       /* afterLoad : function() {
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }*/
    });



});